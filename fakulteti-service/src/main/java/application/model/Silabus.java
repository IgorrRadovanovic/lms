package application.model;


import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Silabus {
	
	public Silabus() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@OneToMany(mappedBy = "silabus")
    private List<Ishod> ishodi = new ArrayList<Ishod>();
	public Silabus(Integer id, List<Ishod> ishodi) {
		super();
		this.id = id;
		this.ishodi = ishodi;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<Ishod> getIshodi() {
		return ishodi;
	}
	public void setIshodi(List<Ishod> ishodi) {
		this.ishodi = ishodi;
	}
	
	
	

}
