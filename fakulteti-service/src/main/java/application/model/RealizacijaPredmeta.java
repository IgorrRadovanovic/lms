package application.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class RealizacijaPredmeta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne()
	private Predmet predmet;

//	@OneToMany(mappedBy = "realizacijaPredmeta")
//	private List<PohadjanjePredmeta> pohadjanjePredmeta = new ArrayList<PohadjanjePredmeta>();

//	@OneToMany(mappedBy = "realizacijaPredmeta")
//	private List<NastavnikNaRealizaciji> nastavnici = new ArrayList<NastavnikNaRealizaciji>();

	@OneToMany(mappedBy = "realizacijaPredmeta")
	private List<TerminNastave> termini = new ArrayList<TerminNastave>();

//	@OneToMany(mappedBy = "realizacijaPredmeta")
//	private List<EvaluacijaZnanja> evaluacije = new ArrayList<EvaluacijaZnanja>();

	public RealizacijaPredmeta() {
		super();
	}

	public RealizacijaPredmeta(
			Integer id/* , ArrayList<PohadjanjePredmeta> pohadjanjePredmeta */,
			/* ArrayList<NastavnikNaRealizaciji> nastavnici, */ ArrayList<TerminNastave> termini/*
																								 * , ArrayList<
																								 * EvaluacijaZnanja>
																								 * evaluacije
																								 */) {
		super();
		this.id = id;
//		this.pohadjanjePredmeta = pohadjanjePredmeta;
//		this.nastavnici = nastavnici;
		this.termini = termini;
//		this.evaluacije = evaluacije;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	public List<PohadjanjePredmeta> getPohadjanjePredmeta() {
//		return pohadjanjePredmeta;
//	}
//
//	public void setPohadjanjePredmeta(List<PohadjanjePredmeta> pohadjanjePredmeta) {
//		this.pohadjanjePredmeta = pohadjanjePredmeta;
//	}
//
//	public List<NastavnikNaRealizaciji> getNastavnici() {
//		return nastavnici;
//	}
//
//	public void setNastavnici(List<NastavnikNaRealizaciji> nastavnici) {
//		this.nastavnici = nastavnici;
//	}

	public List<TerminNastave> getTermini() {
		return termini;
	}

	public void setTermini(List<TerminNastave> termini) {
		this.termini = termini;
	}

//	public List<EvaluacijaZnanja> getEvaluacije() {
//		return evaluacije;
//	}
//
//	public void setEvaluacije(List<EvaluacijaZnanja> evaluacije) {
//		this.evaluacije = evaluacije;
//	}

}
