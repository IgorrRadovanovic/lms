package application.model.dto;

public class FajlDTO {

    

    private Integer id;
    private String opis;
    private String url;

    
    private ObavestenjeDTO obavestenje;
    
    

    public FajlDTO() {
    	super();
    }
	public FajlDTO(Integer id,String opis, String url, ObavestenjeDTO obavestenje) {
		super();
		this.id = id;
		this.opis = opis;
		this.url = url;
		this.obavestenje = obavestenje;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ObavestenjeDTO getObavestenje() {
		return obavestenje;
	}

	public void setObavestenje(ObavestenjeDTO obavestenje) {
		this.obavestenje = obavestenje;
	}

    
}