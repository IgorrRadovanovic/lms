package application.model.dto;

import java.util.List;


public class PredmetDTO {
    

    private Integer id;


    private String naziv;


    private int espb;


    private boolean obavezan;


    private int brojPredavanja;


    private int brojVezbi;


    private int drugiObliciNastave;


    private int istrazivackiRad;


    private int ostaliCasovi;

    
    private SilabusDTO silabus;


    private List<RealizacijaPredmetaDTO> realizacijePredmeta;

    
    private GodinaStudijaDTO godinaStudija;
    
    
    private PredmetDTO preduslov;

	
	public PredmetDTO(Integer id, String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi, SilabusDTO silabus,
			List<RealizacijaPredmetaDTO> realizacijePredmeta, GodinaStudijaDTO godinaStudija, PredmetDTO preduslov) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.espb = espb;
		this.obavezan = obavezan;
		this.brojPredavanja = brojPredavanja;
		this.brojVezbi = brojVezbi;
		this.drugiObliciNastave = drugiObliciNastave;
		this.istrazivackiRad = istrazivackiRad;
		this.ostaliCasovi = ostaliCasovi;
		this.silabus = silabus;
		this.realizacijePredmeta = realizacijePredmeta;
		this.godinaStudija = godinaStudija;
		this.preduslov = preduslov;
	}

	public PredmetDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getEspb() {
		return espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean isObavezan() {
		return obavezan;
	}

	public void setObavezan(boolean obavezan) {
		this.obavezan = obavezan;
	}

	public int getBrojPredavanja() {
		return brojPredavanja;
	}

	public void setBrojPredavanja(int brojPredavanja) {
		this.brojPredavanja = brojPredavanja;
	}

	public int getBrojVezbi() {
		return brojVezbi;
	}

	public void setBrojVezbi(int brojVezbi) {
		this.brojVezbi = brojVezbi;
	}

	public int getDrugiObliciNastave() {
		return drugiObliciNastave;
	}

	public void setDrugiObliciNastave(int drugiObliciNastave) {
		this.drugiObliciNastave = drugiObliciNastave;
	}

	public int getIstrazivackiRad() {
		return istrazivackiRad;
	}

	public void setIstrazivackiRad(int istrazivackiRad) {
		this.istrazivackiRad = istrazivackiRad;
	}

	public int getOstaliCasovi() {
		return ostaliCasovi;
	}

	public void setOstaliCasovi(int ostaliCasovi) {
		this.ostaliCasovi = ostaliCasovi;
	}

	public SilabusDTO getSilabus() {
		return silabus;
	}

	public void setSilabus(SilabusDTO silabus) {
		this.silabus = silabus;
	}

	public List<RealizacijaPredmetaDTO> getRealizacijePredmeta() {
		return realizacijePredmeta;
	}

	public void setRealizacijePredmeta(List<RealizacijaPredmetaDTO> realizacijePredmeta) {
		this.realizacijePredmeta = realizacijePredmeta;
	}

	public GodinaStudijaDTO getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudijaDTO godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public PredmetDTO getPreduslov() {
		return preduslov;
	}

	public void setPreduslov(PredmetDTO preduslov) {
		this.preduslov = preduslov;
	}
    
    

    
}
