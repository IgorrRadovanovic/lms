package application.model.dto;

import java.util.ArrayList;
import java.util.List;


public class RealizacijaPredmetaDTO {

	

	private Integer id;
	

	private PredmetDTO predmet;


//	private List<PohadjanjePredmetaDTO> pohadjanjePredmeta = new ArrayList<PohadjanjePredmetaDTO>();


//	private List<NastavnikNaRealizacijiDTO> nastavnici = new ArrayList<NastavnikNaRealizacijiDTO>();


	private List<TerminNastaveDTO> termini = new ArrayList<TerminNastaveDTO>();


//	private List<EvaluacijaZnanjaDTO> evaluacije = new ArrayList<EvaluacijaZnanjaDTO>();

	public RealizacijaPredmetaDTO() {
		super();
	}

	public RealizacijaPredmetaDTO(
			Integer id/* , ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta */,
			/* ArrayList<NastavnikNaRealizacijiDTO> nastavnici, */ ArrayList<TerminNastaveDTO> termini/*
																								 * , ArrayList<
																								 * EvaluacijaZnanjaDTO>
																								 * evaluacije
																								 */) {
		super();
		this.id = id;
//		this.pohadjanjePredmeta = pohadjanjePredmeta;
//		this.nastavnici = nastavnici;
		this.termini = termini;
//		this.evaluacije = evaluacije;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	public List<PohadjanjePredmetaDTO> getPohadjanjePredmeta() {
//		return pohadjanjePredmeta;
//	}
//
//	public void setPohadjanjePredmeta(List<PohadjanjePredmetaDTO> pohadjanjePredmeta) {
//		this.pohadjanjePredmeta = pohadjanjePredmeta;
//	}
//
//	public List<NastavnikNaRealizacijiDTO> getNastavnici() {
//		return nastavnici;
//	}
//
//	public void setNastavnici(List<NastavnikNaRealizacijiDTO> nastavnici) {
//		this.nastavnici = nastavnici;
//	}

	public List<TerminNastaveDTO> getTermini() {
		return termini;
	}

	public void setTermini(List<TerminNastaveDTO> termini) {
		this.termini = termini;
	}

	public PredmetDTO getPredmet() {
		return predmet;
	}

	public void setPredmet(PredmetDTO predmet) {
		this.predmet = predmet;
	}

//	public List<EvaluacijaZnanjaDTO> getEvaluacije() {
//		return evaluacije;
//	}
//
//	public void setEvaluacije(List<EvaluacijaZnanjaDTO> evaluacije) {
//		this.evaluacije = evaluacije;
//	}

}
