package application.model.dto;



import jakarta.persistence.Column;



public class StudijskiProgramDTO {

	 	

	    private Integer id;

	 	private String naziv;
	 	
//	 	
//	 	private NastavnikDTO rukovodilac;
	 	@Column
	 	private Integer rukovodilacId;
	 	
	 	private GodinaStudijaDTO godinaStudija;
	 	
	 	private FakultetDTO fakultet;

		public StudijskiProgramDTO(Integer id, String naziv, /*NastavnikDTO*/Integer nastavnik,GodinaStudijaDTO g,FakultetDTO f) {
			super();
			this.id = id;
			this.naziv = naziv;
			this.rukovodilacId = nastavnik;
			this.godinaStudija = g;
			this.fakultet = f;
		}
		public StudijskiProgramDTO() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getNaziv() {
			return naziv;
		}
		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}
//		public NastavnikDTO getRukovodilac() {
//			return rukovodilac;
//		}
//		public void setRukovodilac(NastavnikDTO nastavnik) {
//			this.rukovodilac = nastavnik;
//		}
		public GodinaStudijaDTO getGodinaStudija() {
			return godinaStudija;
		}
		public void setGodinaStudija(GodinaStudijaDTO godinaStudija) {
			this.godinaStudija = godinaStudija;
		}
		public Integer getRukovodilacId() {
			return rukovodilacId;
		}
		public void setRukovodilacId(Integer rukovodilacId) {
			this.rukovodilacId = rukovodilacId;
		}
		public FakultetDTO getFakultet() {
			return fakultet;
		}
		public void setFakultet(FakultetDTO fakultet) {
			this.fakultet = fakultet;
		}
		
		
		
	 	
	    
	    

}
