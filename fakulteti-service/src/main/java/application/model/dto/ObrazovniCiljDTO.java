package application.model.dto;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;



public class ObrazovniCiljDTO {
	

	private Integer id;
	@Column
	private String opis;

	private List<IshodDTO> ishodi = new ArrayList<>();
	
	public ObrazovniCiljDTO(Integer id, String opis) {
		super();
		this.id = id;
		this.opis = opis;
	}

	public ObrazovniCiljDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public List<IshodDTO> getIshodi() {
		return ishodi;
	}

	public void setIshodi(List<IshodDTO> ishodi) {
		this.ishodi = ishodi;
	}

}
