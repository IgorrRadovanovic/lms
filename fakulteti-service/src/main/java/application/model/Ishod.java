package application.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
//import studentiApp.model.EvaluacijaZnanja;

@Entity
public class Ishod {//Tema
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String naziv;
	@ManyToOne
	private Silabus silabus;
	//Mozda treba obrnuti vezu izmedju Ishoda i Obrazovnog cilja
	@ManyToOne
	private ObrazovniCilj obrazovniCilj;
	@ManyToOne
	private TerminNastave terminNastave; // obrnuti vezu
	public Ishod() {
		super();
		// TODO Auto-generated constructor stub
	}

//	@ManyToOne
//	private EvaluacijaZnanja evaluacijaZnanja; // ova veza se uklanja


	public Ishod(Integer id, String naziv, Silabus silabus, ObrazovniCilj obrazovniCilj, TerminNastave terminNastave
			/*,EvaluacijaZnanja evaluacijaZnanja*/) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.silabus = silabus;
		this.obrazovniCilj = obrazovniCilj;
		this.terminNastave = terminNastave;
//		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Silabus getSilabus() {
		return silabus;
	}

	public void setSilabus(Silabus silabus) {
		this.silabus = silabus;
	}

	public ObrazovniCilj getObrazovniCilj() {
		return obrazovniCilj;
	}

	public void setObrazovniCilj(ObrazovniCilj obrazovniCilj) {
		this.obrazovniCilj = obrazovniCilj;
	}

	public TerminNastave getTerminNastave() {
		return terminNastave;
	}

	public void setTerminNastave(TerminNastave terminNastave) {
		this.terminNastave = terminNastave;
	}

//	public EvaluacijaZnanja getEvaluacijaZnanja() {
//		return evaluacijaZnanja;
//	}
//
//	public void setEvaluacijaZnanja(EvaluacijaZnanja evaluacijaZnanja) {
//		this.evaluacijaZnanja = evaluacijaZnanja;
//	}

}
