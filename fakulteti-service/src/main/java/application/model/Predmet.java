package application.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Predmet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String naziv;

    @Column(nullable = false)
    private int espb;

    @Column(nullable = false)
    private boolean obavezan;

    @Column(nullable = false)
    private int brojPredavanja;

    @Column(nullable = false)
    private int brojVezbi;

    @Column(nullable = false)
    private int drugiObliciNastave;

    @Column(nullable = false)
    private int istrazivackiRad;

    @Column(nullable = false)
    private int ostaliCasovi;

    @ManyToOne
    private Silabus silabus;

    @OneToMany(mappedBy = "predmet")
    private List<RealizacijaPredmeta> realizacijePredmeta;

    @ManyToOne
    private GodinaStudija godinaStudija;
    
    @ManyToOne
    private Predmet preduslov;

	
	public Predmet(Integer id, String naziv, int espb, boolean obavezan, int brojPredavanja, int brojVezbi,
			int drugiObliciNastave, int istrazivackiRad, int ostaliCasovi, Silabus silabus,
			List<RealizacijaPredmeta> realizacijePredmeta, GodinaStudija godinaStudija, Predmet preduslov) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.espb = espb;
		this.obavezan = obavezan;
		this.brojPredavanja = brojPredavanja;
		this.brojVezbi = brojVezbi;
		this.drugiObliciNastave = drugiObliciNastave;
		this.istrazivackiRad = istrazivackiRad;
		this.ostaliCasovi = ostaliCasovi;
		this.silabus = silabus;
		this.realizacijePredmeta = realizacijePredmeta;
		this.godinaStudija = godinaStudija;
		this.preduslov = preduslov;
	}

	public Predmet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getEspb() {
		return espb;
	}

	public void setEspb(int espb) {
		this.espb = espb;
	}

	public boolean isObavezan() {
		return obavezan;
	}

	public void setObavezan(boolean obavezan) {
		this.obavezan = obavezan;
	}

	public int getBrojPredavanja() {
		return brojPredavanja;
	}

	public void setBrojPredavanja(int brojPredavanja) {
		this.brojPredavanja = brojPredavanja;
	}

	public int getBrojVezbi() {
		return brojVezbi;
	}

	public void setBrojVezbi(int brojVezbi) {
		this.brojVezbi = brojVezbi;
	}

	public int getDrugiObliciNastave() {
		return drugiObliciNastave;
	}

	public void setDrugiObliciNastave(int drugiObliciNastave) {
		this.drugiObliciNastave = drugiObliciNastave;
	}

	public int getIstrazivackiRad() {
		return istrazivackiRad;
	}

	public void setIstrazivackiRad(int istrazivackiRad) {
		this.istrazivackiRad = istrazivackiRad;
	}

	public int getOstaliCasovi() {
		return ostaliCasovi;
	}

	public void setOstaliCasovi(int ostaliCasovi) {
		this.ostaliCasovi = ostaliCasovi;
	}

	public Silabus getSilabus() {
		return silabus;
	}

	public void setSilabus(Silabus silabus) {
		this.silabus = silabus;
	}

	public List<RealizacijaPredmeta> getRealizacijePredmeta() {
		return realizacijePredmeta;
	}

	public void setRealizacijePredmeta(List<RealizacijaPredmeta> realizacijePredmeta) {
		this.realizacijePredmeta = realizacijePredmeta;
	}

	public GodinaStudija getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudija godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public Predmet getPreduslov() {
		return preduslov;
	}

	public void setPreduslov(Predmet preduslov) {
		this.preduslov = preduslov;
	}
    
    

    
}
