package application.service; 
import org.springframework.stereotype.Service; 
import application.model.Silabus;
import application.repository.SilabusRepository;  
@Service
public class SilabusService extends GenericService<Silabus, Integer> {
public SilabusService(SilabusRepository repository){super(repository);
}
}