package application.service; 
import org.springframework.stereotype.Service; 
import application.model.ObrazovniCilj;
import application.repository.ObrazovniCiljRepository;  
@Service
public class ObrazovniCiljService extends GenericService<ObrazovniCilj, Integer> {
public ObrazovniCiljService(ObrazovniCiljRepository repository){super(repository);
}
}