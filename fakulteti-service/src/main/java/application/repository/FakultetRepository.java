package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Fakultet; 
 public interface FakultetRepository extends CrudRepository<Fakultet, Integer> {

}