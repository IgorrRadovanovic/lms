package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Silabus; 
 public interface SilabusRepository extends CrudRepository<Silabus, Integer> {

}