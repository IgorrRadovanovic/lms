package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.GodinaStudija; 
 public interface GodinaStudijaRepository extends CrudRepository<GodinaStudija, Integer> {

}