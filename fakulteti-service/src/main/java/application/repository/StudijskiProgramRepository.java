package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.StudijskiProgram; 
 public interface StudijskiProgramRepository extends CrudRepository<StudijskiProgram, Integer> {

}