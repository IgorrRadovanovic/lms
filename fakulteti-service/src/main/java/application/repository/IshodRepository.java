package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Ishod; 
 public interface IshodRepository extends CrudRepository<Ishod, Integer> {

}