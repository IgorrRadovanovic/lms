package application.controller; 
 import application.model.TerminNastave; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.TerminNastaveDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/termininastave")
public class TerminNastaveController extends GenericController<TerminNastave,TerminNastaveDTO,GenericService<TerminNastave, Integer>>{
public TerminNastaveController(GenericService<TerminNastave, Integer> service){
 super(service);
}}