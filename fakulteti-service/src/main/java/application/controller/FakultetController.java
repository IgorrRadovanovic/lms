package application.controller; 
 import application.model.Fakultet; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.FakultetDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/fakulteti")
public class FakultetController extends GenericController<Fakultet,FakultetDTO,GenericService<Fakultet, Integer>>{
public FakultetController(GenericService<Fakultet, Integer> service){
 super(service);
}}