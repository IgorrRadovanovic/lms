package application.controller; 
 import application.model.Silabus; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.SilabusDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/silabusi")
public class SilabusController extends GenericController<Silabus,SilabusDTO,GenericService<Silabus, Integer>>{
public SilabusController(GenericService<Silabus, Integer> service){
 super(service);
}}