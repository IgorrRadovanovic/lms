package application.controller; 
 import application.model.ObrazovniCilj; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.ObrazovniCiljDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/obrazovniciljevi")
public class ObrazovniCiljController extends GenericController<ObrazovniCilj,ObrazovniCiljDTO,GenericService<ObrazovniCilj, Integer>>{
public ObrazovniCiljController(GenericService<ObrazovniCilj, Integer> service){
 super(service);
}}