package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nastavniciApp.model.TipZvanja;
import nastavniciApp.model.dto.TipZvanjaDTO;
import nastavniciApp.service.GenericService;
@RestController 

@RequestMapping("/api/tipovizvanja")
public class TipZvanjaController extends GenericController<TipZvanja,TipZvanjaDTO,GenericService<TipZvanja, Integer>>{
public TipZvanjaController(GenericService<TipZvanja, Integer> service){
 super(service);
}}