package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import nastavniciApp.model.TipNastave;
import nastavniciApp.model.dto.TipNastaveDTO;
import nastavniciApp.service.GenericService;
@Controller 
//@CrossOrigin(origins = "http://localhost:4200/**")
@RequestMapping("/api/tipovinastave")
public class TipNastaveController extends GenericController<TipNastave,TipNastaveDTO,GenericService<TipNastave, Integer>>{
public TipNastaveController(GenericService<TipNastave, Integer> service){
 super(service);
}}