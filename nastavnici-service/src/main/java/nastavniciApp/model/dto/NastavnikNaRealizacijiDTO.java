package nastavniciApp.model.dto;

public class NastavnikNaRealizacijiDTO {
	

	private Integer id;

	private int broj_casova;

	private Integer realizacijaPredmetaId;

	private NastavnikDTO nastavnik;

	private TipNastaveDTO tipNastave;

	public NastavnikNaRealizacijiDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NastavnikNaRealizacijiDTO(Integer id, int broj_casova, /*RealizacijaPredmetaDTO*/Integer realizacijaPredmeta,
			NastavnikDTO nastavnik, TipNastaveDTO tipNastave) {
		super();
		this.id = id;
		this.broj_casova = broj_casova;
		this.realizacijaPredmetaId = realizacijaPredmeta;
		this.nastavnik = nastavnik;
		this.tipNastave = tipNastave;
	}

	public NastavnikNaRealizacijiDTO(Integer id, int broj_casova) {
		super();
		this.id = id;
		this.broj_casova = broj_casova;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getBroj_casova() {
		return broj_casova;
	}

	public void setBroj_casova(int broj_casova) {
		this.broj_casova = broj_casova;
	}

	public /*RealizacijaPredmetaDTO*/Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(/*RealizacijaPredmetaDTO*/Integer realizacijaPredmeta) {
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public NastavnikDTO getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(NastavnikDTO nastavnik) {
		this.nastavnik = nastavnik;
	}

	public TipNastaveDTO getTipNastave() {
		return tipNastave;
	}

	public void setTipNastave(TipNastaveDTO tipNastave) {
		this.tipNastave = tipNastave;
	}

}
