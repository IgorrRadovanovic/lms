package nastavniciApp.model.dto;



import java.util.ArrayList;
import java.util.List;


public class TipNastaveDTO {
	

	private Integer id;

	private String naziv;

	private List<NastavnikNaRealizacijiDTO> realizacije = new ArrayList<NastavnikNaRealizacijiDTO>();

	public TipNastaveDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipNastaveDTO(Integer id,String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<NastavnikNaRealizacijiDTO> getRealizacije() {
		return realizacije;
	}

	public void setRealizacije(List<NastavnikNaRealizacijiDTO> realizacije) {
		this.realizacije = realizacije;
	}
	
}
