package nastavniciApp.model.dto;

public class ZvanjeDTO {
	

	private Integer id;
	
	private NaucnaOblastDTO oblast;
	
	private TipZvanjaDTO tip;

	private String datum_izbora;

	private String datum_isteka;
	
	private NastavnikDTO nosilac_zvanja;
	public ZvanjeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ZvanjeDTO(Integer id,NaucnaOblastDTO oblast, TipZvanjaDTO tip, String datum_izbora, String datum_isteka) {
		super();
		this.id = id;
		this.oblast = oblast;
		this.tip = tip;
		this.datum_izbora = datum_izbora;
		this.datum_isteka = datum_isteka;
	}
	public NaucnaOblastDTO getOblast() {
		return oblast;
	}
	public void setOblast(NaucnaOblastDTO oblast) {
		this.oblast = oblast;
	}
	public TipZvanjaDTO getTip() {
		return tip;
	}
	public void setTip(TipZvanjaDTO tip) {
		this.tip = tip;
	}
	public String getDatum_izbora() {
		return datum_izbora;
	}
	public void setDatum_izbora(String datum_izbora) {
		this.datum_izbora = datum_izbora;
	}
	public String getDatum_isteka() {
		return datum_isteka;
	}
	public void setDatum_isteka(String datum_isteka) {
		this.datum_isteka = datum_isteka;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public NastavnikDTO getNosilac_zvanja() {
		return nosilac_zvanja;
	}
	public void setNosilac_zvanja(NastavnikDTO nosilac_zvanja) {
		this.nosilac_zvanja = nosilac_zvanja;
	}

}
