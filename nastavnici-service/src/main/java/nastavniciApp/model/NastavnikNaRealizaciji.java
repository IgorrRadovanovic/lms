package nastavniciApp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class NastavnikNaRealizaciji {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private int broj_casova;
	@Column()
	private Integer realizacijaPredmetaId;
	@ManyToOne()
	private Nastavnik nastavnik;
	@ManyToOne()
	private TipNastave tipNastave;

	public NastavnikNaRealizaciji() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NastavnikNaRealizaciji(Integer id, int broj_casova, /*RealizacijaPredmeta*/Integer realizacijaPredmeta,
			Nastavnik nastavnik, TipNastave tipNastave) {
		super();
		this.id = id;
		this.broj_casova = broj_casova;
		this.realizacijaPredmetaId = realizacijaPredmeta;
		this.nastavnik = nastavnik;
		this.tipNastave = tipNastave;
	}

	public NastavnikNaRealizaciji(Integer id, int broj_casova) {
		super();
		this.id = id;
		this.broj_casova = broj_casova;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getBroj_casova() {
		return broj_casova;
	}

	public void setBroj_casova(int broj_casova) {
		this.broj_casova = broj_casova;
	}

	public /*RealizacijaPredmeta*/Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(/*RealizacijaPredmeta*/Integer realizacijaPredmeta) {
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public TipNastave getTipNastave() {
		return tipNastave;
	}

	public void setTipNastave(TipNastave tipNastave) {
		this.tipNastave = tipNastave;
	}

}
