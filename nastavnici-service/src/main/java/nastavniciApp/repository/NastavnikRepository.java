package nastavniciApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import nastavniciApp.model.Nastavnik; 
 public interface NastavnikRepository extends CrudRepository<Nastavnik, Integer> {

}