package nastavniciApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import nastavniciApp.model.TipNastave; 
 public interface TipNastaveRepository extends CrudRepository<TipNastave, Integer> {

}