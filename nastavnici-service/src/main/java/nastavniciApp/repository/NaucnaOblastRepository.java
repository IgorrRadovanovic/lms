package nastavniciApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import nastavniciApp.model.NaucnaOblast; 
 public interface NaucnaOblastRepository extends CrudRepository<NaucnaOblast, Integer> {

}