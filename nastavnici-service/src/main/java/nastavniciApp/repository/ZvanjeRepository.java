package nastavniciApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import nastavniciApp.model.Zvanje; 
 public interface ZvanjeRepository extends CrudRepository<Zvanje, Integer> {

}