package nastavniciApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import nastavniciApp.model.TipZvanja; 
 public interface TipZvanjaRepository extends CrudRepository<TipZvanja, Integer> {

}