export const environment = {
    production: false,
    title: "LMS",
    keycloak: {
        url: 'http://172.23.19.72:46165',
        realm: 'LMS',
        clientId: 'LMSFrontEnd',
    },
    idleConfig: { idle: 10, timeout: 60, ping: 10 },
}