import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikTerminiSviComponent } from './nastavnik-termini-svi.component';

describe('NastavnikTerminiSviComponent', () => {
  let component: NastavnikTerminiSviComponent;
  let fixture: ComponentFixture<NastavnikTerminiSviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikTerminiSviComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikTerminiSviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
