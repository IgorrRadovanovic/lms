import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikTerminJedanComponent } from './nastavnik-termin-jedan.component';

describe('NastavnikTerminJedanComponent', () => {
  let component: NastavnikTerminJedanComponent;
  let fixture: ComponentFixture<NastavnikTerminJedanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikTerminJedanComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikTerminJedanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
