import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikTerminiComponent } from './nastavnik-termini.component';

describe('NastavnikTerminiComponent', () => {
  let component: NastavnikTerminiComponent;
  let fixture: ComponentFixture<NastavnikTerminiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikTerminiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikTerminiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
