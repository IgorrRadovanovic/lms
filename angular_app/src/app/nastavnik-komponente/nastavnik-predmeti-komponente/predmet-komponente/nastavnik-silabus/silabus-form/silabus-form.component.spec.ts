import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SilabusFormComponent } from './silabus-form.component';

describe('SilabusFormComponent', () => {
  let component: SilabusFormComponent;
  let fixture: ComponentFixture<SilabusFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SilabusFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SilabusFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
