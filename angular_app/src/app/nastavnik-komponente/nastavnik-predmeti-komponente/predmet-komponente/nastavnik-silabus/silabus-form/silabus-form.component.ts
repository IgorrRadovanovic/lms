import { Component, OnInit } from '@angular/core';
import { GenericFormComponent } from '../../../../../generic-form/generic-form.component';
import { NgIf } from '@angular/common';
import { GenericService } from '../../../../../services/generic/generic.service';
import { Ishod } from '../../../../../model/ishod';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { Predmet } from '../../../../../model/predmet';
import { InputDetail } from '../../../../../generic-form/input-detail';
import { ObrazovniCilj } from '../../../../../model/obrazovniCilj';
import { Silabus } from '../../../../../model/silabus';

@Component({
  selector: 'app-silabus-form',
  standalone: true,
  imports: [GenericFormComponent,NgIf],
  templateUrl: './silabus-form.component.html',
  styleUrl: './silabus-form.component.css'
})
export class SilabusFormComponent implements OnInit {



  keycloakService: KeycloakService;
  ishod: Ishod = {
    id:-1,
    naziv:"",
    silabus:{id:-1,ishodi:[]},
    silabusId:-1,
    obrazovniCilj:{id:-1,opis:"",ishodi:[]},
    obrazovniCiljId:-1,
    terminiNastave:[]
  };
  ishodId: any;
  sviCiljevi: ObrazovniCilj[]=[];

  inputDetails: InputDetail[] = [

  ];
  forma: FormGroup<any> | undefined;
  
  constructor(
    protected kcs:KeycloakService,
    private translate: TranslateService,
    private ishodServ:GenericService<Ishod>,
    private silServ:GenericService<Silabus>,
    private ocServ:GenericService<ObrazovniCilj>,
    private subServ:GenericService<Predmet>,
    private route: ActivatedRoute,
    private router:Router,
    private formBuilder: FormBuilder,){

    this.keycloakService = kcs;
    this.route.paramMap.subscribe((params)=>{
      let idString: string|null = params.get("ishodId")
      if(idString!=null){
        ishodServ.getById(parseInt(idString),"ishodi").subscribe(res=>{
          console.log(res);
          this.ishod = res;
        })
        
      }
    })
  }
  ngOnInit(): void {
    this.getIshodForm()
  }

  getIshodForm(){
    this.route.params.subscribe(params=>{
      this.ishodId = params["ishodId"]
      if(this.ishodId!= "new"){
      this.ishodServ.getById(this.ishodId,"ishodi").subscribe(ishod=>{
        this.ishod = ishod
        this.ocServ.getAll("obrazovniciljevi").subscribe(ciljevi=>{
          console.log(ciljevi)
          for(let cilj of ciljevi){
            this.sviCiljevi.push(cilj)
          }
        })
        this.inputDetails = [
          {
            input_type: "text",
            input_label: "Naziv",
          },
          {
           input_type:"select-object" ,
           input_label:"obrazovniCilj",
           input_options:this.sviCiljevi,
           input_value:this.ishod.obrazovniCilj
          }];
        this.forma = this.formBuilder.group({
          naziv:this.ishod.naziv,
          obrazovniCilj:this.ishod.obrazovniCilj
        })
        })
      }else{
        console.log(this.ocServ.getAll("obrazovniciljevi"))
        this.ocServ.getAll("obrazovniciljevi").subscribe((ciljevi: ObrazovniCilj[])=>{
          console.log("Entered")
          for(let cilj of ciljevi){
            console.log("Cilj")
            this.sviCiljevi.push(cilj)
          }
          console.log("ciljevi")
          console.log(this.sviCiljevi)
          this.inputDetails = [
            {
              input_type: "text",
              input_label: "Naziv",
            },
            {
             input_type:"select-object" ,
             input_label:"obrazovniCilj",
             input_options:this.sviCiljevi
            }];
          this.forma = this.formBuilder.group({
            naziv:this.ishod.naziv,
            obrazovniCilj:this.ishod.obrazovniCilj
          })
        })
        
      }
    })
    }
  

  submit() {
    if(this.ishodId && this.ishod){
      this.ishodServ.patch(this.ishodId,"ishodi",this.ishod).subscribe(resp=>{
        this.router.navigate(["../main"],{
          relativeTo:this.route
        })
      })
    }else{
      let subId :any ;
      this.route.parent?.parent?.params.subscribe(params=>{
        subId = params["subId"]
        this.subServ.getById(parseInt(subId),"predmeti").subscribe(predmet=>{
          this.silServ.getById(predmet.silabus.id,"silabusi").subscribe(silabus=>{
            this.ishod.silabus = silabus;
            this.ishod.silabusId = silabus.id;
            this.ishodServ.post("ishodi",this.ishod).subscribe(res=>{
              this.router.navigate(["../main"],{
                relativeTo:this.route
              })
            })
          })
        })
      })
    }
    }
}
