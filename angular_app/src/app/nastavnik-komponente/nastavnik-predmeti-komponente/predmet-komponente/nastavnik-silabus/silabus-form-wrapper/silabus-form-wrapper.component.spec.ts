import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SilabusFormWrapperComponent } from './silabus-form-wrapper.component';

describe('SilabusFormWrapperComponent', () => {
  let component: SilabusFormWrapperComponent;
  let fixture: ComponentFixture<SilabusFormWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SilabusFormWrapperComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SilabusFormWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
