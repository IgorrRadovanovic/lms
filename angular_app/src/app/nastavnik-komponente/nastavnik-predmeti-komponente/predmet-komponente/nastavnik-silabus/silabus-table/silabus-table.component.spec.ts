import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SilabusTableComponent } from './silabus-table.component';

describe('SilabusTableComponent', () => {
  let component: SilabusTableComponent;
  let fixture: ComponentFixture<SilabusTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SilabusTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SilabusTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
