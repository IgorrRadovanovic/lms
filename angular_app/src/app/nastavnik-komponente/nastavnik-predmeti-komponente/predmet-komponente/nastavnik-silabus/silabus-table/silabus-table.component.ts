import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GenericService } from '../../../../../services/generic/generic.service';
import { MappingService } from '../../../../../services/mappingService/mapping.service';
import { AgGridAngular } from "ag-grid-angular";
import { ColDef, RowClickedEvent } from "ag-grid-community";
import { ButtonRendererComponent } from '../../../nastavnik-predmeti-svi/nast-predmet-table/button-renderer.component';
import { SharedService } from '../../../../../services/shared.service';
import { KeycloakService } from 'keycloak-angular';
import { Predmet } from '../../../../../model/predmet';
import { ActivatedRoute } from '@angular/router';
import { NasnastavnikPredmetService } from '../../../../../services/nastavnik-predmet/nasnastavnik-predmet.service';
import { Silabus } from '../../../../../model/silabus';
import { Ishod } from '../../../../../model/ishod';
import { forEach } from 'lodash';


@Component({
  selector: 'app-silabus-table',
  standalone: true,
  imports: [AgGridAngular],
  templateUrl: './silabus-table.component.html',
  styleUrl: './silabus-table.component.css'
})
export class SilabusTableComponent<T extends object> implements OnInit {
  rowData: any[] = [];
  colDefs: ColDef[] = [];
  predmetId: any
  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
    resizable: false,
  };
  @Input()
  univerzitetNaziv: String | null = "";

  @Input()
  table: string = "";

  @Input()
  colLimit: string[] = [""];

  @Output()
  signal = new EventEmitter<string>();

  data: any[] = [];

  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];
  tableHeight: number = 525;
  keycloakService: KeycloakService;

  ishodi:Ishod[]=[];

  constructor(
    private sharedService: SharedService,
    private translate: TranslateService, 
    private genService: GenericService<T>, 
    private mappingService: MappingService,
    private nasSubService: NasnastavnikPredmetService,
    private changeDetectorRef: ChangeDetectorRef,
    private route:ActivatedRoute,
    private predmetService: GenericService<Predmet>,
    kcs:KeycloakService) {
      this.keycloakService = kcs
    }
  ngOnInit(): void {
    this.getData();
    this.getColumns();
  }
  getData() {
    this.predmetId = this.route.parent?.parent?.params.subscribe(p=>{
      this.predmetId = p['subId']
      this.nasSubService.getSilabusForPredmet(parseInt(this.predmetId)).then((silabus:Silabus)=>{
        console.log("Silabus")
        console.log(silabus)
        for(let ishod of silabus.ishodi){
          console.log("Ishod")
          console.log(ishod)
          this.data.push(ishod)
          this.rowData = this.data.map((item) => {
            let mappedItem: { [key: string]: any } = {};
            Object.keys(item).forEach((key) => {
              mappedItem[key] = item[key];
            });
            return mappedItem;
          });
          
        }
        console.log(this.rowData)
      })
    });

    
  }
  getColumns() {
    this.genService.getColumns(this.table).subscribe((keys) => {
      this.colDefs = keys
        .filter((key) => this.colLimit.includes(key))
        .map((key) => {
          this.colDefs = [];
          const formatterName = `${key}Formatter`;
          return {
            field: key,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
          };
        });

      this.colDefs.push({
        headerName: "Pregledaj",
        cellRenderer: ButtonRendererComponent,
      });
      this.changeDetectorRef.detectChanges();
    });
  }

  pregledajIshod(rowData: any) {
    let ishodId = rowData.data.id
    this.signal.emit(ishodId)
  }

}
