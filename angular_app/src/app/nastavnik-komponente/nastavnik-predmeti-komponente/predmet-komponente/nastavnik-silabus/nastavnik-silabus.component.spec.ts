import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikSilabusComponent } from './nastavnik-silabus.component';

describe('NastavnikSilabusComponent', () => {
  let component: NastavnikSilabusComponent;
  let fixture: ComponentFixture<NastavnikSilabusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikSilabusComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikSilabusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
