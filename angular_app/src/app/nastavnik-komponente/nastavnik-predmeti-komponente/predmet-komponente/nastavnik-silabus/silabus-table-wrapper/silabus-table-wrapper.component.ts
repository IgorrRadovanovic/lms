import { Component, EventEmitter, Output } from '@angular/core';
import { SilabusTableComponent } from '../silabus-table/silabus-table.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-silabus-table-wrapper',
  standalone: true,
  imports: [SilabusTableComponent],
  templateUrl: './silabus-table-wrapper.component.html',
  styleUrl: './silabus-table-wrapper.component.css'
})
export class SilabusTableWrapperComponent {
newIshod() {
  this._router.navigate(["../new"],{
    relativeTo:this.route
  })
}
  constructor(private _router: Router,private route:ActivatedRoute) { }
receiveViewSignal($event: string) {
  let id = $event
  console.log(this.route.url)
  this._router.navigate(["../"+id],{
    relativeTo:this.route
  })
}

}
