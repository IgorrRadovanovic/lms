import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SilabusTableWrapperComponent } from './silabus-table-wrapper.component';

describe('SilabusTableWrapperComponent', () => {
  let component: SilabusTableWrapperComponent;
  let fixture: ComponentFixture<SilabusTableWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SilabusTableWrapperComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SilabusTableWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
