import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstrumentEvaluacijeJedanComponent } from './instrument-evaluacije-jedan.component';

describe('InstrumentEvaluacijeJedanComponent', () => {
  let component: InstrumentEvaluacijeJedanComponent;
  let fixture: ComponentFixture<InstrumentEvaluacijeJedanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [InstrumentEvaluacijeJedanComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InstrumentEvaluacijeJedanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
