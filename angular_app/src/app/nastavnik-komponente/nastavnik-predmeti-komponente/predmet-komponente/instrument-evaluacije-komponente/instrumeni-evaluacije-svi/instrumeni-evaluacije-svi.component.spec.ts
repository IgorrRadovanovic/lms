import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstrumeniEvaluacijeSviComponent } from './instrumeni-evaluacije-svi.component';

describe('InstrumeniEvaluacijeSviComponent', () => {
  let component: InstrumeniEvaluacijeSviComponent;
  let fixture: ComponentFixture<InstrumeniEvaluacijeSviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [InstrumeniEvaluacijeSviComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InstrumeniEvaluacijeSviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
