import { Component } from '@angular/core';
import { GenericFormComponent } from "../../../../generic-form/generic-form.component";
import { InputDetail } from '../../../../generic-form/input-detail';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenericService } from '../../../../services/generic/generic.service';
import { Predmet } from '../../../../model/predmet';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-predmet-jedan',
  standalone: true,
  imports: [GenericFormComponent,NgIf],
  templateUrl: './predmet-jedan.component.html',
  styleUrl: './predmet-jedan.component.css'
})
export class PredmetJedanComponent {
  keycloakService: KeycloakService;
  predmet: Predmet|undefined;
  predmetId: any;
  sviPredmeti: Predmet[]=[];
  
  constructor(
    protected kcs:KeycloakService,
    private translate: TranslateService,
    private serv:GenericService<Predmet>,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,){

    this.keycloakService = kcs;
    this.route.paramMap.subscribe((params)=>{
      let idString: string|null = params.get("predmetId")
      if(idString!=null){
        serv.getById(parseInt(idString),"predmeti").subscribe(res=>{
          console.log(res);
          this.predmet = res;
        })
        
      }
    })
  }
  
  ngOnInit() {
    console.log("Init")
    this.route.parent?.params.subscribe((params)=>{
      
      let idString: string|null = params['subId'];
      
      if(idString!=null){
        this.serv.getById(parseInt(idString),"predmeti").subscribe(res=>{
          console.log(res);
          this.predmet = res;
          this.getPredmetForm();
          console.log("Detalji: "+this.inputDetails)
        })}})
    
  }
  inputDetails: InputDetail[] = [

  ];
  forma: FormGroup<any> | undefined;

  getPredmetForm(){
    this.route.parent?.params.subscribe(params => {
      this.predmetId = params["subId"];
      if (true) {
        this.serv.getAll("predmeti").subscribe(predmeti=>{
          this.sviPredmeti = predmeti;
          for(let curr_predmet of this.sviPredmeti){
            if(curr_predmet.id == this.predmetId){
              this.predmet = curr_predmet;
              let index = this.sviPredmeti.indexOf(this.predmet,0);
              if(index > -1){
                this.sviPredmeti.splice(index,1);
              }
            }
          }
          this.inputDetails = [
            {
              input_type: "text",
              input_label: "Naziv",
            },
            {
              input_type: "number",
              input_label: "ESPB",
            },
            {
              input_type: "bool",
              input_label: "Obavezan",
            },
            {
              input_type: "number",
              input_label: "brojPredavanja",
            },
            {
              input_type: "number",
              input_label: "drugiObliciNastave",
            },
            {
              input_type: "number",
              input_label: "istrazivackiRad",
            },
            {
              input_type: "number",
              input_label: "ostaliCasovi",
            },
            {
              input_type: "select-object",
              input_label: "preduslov",
              input_options: this.sviPredmeti
            }
            
          ];
          console.log(this.predmet)
          this.forma = this.formBuilder.group({
            naziv:this.predmet?.naziv,
            espb:this.predmet?.espb,
            obavezan:this.predmet?.obavezan,
            brojPredavanja:this.predmet?.brojPredavanja,
            drugiObliciNastave:this.predmet?.drugiObliciNastave,
            istrazivackiRad:this.predmet?.istrazivackiRad,
            ostaliCasovi:this.predmet?.ostaliCasovi,
            preduslov:this.predmet?.preduslov

          })
          console.log(this.formBuilder.group({
            naziv:this.predmet?.naziv,
            espb:this.predmet?.espb,
            obavezan:this.predmet?.obavezan,
            brojPredavanja:this.predmet?.brojPredavanja,
            drugiObliciNastave:this.predmet?.drugiObliciNastave,
            istrazivackiRad:this.predmet?.istrazivackiRad,
            ostaliCasovi:this.predmet?.ostaliCasovi,
            preduslov:this.predmet?.preduslov})
    )});
      }
    })
  }

}
