import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredmetJedanComponent } from './predmet-jedan.component';

describe('PredmetJedanComponent', () => {
  let component: PredmetJedanComponent;
  let fixture: ComponentFixture<PredmetJedanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PredmetJedanComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PredmetJedanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
