import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-nastavnik-predmet',
  standalone: true,
  imports: [RouterLink,RouterOutlet],
  templateUrl: './nastavnik-predmet.component.html',
  styleUrl: './nastavnik-predmet.component.css'
})
export class NastavnikPredmetComponent {

}
