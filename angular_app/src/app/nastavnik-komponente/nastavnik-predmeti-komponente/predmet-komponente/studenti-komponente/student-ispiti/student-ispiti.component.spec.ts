import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentIspitiComponent } from './student-ispiti.component';

describe('StudentIspitiComponent', () => {
  let component: StudentIspitiComponent;
  let fixture: ComponentFixture<StudentIspitiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentIspitiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentIspitiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
