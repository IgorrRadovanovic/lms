import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentiKojiPohadjajuComponent } from './studenti-koji-pohadjaju.component';

describe('StudentiKojiPohadjajuComponent', () => {
  let component: StudentiKojiPohadjajuComponent;
  let fixture: ComponentFixture<StudentiKojiPohadjajuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentiKojiPohadjajuComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentiKojiPohadjajuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
