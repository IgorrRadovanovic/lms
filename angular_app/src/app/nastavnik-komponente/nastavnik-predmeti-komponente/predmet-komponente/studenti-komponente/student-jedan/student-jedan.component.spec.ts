import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentJedanComponent } from './student-jedan.component';

describe('StudentJedanComponent', () => {
  let component: StudentJedanComponent;
  let fixture: ComponentFixture<StudentJedanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudentJedanComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudentJedanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
