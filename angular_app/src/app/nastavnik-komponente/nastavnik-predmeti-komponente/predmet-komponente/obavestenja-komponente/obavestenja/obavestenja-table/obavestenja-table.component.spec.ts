import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObavestenjaTableComponent } from './obavestenja-table.component';

describe('ObavestenjaTableComponent', () => {
  let component: ObavestenjaTableComponent;
  let fixture: ComponentFixture<ObavestenjaTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ObavestenjaTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ObavestenjaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
