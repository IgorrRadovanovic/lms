import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { KeycloakService } from 'keycloak-angular';
import { Ishod } from '../../../../../../model/ishod';
import { Predmet } from '../../../../../../model/predmet';
import { GenericService } from '../../../../../../services/generic/generic.service';
import { MappingService } from '../../../../../../services/mappingService/mapping.service';
import { NasnastavnikPredmetService } from '../../../../../../services/nastavnik-predmet/nasnastavnik-predmet.service';
import { SharedService } from '../../../../../../services/shared.service';
import { AgGridAngular } from 'ag-grid-angular';
import { RowClickedEvent } from 'ag-grid-community';
import { ColDef} from "ag-grid-community";
import { ButtonRendererComponent } from '../../../../nastavnik-predmeti-svi/nast-predmet-table/button-renderer.component';
import { Obavestenje } from '../../../../../../model/obavestenje';

@Component({
  selector: 'app-obavestenja-table',
  standalone: true,
  imports: [AgGridAngular],
  templateUrl: './obavestenja-table.component.html',
  styleUrl: './obavestenja-table.component.css'
})
export class ObavestenjaTableComponent<T extends object> implements OnInit {

  rowData: any[] = [];
  colDefs: ColDef[] = [];
  predmetId: any
  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
    resizable: false,
  };
  @Input()
  univerzitetNaziv: String | null = "";

  @Input()
  table: string = "";

  @Input()
  colLimit: string[] = [""];

  @Output()
  signal = new EventEmitter<string>();

  data: any[] = [];

  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];
  tableHeight: number = 525;
  keycloakService: KeycloakService;

  ishodi:Ishod[]=[];

  constructor(
    private sharedService: SharedService,
    private translate: TranslateService, 
    private genService: GenericService<T>, 
    private mappingService: MappingService,
    private nasSubService: NasnastavnikPredmetService,
    private changeDetectorRef: ChangeDetectorRef,
    private route:ActivatedRoute,
    private predmetService: GenericService<Predmet>,
    private obavService: GenericService<Obavestenje>,
    kcs:KeycloakService) {
      this.keycloakService = kcs
    }
  ngOnInit(): void {
    this.getColumns()
    this.getData()
  }

  getData() {

    this.obavService.getAll("obavestenja").subscribe(obavestenja=>{
      for(let obavestenje of obavestenja){
        this.data.push(obavestenje);
        this.rowData = this.data.map((item) => {
          let mappedItem: { [key: string]: any } = {};
          Object.keys(item).forEach((key) => {
            mappedItem[key] = item[key];
          });
          return mappedItem;
        });

      }
    })

    
  }
  getColumns() {
    this.genService.getColumns(this.table).subscribe((keys) => {
      this.colDefs = keys
        .filter((key) => this.colLimit.includes(key))
        .map((key) => {
          this.colDefs = [];
          const formatterName = `${key}Formatter`;
          return {
            field: key,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
          };
        });

      this.colDefs.push({
        headerName: "Pregledaj",
        cellRenderer: ButtonRendererComponent,
      });
      this.changeDetectorRef.detectChanges();
    });
  }

  pregledajObavestenje($event: RowClickedEvent<any,any>) {
    throw new Error('Method not implemented.');
    }
}
