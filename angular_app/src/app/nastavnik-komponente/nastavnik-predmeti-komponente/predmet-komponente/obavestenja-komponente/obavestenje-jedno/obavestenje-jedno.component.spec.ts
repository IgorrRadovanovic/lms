import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObavestenjeJednoComponent } from './obavestenje-jedno.component';

describe('ObavestenjeJednoComponent', () => {
  let component: ObavestenjeJednoComponent;
  let fixture: ComponentFixture<ObavestenjeJednoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ObavestenjeJednoComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ObavestenjeJednoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
