import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IspitiSviComponent } from './ispiti-svi.component';

describe('IspitiSviComponent', () => {
  let component: IspitiSviComponent;
  let fixture: ComponentFixture<IspitiSviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IspitiSviComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IspitiSviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
