import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IspitiPolaganjaComponent } from './ispiti-polaganja.component';

describe('IspitiPolaganjaComponent', () => {
  let component: IspitiPolaganjaComponent;
  let fixture: ComponentFixture<IspitiPolaganjaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IspitiPolaganjaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IspitiPolaganjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
