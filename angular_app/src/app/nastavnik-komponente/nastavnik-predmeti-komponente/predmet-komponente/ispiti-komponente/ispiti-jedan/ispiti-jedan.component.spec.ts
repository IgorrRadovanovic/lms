import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IspitiJedanComponent } from './ispiti-jedan.component';

describe('IspitiJedanComponent', () => {
  let component: IspitiJedanComponent;
  let fixture: ComponentFixture<IspitiJedanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IspitiJedanComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IspitiJedanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
