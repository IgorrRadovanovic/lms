import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastavnikPredmetiSviComponent } from './nastavnik-predmeti-svi.component';

describe('NastavnikPredmetiSviComponent', () => {
  let component: NastavnikPredmetiSviComponent;
  let fixture: ComponentFixture<NastavnikPredmetiSviComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastavnikPredmetiSviComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastavnikPredmetiSviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
