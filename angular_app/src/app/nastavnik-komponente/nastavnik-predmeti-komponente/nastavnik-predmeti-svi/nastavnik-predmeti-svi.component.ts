import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ColDef, RowClickedEvent } from "ag-grid-community";
import { Predmet } from '../../../model/predmet';
import { EvaluacijaService } from '../../../services/evaluacijaService/evaluacija.service';
import { GenericService } from '../../../services/generic/generic.service';
import { LoggedStudentService } from '../../../services/loggedStudent/logged-student.service';
import { PohadjanjaService } from '../../../services/pohadjanjaService/pohadjanja.service';
import { RealizacijeService } from '../../../services/realizacijeService/realizacije.service';
import { SharedService } from '../../../services/shared.service';
import { StudentNaGodiniService } from '../../../services/studentNaGodService/student-na-godini.service';
import { UserService } from '../../../services/userService/user.service';
import { NastPredmetTableComponent } from './nast-predmet-table/nast-predmet-table.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nastavnik-predmeti-svi',
  standalone: true,
  imports: [NastPredmetTableComponent],
  templateUrl: './nastavnik-predmeti-svi.component.html',
  styleUrl: './nastavnik-predmeti-svi.component.css'
})
export class NastavnikPredmetiSviComponent {
  constructor(private _router: Router) { }
  receiveViewSignal($event: string) {
    let id = $event
    this._router.navigateByUrl("/nastHome/mySubjects/"+id);
  }
}
