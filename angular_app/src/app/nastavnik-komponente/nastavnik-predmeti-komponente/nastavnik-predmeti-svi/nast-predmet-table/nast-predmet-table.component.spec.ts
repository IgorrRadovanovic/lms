import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NastPredmetTableComponent } from './nast-predmet-table.component';

describe('NastPredmetTableComponent', () => {
  let component: NastPredmetTableComponent;
  let fixture: ComponentFixture<NastPredmetTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NastPredmetTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NastPredmetTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
