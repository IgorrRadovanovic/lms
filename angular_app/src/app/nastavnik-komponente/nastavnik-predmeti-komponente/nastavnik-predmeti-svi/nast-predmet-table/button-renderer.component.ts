import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-button-renderer',
  template: `<button class="btn" (click)="onClick($event)">Pregledaj</button>`,
  styles: `.btn{ max-height: 30px, font-size: 0.5 }`
})
export class ButtonRendererComponent implements ICellRendererAngularComp {
  agInit(params: ICellRendererParams<any, any, any>): void {
  }
  refresh(params: ICellRendererParams<any, any, any>): boolean {
      return false;
  }
  onClick(event: any) {
    alert('Button clicked!');
  }
}
