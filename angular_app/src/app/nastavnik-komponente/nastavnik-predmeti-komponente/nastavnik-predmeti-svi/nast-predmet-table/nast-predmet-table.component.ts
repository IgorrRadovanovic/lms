import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GenericService } from '../../../../services/generic/generic.service';
import { MappingService } from '../../../../services/mappingService/mapping.service';
import { AgGridAngular } from "ag-grid-angular";
import { ColDef, RowClickedEvent } from "ag-grid-community";
import { ButtonRendererComponent } from './button-renderer.component';
import { SharedService } from '../../../../services/shared.service';
import { KeycloakService } from 'keycloak-angular';
import { Predmet } from '../../../../model/predmet';

@Component({
  selector: 'app-nast-predmet-table',
  standalone: true,
  imports: [AgGridAngular],
  templateUrl: './nast-predmet-table.component.html',
  styleUrl: './nast-predmet-table.component.css'
})
export class NastPredmetTableComponent<T extends object> implements OnInit {

  keycloakService: KeycloakService;
  userId: number | any;

  constructor(
    private sharedService: SharedService,
    private translate: TranslateService, 
    private genService: GenericService<T>, 
    private mappingService: MappingService,
    private changeDetectorRef: ChangeDetectorRef,
    private predmetService: GenericService<Predmet>,
    kcs:KeycloakService) {
      this.keycloakService = kcs
    }
  
  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];

  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
    resizable: false,
  };

  @Input()
  univerzitetNaziv: String | null= "";

  @Input()
  table: string = "";

  @Input()
  colLimit: string[] = [""];

  @Output()
  menuChoice = new EventEmitter<boolean>();
  silabus: boolean = false;

  @Output()
  signal = new EventEmitter<string>();

  @Input()
  dataLimit: { [key: string]: number } = {};

  @Input()
  dataLimit2: string = "";

  data: any[] = [];
  tableHeight: number = 525;
  columns: { field: string }[] = [];
  colDefs: ColDef[] = [];
  rowData: any[] = [];
  dataLimitKey: string = "";
  date: Date = new Date();


  ngOnInit(): void {
    this.keycloakService.loadUserProfile().then((p) => {
      if (p.attributes) {
        this.userId = p.attributes["userDataID"];
        console.log("User data id: "+this.userId)
      }
      this.getData();
      this.getColumns();
    });

  }
  getColumns() {
    this.genService.getColumns(this.table).subscribe((keys) => {
      this.colDefs = keys
        .filter((key) => this.colLimit.includes(key))
        .map((key) => {
          this.colDefs = [];
          const formatterName = `${key}Formatter`;
          return {
            field: key,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
          };
        });

      this.colDefs.push({
        headerName: "Pregledaj",
        cellRenderer: ButtonRendererComponent,
      });
      this.changeDetectorRef.detectChanges();
    });
  }
  getData() {
    
    this.sharedService.getPremetiIdForNastavnik(this.userId).then((predmetiId: number[]) => {
      for(let id of predmetiId){
        console.log(id)
        this.predmetService.getById(id, "predmeti").subscribe((item) => {
          if (/*item.godinaStudija.godina == this.date.getFullYear()*/true) {
            this.data.push(item);
            this.rowData = this.data.map((item) => {
              let mappedItem: { [key: string]: any } = {};
              Object.keys(item).forEach((key) => {
                mappedItem[key] = item[key];
              });
              return mappedItem;
            });
          }
        });
      }
      // predmetiId.forEach((studPredmetiID) => {
      //   this.predmetService.getById(studPredmetiID, "predmeti").subscribe((item) => {
      //     if (item.godinaStudija.godina == this.date.getFullYear()) {
      //       this.data.push(item);
      //       this.rowData = this.data.map((item) => {
      //         let mappedItem: { [key: string]: any } = {};
      //         Object.keys(item).forEach((key) => {
      //           mappedItem[key] = item[key];
      //         });
      //         return mappedItem;
      //       });
      //     }
      //   });
      // });
      this.adjustTableHeight();
    });
  }

  adjustTableHeight() {
    const rowCount = this.rowData.length;
    this.tableHeight = rowCount >= 10 || rowCount <= 3 ? 250 : (rowCount / 8) * 525;
  }

  pregledajPredmet(rowData: any) {
    let predmetId = rowData.data.id;
    this.signal.emit(predmetId)
    }
}
