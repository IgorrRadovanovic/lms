import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-nastavnik-predmeti',
  standalone: true,
  imports: [RouterLink,RouterOutlet],
  templateUrl: './nastavnik-predmeti.component.html',
  styleUrl: './nastavnik-predmeti.component.css'
})
export class NastavnikPredmetiComponent {

  
}
