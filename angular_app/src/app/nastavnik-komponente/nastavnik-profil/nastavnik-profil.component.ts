import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { KeycloakService } from 'keycloak-angular';
import { Nastavnik } from '../../model/nastavnik';
import { Student } from '../../model/student';
import { GenericService } from '../../services/generic/generic.service';
import { SharedService } from '../../services/shared.service';
import { InputDetail } from '../../generic-form/input-detail';
import { GenericFormComponent } from "../../generic-form/generic-form.component";
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-nastavnik-profil',
  standalone: true,
  imports: [GenericFormComponent,NgIf],
  templateUrl: './nastavnik-profil.component.html',
  styleUrl: './nastavnik-profil.component.css'
})
export class NastavnikProfilComponent {

  student: Student | null = null;
  nastavnik: Nastavnik | null = null;
  inputDetails: InputDetail[] = [];
  forma: FormGroup | undefined;
  userId: number | any = null;

  constructor(
    private sharedService: SharedService,
    private kcService: KeycloakService,
    private formBuilder: FormBuilder,
    private nastavnikService: GenericService<Nastavnik>,
    private studService: GenericService<Student>
  ) {}


  ngOnInit(): void {
    this.kcService.loadUserProfile().then((p) => {
      console.log("profil")
      console.log(p);
      if (p.attributes) {
        this.userId = p.attributes["userDataID"];
        console.log(this.userId)
        
      }
    });
    this.kcService.getToken().then((t) => {
      const payload = this.sharedService.parseJwt(t);
      if (payload.realm_access.roles.includes("student")) {
        this.getStudentForm();
      } else if (payload.realm_access.roles.includes("teacher")) {
        this.getNastavnikForm();
      } else if (payload.realm_access.roles.includes("administrator")) {
        // TODO
      }
    });
  }

  getStudentForm(): void {
    this.studService.getById(1, "studenti").subscribe((s) => {
      this.student = s;
      // console.log(this.student);
      this.inputDetails = [
        {
          input_type: "text",
          input_label: "Ime",
        },
        {
          input_type: "number",
          input_label: "JMBG",
        },
        {
          input_type: "email",
          input_label: "Email",
        },
        {
          input_type: "text",
          input_label: "Broj",
        },
      ];
      this.forma = this.formBuilder.group({
        ime: this.student.ime,
        jmbg: this.student.jmbg,
        email: this.student.email,
        brojTelefona: this.student.brojTelefona,
      });
    });
  }

  getNastavnikForm(): void {
    this.nastavnikService.getById(this.userId, "nastavnici").subscribe((n) => {
      this.nastavnik = n;
      this.inputDetails = [
        {
          input_type: "text",
          input_label: "Ime",
        },
        {
          input_type: "number",
          input_label: "JMBG",
        },
        {
          input_type: "textarea",
          input_label: "Biografija",
        },
      ];
      this.forma = this.formBuilder.group({
        ime: this.nastavnik.ime,
        jmbg: this.nastavnik.jmbg,
        biografija: this.nastavnik.biografija,
      });
    });
  }
}
