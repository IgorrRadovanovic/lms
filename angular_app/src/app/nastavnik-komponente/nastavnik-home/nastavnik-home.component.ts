import { Component } from "@angular/core";
import { RouterLink, RouterOutlet } from "@angular/router";
import { UserService } from "../../services/userService/user.service";
import { KeycloakService } from "keycloak-angular";
import { NgIf } from "@angular/common";

@Component({
  selector: "app-nastavnik-home",
  standalone: true,
  imports: [RouterLink, RouterOutlet, NgIf],
  templateUrl: "./nastavnik-home.component.html",
  styleUrl: "./nastavnik-home.component.css",
})
export class NastavnikHomeComponent {
  
  userId: number | null = null;
  constructor(private userService: UserService, private kcService: KeycloakService) {}

  ngOnInit() {
    this.userService.getUserId().then((e) => (this.userId = e));
  }

  logout() {
    this.kcService.logout("http://localhost:4200/");
  }
  showNastavnik() {
    throw new Error("Method not implemented.");
  }
}
