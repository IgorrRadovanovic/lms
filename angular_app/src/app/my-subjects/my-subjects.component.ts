import { Component, OnInit } from "@angular/core";
import { RouterLink } from "@angular/router";
import { NgIf } from "@angular/common";

import { SubjectTableComponent } from "./subject-table/subject-table.component";

@Component({
  selector: "app-my-subjects",
  standalone: true,
  imports: [SubjectTableComponent, RouterLink, NgIf],
  templateUrl: "./my-subjects.component.html",
  styleUrl: "./my-subjects.component.css",
})
export class MySubjectsComponent implements OnInit {
  constructor() {}
  prijavljen: boolean = false;
  predmetiId: number[] = [];
  ngOnInit(): void {}

  receiveIspitSignal(message: string) {
    console.log('test');
    alert(message);
  }

}
