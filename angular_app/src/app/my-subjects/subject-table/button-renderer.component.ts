import { Component, EventEmitter, Output } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-button-renderer',
  template: `<button class="btn" (click)="onClick($event)">Пријави испит</button>`,
  styles: `.btn{ max-height: 30px, font-size: 0.5 }`
})
export class ButtonRendererComponent implements ICellRendererAngularComp {
  private params: any;

  @Output()
  rowClicked: EventEmitter<any> = new EventEmitter();

  agInit(params: ICellRendererParams<any, any, any>): void {
    this.params = params;
  }

  refresh(params: ICellRendererParams<any, any, any>): boolean {
    return false;
  }

  onClick(event: any) {
   this.rowClicked.emit(this.params.data); // This will log the row data
  }
}
