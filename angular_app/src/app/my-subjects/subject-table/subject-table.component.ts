import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, Output, EventEmitter, SimpleChanges } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { ColDef } from "ag-grid-community";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";
import { GenericService } from "../../services/generic/generic.service";
import { MappingService } from "../../services/mappingService/mapping.service";
import { ButtonRendererComponent } from "./button-renderer.component";
import { Predmet } from "../../model/predmet";
import { __await } from "tslib";
import { LoggedStudentService } from "../../services/loggedStudent/logged-student.service";
import { StudentNaGodiniService } from "../../services/studentNaGodService/student-na-godini.service";
import { PohadjanjaService } from "../../services/pohadjanjaService/pohadjanja.service";
import { firstValueFrom } from "rxjs";
import { RealizacijaPredmeta } from "../../model/realizacijaPredmeta";
import { RealizacijeService } from "../../services/realizacijeService/realizacije.service";
import { UserService } from "../../services/userService/user.service";
import { EvaluacijaZnanja, EvaluacijaZnanjaZaBazu } from "../../model/evaluacijaZnanja";
import { EvaluacijaService } from "../../services/evaluacijaService/evaluacija.service";
import { InstrumentEvaluacije } from "../../model/instrumentEvaluacije";
import { TipEvaluacije } from "../../model/tipEvaluacije";
import { SharedService } from "../../services/sharedService/shared.service";

@Component({
  selector: "app-subject-table",
  standalone: true,
  imports: [AgGridAngular],
  template: `
    <ag-grid-angular
      class="ag-theme-quartz"
      [style.height.px]="tableHeight"
      [rowData]="rowData"
      [columnDefs]="colDefs"
      [defaultColDef]="defaultColDef"
      [pagination]="pagination"
      [paginationPageSize]="paginationPageSize"
      [paginationPageSizeSelector]="paginationPageSizeSelector"
      (rowClicked)="prijavaIspita($event)"
      >/>
    </ag-grid-angular>
  `,
  styleUrl: "./subject-table.component.css",
})
export class SubjectTableComponent<T extends object> implements OnInit {
  constructor(
    private myStudService: LoggedStudentService,
    private sharedService: SharedService,
    private genService: GenericService<T>,
    private predmetService: GenericService<Predmet>,
    private pohadjanjaService: PohadjanjaService,
    private studGodService: StudentNaGodiniService,
    private userService: UserService,
    private relService: RealizacijeService,
    private changeDetectorRef: ChangeDetectorRef,
    private evService: EvaluacijaService
  ) {}

  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];

  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
  };

  @Input()
  table: string = "";

  @Input()
  colLimit: string[] = [""];
  @Output()
  signal = new EventEmitter<string>();
  data: any[] = [];
  tableHeight: number = 525;
  columns: { field: string }[] = [];
  colDefs: ColDef[] = [];
  rowData: any[] = [];
  dataLimitKey: string = "id";
  date: Date = new Date();
  userId: number | null = null;
  async ngOnInit() {
    await this.userService.getUserId().then((uid) => {
      if (uid) {
        this.userId = uid;
      }
    });
    this.getData();
    this.getColumns();
  }
  async prijavaIspita(rowData: any) {
    let predmetId = rowData.data.id;
    let predmet = await firstValueFrom(this.predmetService.getById(predmetId, "predmeti"));
    // studijski program tog predmeta
    let studProId = predmet.studijskiProgram.id;
    // student na godini po studId i studijskiProgramID
    if (this.userId) {
      let studGod = await firstValueFrom(this.studGodService.getBySsp("studentinagodini", this.userId, studProId));
      let studGodId = studGod[0].id;
      console.log(studGodId, "stud god id");
      if (this.sharedService.isIspitniRokActive(predmetId)) { // Stub
        // sve realizacije jednog Predmeta
        // medju njima pronaci koji imaju studGod.id u pohadjanju i predmetId u realizaciji
        const realizacije = await firstValueFrom(this.relService.getByPredmetID("realizacijepredmeta", predmetId)); // sve realizacije tog predmeta
        const pohadjanja = await firstValueFrom(this.pohadjanjaService.getByStudGodID("pohadjanjapredmeta", studGodId)); //sva pohadjanja tog studenta
        // const evaluacije = await firstValueFrom(this.evService.getAll("evaluacijeznanja"));
        const instrument = await firstValueFrom(this.genService.getById(4, "instrumentievaluacije"));
        const tip = await firstValueFrom(this.genService.getById(4, "tipovievaluacije"));

        for (const p of pohadjanja) {
          for (const r of realizacije) {
            if (p.realizacijaPredmetaId == r.id) {
              const evaluacijeTeRealizacije = await firstValueFrom(this.evService.getByRelId(r.id, "evaluacijeznanja"));
              if (!evaluacijeTeRealizacije) {
                const evaluacija: EvaluacijaZnanjaZaBazu = {
                  id: 0,
                  ishodId: 1, //?????
                  vremePocetka: new Date("2024-10-09T10:00:00.000000"), // Stub  - Preuzeti od studentske sluzbe datum i vreme polaganja, tip i instrument evaluacije
                  vremeZavrsetka: new Date("2024-10-09T12:00:00.000000"),
                  instrument: instrument as InstrumentEvaluacije,
                  tip: tip as TipEvaluacije,
                  polaganje: null,
                  realizacijaPredmetaId: r.id,
                };

                console.log(evaluacija);
                console.log(this.evService.post("evaluacijeznanja", evaluacija).subscribe());
                this.signal.emit("Uspešno ste prijavili ispit!!");
              } else {
                this.signal.emit("ispit je vec prijavljen");
              }
            }
          }
        }
      }
    }

    // student ima vise studNaGod  <>
    // predmetID->studijskiProgram.id <>
    // studentID i studijskiProgramId -> studentNaGodini <>
    // student na godini 1 ima 4 pohadjanja
    // predmet ID je 4, on ima 3 realizacije
    //  treba te 3 realizacijeID uporediti sa ova 4 pohadjanja.realizacijaID

    // i onda da napravim evaluaciju sa tom realizacijom

  }

  async getData() {
    if (this.userId) {
      this.myStudService.getPredmetiForStudent(this.userId).then((predmeti: Predmet[]) => {
        predmeti.forEach((item) => {
          if (item.godinaStudija.godina == this.date.getFullYear()) {
            this.data.push(item);
            this.rowData = this.data.map((item) => {
              let mappedItem: { [key: string]: any } = {};
              Object.keys(item).forEach((key) => {
                mappedItem[key] = item[key];
              });
              return mappedItem;
            });
          }
        });
        this.adjustTableHeight();
      });
    }
  }

  getColumns() {
    this.genService.getColumns(this.table).subscribe((keys) => {
      this.colDefs = keys
        .filter((key) => this.colLimit.includes(key))
        .map((key) => {
          this.colDefs = [];
          const formatterName = `${key}Formatter`;
          return {
            field: key,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
          };
        });

      this.colDefs.push({
        headerName: "Prijavi ispit",
        cellRenderer: ButtonRendererComponent,
      });
      this.changeDetectorRef.detectChanges();
    });
  }

  removeFormatter() {
    this.colDefs = this.colDefs.map((colDef) => {
      return {
        ...colDef,
        valueFormatter: undefined,
      };
    });
    this.changeDetectorRef.detectChanges();
  }

  adjustTableHeight() {
    const rowCount = this.rowData.length;
    this.tableHeight = rowCount >= 10 || rowCount <= 3 ? 250 : (rowCount / 8) * 525;
  }
}
