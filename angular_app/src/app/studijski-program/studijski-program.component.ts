import { Component, EventEmitter, HostListener, Output } from "@angular/core";
import { ActivatedRoute, RouterLink, RouterOutlet } from "@angular/router";
import { StudProTableComponent } from "../studPro-table/studPro-table.component";
import { Univerzitet } from "../model/univerzitet";
import { Fakultet } from "../model/fakultet";
import { SharedService } from "../services/sharedService/shared.service";
import { switchMap, map, tap } from "rxjs";
import { GenericService } from "../services/generic/generic.service";
import { MappingService } from "../services/mappingService/mapping.service";
import { NgClass, NgFor, NgIf } from "@angular/common";
import { StudijskiProgram } from "../model/studijskiProgram";
import { Ishod } from "../model/ishod";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { Nastavnik } from "../model/nastavnik";

@Component({
  selector: "app-studijski-program",
  standalone: true,
  imports: [TranslateModule, StudProTableComponent, RouterOutlet, RouterLink, NgIf, NgFor, NgClass],
  templateUrl: "./studijski-program.component.html",
  styleUrl: "./studijski-program.component.css",
})
export class StudijskiProgramComponent {
  rukovodioc?: Nastavnik;
  univerzitet?: Univerzitet | null;
  uniId: number | null = null;
  fakultet?: Fakultet | null;
  selectedStudPro?: StudijskiProgram | null;
  fakultetId: number = 0;
  bgUrl: string = "";
  dataLimit: { [key: string]: number } = {};
  menu: string = "studmenu";
  studijskiProgrami?: StudijskiProgram[];
  ishodi: Ishod[] = [];
  silabus: boolean = false;
  showGenericTable = true;
  now: Date = new Date();

  @Output()
  backSignalEmitted = new EventEmitter<void>();

  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private facService: GenericService<Fakultet>,
    private mappingService: MappingService
  ) {
    this.translate.setDefaultLang("sr");
  }
  ngOnInit() {
    this.univerzitet = this.sharedService.getActiveUni();
    this.fakultet = this.sharedService.getActiveFac();
    if (this.univerzitet != null && this.fakultet != null) {
      this.mappingService.mapStudPro().subscribe((e) => {
        this.studijskiProgrami = e.filter((f) => f.fakultet.id == this.fakultet?.id && f.godinaStudija.godina === this.now.getFullYear());
      });
      this.bgUrl = "assets/images/univerziteti/" + this.univerzitet.naziv.toLowerCase() + "/" + this.univerzitet.naziv.toLowerCase() + "_background.jpg";
    } else {
      this.route.params
        .pipe(
          switchMap(() => {
            this.route.parent?.params.subscribe((params) => {
              this.fakultetId = params["facId"];
            });
            return this.facService.getById(this.fakultetId, "fakulteti");
          }),
          switchMap((fac) => {
            this.uniId = fac.univerzitet.id;
            this.mappingService.mapFacById(fac.id).subscribe((e) => {
              this.fakultet = e;
            });
            this.mappingService.mapStudPro().subscribe((e) => {
              this.studijskiProgrami = e.filter((f) => f.fakultet.id == this.fakultet?.id && f.godinaStudija.godina === this.now.getFullYear());
            });
            return this.mappingService.mapUniById(this.uniId);
          }),
          map((uni) => {
            this.univerzitet = uni;
            this.bgUrl = "assets/images/univerziteti/" + this.univerzitet.naziv.toLowerCase() + "/" + this.univerzitet.naziv.toLowerCase() + "_background.jpg";
          })
        )
        .subscribe();
    }
  }

  mainMenu() {
    this.menu = "studmenu";
  }
  silabusChoice() {
    this.silabus = true;
  }
  emitSignal() {
    this.silabus = false;
    this.backSignalEmitted.emit();
  }
  reloadGenericTable() {
    this.silabus = false;
    this.showGenericTable = false;
    setTimeout(() => (this.showGenericTable = true), 0);
  }

  studProChoice(studPro: StudijskiProgram) {
    this.menu = "predmet";
    if (studPro != null) {
      this.selectedStudPro = studPro;
    }
  }
}
