import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, Output, EventEmitter, SimpleChanges } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { ColDef } from "ag-grid-community";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";
import { GenericService } from "../services/generic/generic.service";
import { MappingService } from "../services/mappingService/mapping.service";
import { TranslateService } from "@ngx-translate/core";
import { firstValueFrom } from "rxjs";

@Component({
  selector: "app-studPro-table",
  standalone: true,
  imports: [AgGridAngular],
  template: `
    <ag-grid-angular
      class="ag-theme-quartz"
      [style.height.px]="tableHeight"
      [rowData]="rowData"
      [columnDefs]="colDefs"
      [defaultColDef]="defaultColDef"
      [pagination]="pagination"
      [paginationPageSize]="paginationPageSize"
      [paginationPageSizeSelector]="paginationPageSizeSelector" />
  `,
  styleUrl: "./studPro-table.component.css",
})

export class StudProTableComponent<T extends object> implements OnInit {
  constructor(private translate: TranslateService, private genService: GenericService<T>, private mappingService: MappingService) {}
  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];

  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
    resizable: false,
  };

  @Input()
  univerzitetNaziv: String | null= "";

  @Input()
  table: string = "";

  @Input()
  table2: string = "";

  @Input()
  table2ColLimits: string[] = [];

  @Input()
  colLimit: string[] = [""];

  @Output()
  menuChoice = new EventEmitter<boolean>();
  silabus: boolean = false;

  @Input()
  dataLimit: { [key: string]: number } = {};

  @Input()
  dataLimit2: string = "";

  data: any[] = [];
  tableHeight: number = 525;
  columns: { field: string }[] = [];
  colDefs: ColDef[] = [];
  rowData: any[] = [];
  dataLimitKey: string = "";
  dataLimitValue: number = 0;
  dataLimitValue2: number = 0;
  dataLimitKey2: string = "";

  ngOnInit() {
    this.dataLimitKey = Object.keys(this.dataLimit)[0];
    this.dataLimitValue = this.dataLimit[this.dataLimitKey];
    if (this.univerzitetNaziv == "Aegis") {
      document.documentElement.style.setProperty("--backColor", "rgb(65, 47, 69, 0.8)");
    }
    if (this.univerzitetNaziv == "Stella") {
      document.documentElement.style.setProperty("--backColor", "rgb(73, 17, 32, 0.8)");
    }
    if (this.univerzitetNaziv == "Astrum") {
      document.documentElement.style.setProperty("--backColor", "rgb(55, 17, 22, 0.8)");
    }
    this.getData();
    this.getColumns();
    this.translate.onLangChange.subscribe(() => {
      if (!this.silabus) {
        this.getData();
        this.getColumns();
      } else {
        this.getNewDataCols(this.dataLimitKey2, this.dataLimitValue2, this.table2, this.table2ColLimits);
      }
    });
  }

  getData() {
    this.mappingService.mapPredmet().subscribe((data: Array<{ [key: string]: any }>) => {
      this.rowData = data
        .filter((item) => item[`${this.dataLimitKey}`] === this.dataLimitValue)
        .map((item) => {
          this.translate.get(item["naziv"]).subscribe((e) => {
            item["naziv"] = e;
          });
          this.translate.get(item["preduslov"].naziv).subscribe((e) => (item["preduslov"].naziv = e));
          return item;
        });
      this.adjustTableHeight();
    });


  }

  async getColumns() {
    const keys = await firstValueFrom(this.genService.getColumns(this.table));
    this.colDefs = [];
    for (const key of keys) {
      if (this.colLimit.includes(key)) {
        const formatterName = `${key}Formatter`;
        const translation = await this.getTranslation(key);
        this.colDefs.push({
          field: key,
          headerName: translation,
          valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName] : undefined,

          onCellClicked: (event) => {
            if (this.isMobileDevice()) {
              this.removeFormatter();
              this.dataLimitValue2 = event.data[this.dataLimit2].id;
              this.dataLimitKey2 = this.dataLimit2;
              this.menuChoice.emit(true);
              this.silabus = true;
              this.getNewDataCols(this.dataLimitKey2, this.dataLimitValue2, this.table2, this.table2ColLimits);
            }
          },

          onCellDoubleClicked: (event) => {
            if (!this.isMobileDevice()) {
              this.removeFormatter();
              this.dataLimitValue2 = event.data[this.dataLimit2].id;
              this.dataLimitKey2 = this.dataLimit2;
              this.menuChoice.emit(true);
              this.silabus = true;
              this.getNewDataCols(this.dataLimitKey2, this.dataLimitValue2, this.table2, this.table2ColLimits);
            }
          },
        });
      }
    }
  }

  getNewDataCols(dataLimitKey2: string, dataLimitValue2: number, table: string, colLimit: string[]) {
    this.genService.getColumns(table).subscribe((keys) => {
      this.colDefs = keys
        .filter((key) => colLimit.includes(key))
        .map((key) => {
          const formatterName = `${key}Formatter`;
          let headerName = "";
          this.translate.get(key).subscribe((e) => (headerName = e));
          return {
            field: key,
            headerName: headerName,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
          };
        });
    });

    this.mappingService.mapIshod().subscribe((data: Array<{ [key: string]: any }>) => {
      this.rowData = data
        .filter((item) => item[dataLimitKey2].id === dataLimitValue2)
        .map((item) => {
          this.translate.get(item["naziv"]).subscribe((e) => (item["naziv"] = e));
          this.translate.get(item["obrazovniCilj"].opis).subscribe((e) => (item["obrazovniCilj"].opis = e));
          return item;
        });
      this.adjustTableHeight();
    });
  }

  async getTranslation(key: string): Promise<string> {
    const translation = await firstValueFrom(this.translate.get(key));
    return translation;
  }

  isMobileDevice() {
    return /Mobi|Android/i.test(navigator.userAgent);
  }

  removeFormatter() {
    this.colDefs = this.colDefs.map((colDef) => {
      return {
        ...colDef,
        valueFormatter: undefined,
      };
    });
  }

  godinaStudijaFormatter(params: any): string {
    const godStud = params.value;
    return `${godStud.godina}`;
  }

  preduslovFormatter(params: any): string {
    const preduslov = params.value;
    return `${preduslov.naziv}`;
  }

  obrazovniCiljFormatter(params: any): string {
    const obrCilj = params.value;
    return `${obrCilj.opis}`;
  }

  adjustTableHeight() {
    const rowCount = this.rowData.length;
    this.tableHeight = rowCount >= 10 || rowCount <= 3 ? 425 : (rowCount / 8) * 525;
  }
}
