import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudProTableComponent } from './studPro-table.component';

describe('GenericTableComponent', () => {
  let component: StudProTableComponent;
  let fixture: ComponentFixture<StudProTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StudProTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StudProTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
