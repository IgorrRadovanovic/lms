import { Component, OnInit } from "@angular/core";
import { RouterLink, RouterLinkActive, RouterOutlet } from "@angular/router";
import { UserService } from "../services/userService/user.service";
import { firstValueFrom } from "rxjs";
import { NgIf } from "@angular/common";
import { KeycloakService } from "keycloak-angular";

@Component({
  selector: "app-reg-home",
  standalone: true,
  imports: [RouterLink, RouterOutlet, RouterLinkActive, NgIf],
  templateUrl: "./reg-home.component.html",
  styleUrl: "./reg-home.component.css",
})
export class RegHomeComponent implements OnInit {
  userId: number | null = null;
  constructor(private userService: UserService, private kcService: KeycloakService) {}

  ngOnInit() {
    this.userService.getUserId().then((e) => (this.userId = e));
  }

  logout() {
    this.kcService.logout("http://localhost:4200/");
  }
}
