import { Component, Input, OnInit } from "@angular/core";
import { RegKorisnik } from "../model/regKorisnik";
import { RouterLink } from "@angular/router";
import { GenericTableComponent } from "../generic-table/generic-table.component";
import { GenericFormComponent } from "../generic-form/generic-form.component";
import { InputDetail } from "../generic-form/input-detail";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Student, StudentZaBazu } from "../model/student";
import { Nastavnik } from "../model/nastavnik";
import { GenericService } from "../services/generic/generic.service";
import { NgIf } from "@angular/common";
import { KeycloakService } from "keycloak-angular";
import { SharedService } from "../services/sharedService/shared.service";
import { UserService } from "../services/userService/user.service";
import { MappingService } from "../services/mappingService/mapping.service";
import { Mesto } from "../model/mesto";
import { first, firstValueFrom } from "rxjs";
import { Adresa } from "../model/adresa";
import { EvaluacijaZnanja } from "../model/evaluacijaZnanja";

@Component({
  selector: "app-my-profile",
  standalone: true,
  imports: [RouterLink, GenericTableComponent, GenericFormComponent, NgIf],
  templateUrl: "./my-profile.component.html",
  styleUrl: "./my-profile.component.css",
})
export class MyProfileComponent implements OnInit {
  student: Student | null = null;
  nastavnik: Nastavnik | null = null;
  regKorisnik: RegKorisnik | null = null;
  constructor(
    private sharedService: SharedService,
    private kcService: KeycloakService,
    private formBuilder: FormBuilder,
    private nastavnikService: GenericService<Nastavnik>,
    private studService: GenericService<Student>,
    private userService: UserService,
    private mappingService: MappingService,
    private mestoService: GenericService<Mesto>,
    private evService: GenericService<EvaluacijaZnanja>,
    private adrService: GenericService<Adresa>
  ) {}
  inputDetails: InputDetail[] = [];
  forma: FormGroup | null = null;
  userId: number | any = null;
  userAccess: string | null = null;
  readonly = "readonly";
  mesta: Mesto[] | null = null;
  mesto: Mesto | null = null;
  mestoId: number = 0;
  inputAdresa: Adresa | null = null;

  async submit(event: any) {
    const modifiedValues: any = {};
    if (!event.biografija && event.jmbg && this.student && this.forma) {
      this.mestoId = this.forma.get("mesto")?.getRawValue();

      Object.keys(this.forma.controls).forEach(async (key) => {
        if (this.forma) {
          const control = this.forma.get(key);

          if (control && control.dirty) {
            modifiedValues[key] = control.value;
            if (key == "ulica" || "broj" || "mesto") {
              const highId = await firstValueFrom(this.adrService.getHighestId("adrese"));
              const adrId = (highId as number) + 1;
              const mesto = await firstValueFrom(this.mestoService.getById(this.mestoId, "mesta"));

              if (key == "ulica") {
                if (this.mestoId != 0) {
                  this.inputAdresa = {
                    id: 0,
                    ulica: "",
                    broj: "",
                    mesto: await firstValueFrom(this.mestoService.getById(this.mestoId, "mesta")),
                  };
                }
                // Treba da pravimo novu adresu ako nisu isti ulica broj i mesto ID
                console.log("nije ista ulica"); // ako unesene ulice nema
                if (this.inputAdresa) {
                  this.inputAdresa.id = adrId;
                  this.inputAdresa.ulica = control.value;
                  this.inputAdresa.mesto = await firstValueFrom(this.mestoService.getById(this.mestoId, "mesta"));
                }
                if (this.inputAdresa) {
                  if (this.inputAdresa.broj == "") {
                    this.inputAdresa.broj = event.broj;
                  }
                }
              } else if (key == "broj") {
                if (this.inputAdresa) {
                  this.inputAdresa.id = adrId;
                  this.inputAdresa.ulica = event.ulica;
                  this.inputAdresa.broj = control.value;
                  this.inputAdresa.mesto = await firstValueFrom(this.mestoService.getById(this.mestoId, "mesta"));
                  // this.adrService.post("adrese", this.inputAdresa).subscribe();
                }
              }
              if (key == "mesto") {
                // ako to mesto nema tu adresu, dodati mu je.
                let postojiAdr = false;
                mesto.adrese?.forEach((a) => {
                  if (a.ulica == event.ulica && a.broj == event.broj) {
                    postojiAdr = true;
                    modifiedValues["adresaId"] = a.id;
                    console.log("Ovo mesto ima tu adresu", modifiedValues);
                    this.studService.patch(this.userId, "studenti", modifiedValues).subscribe();
                  }
                });
                if (postojiAdr == false) {
                  const adresa: Adresa = {
                    id: 0,
                    broj: event.broj,
                    ulica: event.ulica,
                    mesto: await firstValueFrom(this.mestoService.getById(control.value, "mesta")),
                  };

                  console.log("Mesto nema tu adresu", adresa);
                  this.adrService.post("adrese", adresa).subscribe(async () => {
                    let mesto2 = await firstValueFrom(this.mestoService.getById(this.mestoId, "mesta"));
                    mesto2.adrese?.forEach((a) => {
                      console.log(a, "  ", adresa);
                      if (a.ulica == adresa.ulica && a.broj == adresa.broj) {
                        modifiedValues["adresaId"] = a.id;
                      }
                    });
                    this.studService.patch(this.userId, "studenti", modifiedValues).subscribe();
                  });
                }
              }

              let postoji = false;
              const adrese: Adresa[] = await firstValueFrom(this.adrService.getAll("adrese"));
              adrese.forEach(async (a) => {
                if (this.inputAdresa && a.broj == this.inputAdresa.broj && a.ulica.toLowerCase() == this.inputAdresa.ulica.toLowerCase()) {
                  postoji = true;
                  modifiedValues["adresaId"] = a.id;
                  console.log("Patch I", modifiedValues);
                  this.studService.patch(this.userId, "studenti", modifiedValues).subscribe();
                }
              });

              if (this.inputAdresa && postoji == false) {
                this.adrService.post("adrese", this.inputAdresa).subscribe();
                modifiedValues["adresaId"] = this.inputAdresa.id;
                console.log("Patch II", modifiedValues);
                this.studService.patch(this.userId, "studenti", modifiedValues).subscribe();
              }
            }
          }
        }
      });
      console.log("Final Patch", modifiedValues);
      this.studService.patch(this.userId, "studenti", modifiedValues).subscribe();
    }
  }

  async ngOnInit() {
    this.mesta = await firstValueFrom(this.mestoService.getAll("mesta"));
    await this.userService.getUserId().then((uid) => (this.userId = uid));
    await this.kcService.getToken().then((t) => {
      const payload = this.sharedService.parseJwt(t);
      if (payload.realm_access.roles.includes("student")) {
        this.getStudentForm();
      } else if (payload.realm_access.roles.includes("nastavnik")) {
        this.getNastavnikForm();
      } else if (payload.realm_access.roles.includes("administrator")) {
        // TODO
      }
    });
  }

  async getStudentForm() {
    this.student = await firstValueFrom(this.mappingService.mapStudentById(this.userId));
    this.inputDetails = [
      {
        input_type: "text",
        input_label: "Ime",
      },
      {
        input_type: "jmbg",
        input_label: "JMBG",
      },
      {
        input_type: "email",
        input_label: "Email",
      },
      {
        input_type: "text",
        input_label: "Broj",
      },
      {
        input_type: "select-object",
        input_options: this.mesta,
        input_label: "Mesto",
      },
      {
        input_type: "text",
        input_label: "Ulica",
      },
      {
        input_type: "text",
        input_label: "Broj",
      },
    ];
    this.forma = this.formBuilder.group({
      ime: this.student.ime,
      jmbg: [this.student.jmbg, [Validators.required, Validators.pattern(/^\d{13}$/)]],
      email: [this.student.email, [Validators.required, Validators.email]],
      brojTelefona: this.student.brojTelefona,
      mesto: this.student.adresa.mesto.id,
      ulica: this.student.adresa.ulica,
      broj: this.student.adresa.broj,
    });
  }

  getNastavnikForm(): void {
    this.nastavnikService.getById(1, "nastavnici").subscribe((n) => {
      this.nastavnik = n;
      this.inputDetails = [
        {
          input_type: "text",
          input_label: "Ime",
        },
        {
          input_type: "number",
          input_label: "JMBG",
        },
        {
          input_type: "textarea",
          input_label: "Biografija",
        },
      ];
      this.forma = this.formBuilder.group({
        ime: this.nastavnik.ime,
        jmbg: this.nastavnik.jmbg,
        biografija: this.nastavnik.biografija,
      });
    });
  }
  menuChoice(): void {}
}
