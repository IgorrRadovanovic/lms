import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, Output, EventEmitter, SimpleChanges } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { ColDef } from "ag-grid-community";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";
import { GenericService } from "../services/generic/generic.service";
import { MappingService } from "../services/mappingService/mapping.service";


@Component({
  selector: "app-subject-table",
  standalone: true,
  imports: [AgGridAngular],
  template: `
    <ag-grid-angular
      class="ag-theme-quartz"
      [style.height.px]="tableHeight"
      [rowData]="rowData"
      [columnDefs]="colDefs"
      [defaultColDef]="defaultColDef"
      [pagination]="pagination"
      [paginationPageSize]="paginationPageSize"
      [paginationPageSizeSelector]="paginationPageSizeSelector" />
  `,
  styleUrl: "./generic-table.component.css",
})
export class GenericTableComponent<T extends object> implements OnInit, OnChanges {
  constructor(private genService: GenericService<T>, private mappingService: MappingService, private changeDetectorRef: ChangeDetectorRef) {}

  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];

  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
  };

  @Input()
  table: string = "";

  @Input()
  table2: string = "";

  @Input()
  table2ColLimits: string[] = [];

  @Input()
  colLimit: string[] = [""];

  @Output()
  menuChoice = new EventEmitter<boolean>();
  silabus: boolean = false;

  @Input()
  dataLimit: { [key: string]: number } = {};

  @Input()
  dataLimit2: string = "";

  data: any[] = [];
  tableHeight: number = 525;
  columns: { field: string }[] = [];
  colDefs: ColDef[] = [];
  rowData: any[] = [];
  dataLimitKey: string = Object.keys(this.dataLimit)[0];
  dataLimitValue: number = this.dataLimit[this.dataLimitKey];

  ngOnInit() {
    this.getData();
    this.getColumns();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["dataLimit"]) {
      this.dataLimitKey = Object.keys(this.dataLimit)[0];
      this.dataLimitValue = this.dataLimit[this.dataLimitKey];
    }
  }

  getData() {
    this.mappingService.mapPredmet().subscribe((data: Array<{ [key: string]: any }>) => {
      console.log(data);
      this.rowData = [];
      this.rowData = data
        .filter((item) => item[`${this.dataLimitKey}`] === this.dataLimitValue)
        .map((item) => {
          let mappedItem: { [key: string]: any } = {};
          Object.keys(item).forEach((key) => {
            mappedItem[key] = item[key];
          });
          return mappedItem;
        });
      this.adjustTableHeight();
    });
  }

  getColumns() {
    this.genService.getColumns(this.table).subscribe((keys) => {
      this.colDefs = keys
        .filter((key) => this.colLimit.includes(key))
        .map((key) => {
          this.colDefs = [];
          const formatterName = `${key}Formatter`;
          return {
            field: key,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
            onCellClicked: (event) => {
              if (this.isMobileDevice()) {
                this.removeFormatter();
                const dataLimitValue2 = event.data[this.dataLimit2].id;
                const dataLimitKey2 = this.dataLimit2;
                this.menuChoice.emit(true);
                this.getNewDataCols(dataLimitKey2, dataLimitValue2, this.table2, this.table2ColLimits);
              }
            },
            onCellDoubleClicked: (event) => {
              if (!this.isMobileDevice()) {
                this.removeFormatter();
                const dataLimitValue2 = event.data[this.dataLimit2].id;
                const dataLimitKey2 = this.dataLimit2;
                this.menuChoice.emit(true);
                this.getNewDataCols(dataLimitKey2, dataLimitValue2, this.table2, this.table2ColLimits);
              }
            },
          };
        });
      this.changeDetectorRef.detectChanges();
    });
  }

  isMobileDevice() {
    return /Mobi|Android/i.test(navigator.userAgent);
  }

  removeFormatter() {
    this.colDefs = this.colDefs.map((colDef) => {
      return {
        ...colDef,
        valueFormatter: undefined,
      };
    });

    this.changeDetectorRef.detectChanges();
  }

  getNewDataCols(dataLimitKey2: string, dataLimitValue2: number, table: string, colLimit: string[]) {
    this.rowData = [];
    this.genService.getColumns(table).subscribe((keys) => {
     this.colDefs = keys
        .filter((key) => colLimit.includes(key))
        .map((key) => {
          const formatterName = `${key}Formatter`;
          return {
            field: key,
            valueFormatter: (this as any)[formatterName] ? (this as any)[formatterName].bind(this) : undefined,
          };
        });
    });

    this.mappingService.mapIshod().subscribe((data: Array<{ [key: string]: any }>) => {
      this.rowData = data
        .filter((item) => item[dataLimitKey2].id === dataLimitValue2)
        .map((item) => {
          let mappedItem: { [key: string]: any } = {};
          Object.keys(item).forEach((key) => {
            mappedItem[key] = item[key];
          });
          return mappedItem;
        });
      this.adjustTableHeight();
    });
    this.changeDetectorRef.detectChanges();
  }

  godinaStudijaFormatter(params: any): string {
    const godStud = params.value;
    return `${godStud.godina}`;
  }

  preduslovFormatter(params: any): string {
    const preduslov = params.value;
    return `${preduslov.naziv}`;
  }

  obrazovniCiljFormatter(params: any): string {
    const obrCilj = params.value;
    return `${obrCilj.opis}`;
  }

  adjustTableHeight() {
    const rowCount = this.rowData.length;
    this.tableHeight = rowCount >= 10 || rowCount <= 3 ? 425 : (rowCount / 8) * 525;
  }
}
