import { NgFor, NgIf, NgOptimizedImage } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, RouterLink, RouterModule } from "@angular/router";
import { Univerzitet } from "../model/univerzitet";
import { MappingService } from "../services/mappingService/mapping.service";
import { SharedService } from "../services/sharedService/shared.service";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { UserService } from "../services/userService/user.service";

@Component({
  selector: "app-homepage",
  standalone: true,
  imports: [RouterLink, TranslateModule, RouterModule, NgOptimizedImage, NgFor, NgIf],
  templateUrl: "./homepage.component.html",
  styleUrl: "./homepage.component.css",
})
export class HomepageComponent implements OnInit {
  univerziteti: Univerzitet[] = [];
  uniMenu: boolean = false;
  um: string = "";
  userId: null | number = null;
  constructor(private uService: UserService, private translate: TranslateService, private route: ActivatedRoute, private mappingService: MappingService, private sharedService: SharedService) {
    this.translate.setDefaultLang("sr");
  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit(): void {
    this.uService.getUserId().then((e) => (this.userId = e));
    this.route.params.subscribe((params) => {
      if (params["uni"] == "um") {
        this.uniMenu = true;
      }
    });
    this.mappingService.mapUni().subscribe((univerziteti) => {
      this.univerziteti = univerziteti;
    });
  }

  uniSelectionMenu() {
    this.uniMenu = true;
    console.log("Uni", this.uniMenu);
  }

  selectUniversity(univerzitet: Univerzitet) {
    this.sharedService.setActiveUni(univerzitet);
  }

  backToMainMenu() {
    this.uniMenu = false;
  }
}
