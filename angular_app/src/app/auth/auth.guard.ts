import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard extends KeycloakAuthGuard {

  constructor(
    protected override readonly router: Router,
    protected readonly keycloak: KeycloakService
  ) {
    super(router, keycloak);
  }
  public async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    // Force the user to log in if currently unauthenticated.
    if (!this.authenticated) {
      await this.keycloak.login({
        redirectUri: window.location.origin + state.url
      });
    }

    // Get the roles required from the route.
    const requiredRoles = route.data['required-roles'];
    console.log("Required roles")
    console.log(requiredRoles)
    

    // If no specific roles are required, check if there is a limiter for allowed roles
    if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
      const allowedRoles = route.data['allowed-roles'];
      console.log("Allowed roles")
      console.log(allowedRoles)

      // If there is no limiter on allowed roles, allow access
      if (!(allowedRoles instanceof Array) || allowedRoles.length === 0){
        return true;
      }

      // If user has one of allowed roles, allow access, otherwise block access
      for(let i=0;i<allowedRoles.length;i++){
        console.log(allowedRoles[i])
        if(this.roles.includes(allowedRoles[i])){
          return true
        }
      }
      return false;
    }

    // Allow the user to proceed if all the required roles are present.
    return requiredRoles.every((role) => this.roles.includes(role));
  }
}