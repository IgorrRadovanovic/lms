import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { UserService } from '../../services/userService/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirect',
  standalone: true,
  imports: [],
  templateUrl: './redirect.component.html',
  styleUrl: './redirect.component.css'
})
export class RedirectComponent implements OnInit {
  protected readonly keycloakService: KeycloakService;
  constructor(kcs: KeycloakService,private _router: Router) {
    this.keycloakService = kcs;
  }
  ngOnInit(): void {
    let roles = this.keycloakService.getUserRoles();
    if(roles.includes("teacher")){
      this._router.navigateByUrl("nastHome");
    }else if(roles.includes("student")){
      this._router.navigateByUrl("regHome");
    }else{
      this._router.navigateByUrl("");
    }
  }
}