import { Component, OnInit } from "@angular/core";
import { KeycloakService } from "keycloak-angular";

@Component({
  selector: "app-auth",
  standalone: true,
  imports: [],
  templateUrl: "./auth.component.html",
  styleUrl: "./auth.component.css",
})
export class AuthComponent implements OnInit {
  protected readonly keycloakService: KeycloakService;
  constructor(kcs: KeycloakService) {
    this.keycloakService = kcs;
  }
  ngOnInit(): void {
    this.login();
  }

  async login(): Promise<void> {
    await this.keycloakService.login({
      redirectUri: 'http://localhost:4200/roleRedirect',
    });
  }
  async logout(): Promise<void> {
    await this.keycloakService.logout('http://localhost:4200/');
  }

}
