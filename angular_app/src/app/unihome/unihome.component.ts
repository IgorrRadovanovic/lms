import { Component, OnInit } from "@angular/core";
import { Fakultet } from "../model/fakultet";
import { Univerzitet } from "../model/univerzitet";
import { GenericService } from "../services/generic/generic.service";
import { MappingService } from "../services/mappingService/mapping.service";
import { NgFor, NgIf } from "@angular/common";
import { ActivatedRoute, RouterLink, RouterModule } from "@angular/router";
import { switchMap, map } from "rxjs";
import { SharedService } from "../services/sharedService/shared.service";
import { ONamaCard, aegisOnamaCards, astrumOnamaCards, stellaOnamaCards, aegisOnamaCardsEN, astrumOnamaCardsEN, stellaOnamaCardsEN } from "../model/oNama/onamaCard";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-unihome",
  standalone: true,
  imports: [TranslateModule, NgFor, NgIf, RouterLink, RouterModule],
  templateUrl: "./unihome.component.html",
  styleUrl: "./unihome.component.css",
})
export class UnihomeComponent implements OnInit {
  fakulteti: Fakultet[] = [];
  uniId: number = 0;
  fakultet: Fakultet | null = null;
  univerzitet: Univerzitet | null = null;
  facMenu: boolean = false;
  bg_url: string = "";
  menu = "mainmenu";

  oNama: ONamaCard[] = [];

  constructor(
    private translate: TranslateService,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private mappingService: MappingService,
    private facService: GenericService<Fakultet>
  ) {
    this.translate.onLangChange.subscribe((e) => {
      console.log("Lang change");
      if (this.univerzitet) {
        this.setOnama(this.univerzitet);
      }
    });
  }

  ngOnInit(): void {
    this.translate.setDefaultLang("sr");

    this.univerzitet = this.sharedService.getActiveUni();
    if (this.univerzitet != null) {
      this.bg_url = "assets/images/univerziteti/" + this.univerzitet.naziv.toLowerCase() + "/" + this.univerzitet.naziv.toLowerCase() + "_background.jpg";
      this.mappingService.mapFac().subscribe((mFakulteti) => {
        this.fakulteti = mFakulteti.filter((f) => {
          if (this.univerzitet) {
            return f.univerzitetId === this.univerzitet.id;
          } else return null;
        });
      });
    } else {
      this.route.params
        .pipe(
          switchMap((params) => {
            this.uniId = params["uniId"];
            return this.mappingService.mapUniById(this.uniId);
          }),
          switchMap((uni) => {
            this.univerzitet = uni;
            console.log(this.univerzitet);
            this.bg_url = "assets/images/univerziteti/" + this.univerzitet.naziv.toLowerCase() + "/" + this.univerzitet.naziv.toLowerCase() + "_background.jpg";
            return this.mappingService.mapFac();
          }),
          map((fakulteti) => {
            this.fakulteti = fakulteti.filter((f) => {
              if (this.univerzitet) {
                f.univerzitetId === this.univerzitet.id;
              }
            });
          })
        )
        .subscribe();
    }
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  setOnama(univerzitet: Univerzitet) {
    if (this.translate.currentLang == undefined) {
      this.translate.currentLang = "sr";
    }
    if (univerzitet.naziv == "Aegis" && this.translate.currentLang == "sr") {
      this.oNama = aegisOnamaCards;
    }
    if (univerzitet.naziv == "Aegis" && this.translate.currentLang == "en") {
      this.oNama = aegisOnamaCardsEN;
    }
    if (univerzitet.naziv == "Astrum" && this.translate.currentLang == "sr") {
      this.oNama = astrumOnamaCards;
    }
    if (univerzitet.naziv == "Astrum" && this.translate.currentLang == "en") {
      this.oNama = astrumOnamaCardsEN;
    }
    if (univerzitet.naziv == "Stella" && this.translate.currentLang == "sr") {
      this.oNama = stellaOnamaCards;
    }
    if (univerzitet.naziv == "Stella" && this.translate.currentLang == "en") {
      this.oNama = stellaOnamaCardsEN;
    }
  }

  facSelectionMenu() {
    this.facMenu = true;
    console.log("Fac", this.facMenu);
  }

  resetSelection() {
    this.facMenu = false;
  }

  selectFaculty(fakultet: Fakultet) {
    this.sharedService.setActiveFac(fakultet);
  }

  onamaMenu() {
    if (this.univerzitet) {
      this.setOnama(this.univerzitet);
    }
    this.menu = "onama";
  }

  mainMenu() {
    this.facMenu = false;
    this.fakultet = null;
    this.menu = "mainmenu";
  }
}
