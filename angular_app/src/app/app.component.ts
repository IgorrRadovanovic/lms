import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'lms';
  constructor(private translate: TranslateService ) {
    this.translate.setDefaultLang('sr')
      }
      switchLanguage(language: string) {
        this.translate.use(language);
      }
}
