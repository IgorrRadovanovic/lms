import { Routes } from "@angular/router";
import { HomepageComponent } from "./homepage/homepage.component";
import { AuthComponent } from "./auth/auth.component";
import { UnihomeComponent } from "./unihome/unihome.component";
import { FachomeComponent } from "./fachome/fachome.component";
import { StudijskiProgramComponent } from "./studijski-program/studijski-program.component";
import { MySubjectsComponent } from "./my-subjects/my-subjects.component";
import { StudyHistoryComponent } from "./study-history/study-history.component";
import { RegHomeComponent } from "./reg-home/reg-home.component";
import { MyProfileComponent } from "./my-profile/my-profile.component";
import { NastavnikHomeComponent } from "./nastavnik-komponente/nastavnik-home/nastavnik-home.component";
import { NastavnikProfilComponent } from "./nastavnik-komponente/nastavnik-profil/nastavnik-profil.component";
import { NastavnikPredmetComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/nastavnik-predmet/nastavnik-predmet.component";
import { NastavnikPredmetiSviComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/nastavnik-predmeti-svi/nastavnik-predmeti-svi.component";
import { PredmetJedanComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/predmet-jedan/predmet-jedan.component";
import { NastavnikPredmetiComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/nastavnik-predmeti/nastavnik-predmeti.component";
import { NastavnikSilabusComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/nastavnik-silabus/nastavnik-silabus.component";
import { NastavnikTerminiComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/termini-komponente/nastavnik-termini/nastavnik-termini.component";
import { NastavnikTerminiSviComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/termini-komponente/nastavnik-termini-svi/nastavnik-termini-svi.component";
import { NastavnikTerminJedanComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/termini-komponente/nastavnik-termin-jedan/nastavnik-termin-jedan.component";
import { InstrumentiEvaluacijeComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/instrument-evaluacije-komponente/instrumenti-evaluacije/instrumenti-evaluacije.component";
import { InstrumeniEvaluacijeSviComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/instrument-evaluacije-komponente/instrumeni-evaluacije-svi/instrumeni-evaluacije-svi.component";
import { InstrumentEvaluacijeJedanComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/instrument-evaluacije-komponente/instrument-evaluacije-jedan/instrument-evaluacije-jedan.component";
import { ObavestenjaComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/obavestenja-komponente/obavestenja/obavestenja.component";
import { ObavestenjeJednoComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/obavestenja-komponente/obavestenje-jedno/obavestenje-jedno.component";
import { StudentiKojiPohadjajuComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/studenti-komponente/studenti-koji-pohadjaju/studenti-koji-pohadjaju.component";
import { StudentJedanComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/studenti-komponente/student-jedan/student-jedan.component";
import { StudentIspitiComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/studenti-komponente/student-ispiti/student-ispiti.component";
import { UpisiComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/studenti-komponente/upisi/upisi.component";
import { IspitiSviComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/ispiti-komponente/ispiti-svi/ispiti-svi.component";
import { IspitiJedanComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/ispiti-komponente/ispiti-jedan/ispiti-jedan.component";
import { IspitiPolaganjaComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/ispiti-komponente/ispiti-polaganja/ispiti-polaganja.component";
import { RedirectComponent } from "./auth/redirect/redirect.component";
import { SilabusTableComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/nastavnik-silabus/silabus-table/silabus-table.component";
import { SilabusFormComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/nastavnik-silabus/silabus-form/silabus-form.component";
import { SilabusTableWrapperComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/nastavnik-silabus/silabus-table-wrapper/silabus-table-wrapper.component";
import { SilabusFormWrapperComponent } from "./nastavnik-komponente/nastavnik-predmeti-komponente/predmet-komponente/nastavnik-silabus/silabus-form-wrapper/silabus-form-wrapper.component";
import { AuthGuard } from "./auth/auth.guard";

export const routes: Routes = [
  { path: "home", component: HomepageComponent },
  { path: "roleRedirect", component:RedirectComponent},
  { path: "homepage/:uni", component: HomepageComponent },
  { path: "auth", component: AuthComponent },
  { path: "unihome/:uniId", component: UnihomeComponent },
  { path: "faculties/:facId", component: FachomeComponent, children: [{ path: "studijskiProgram", component: StudijskiProgramComponent, outlet: "studijskiProgram" }] },
  // { path: "subject/:subId", 
  //   component:PredmetHomeComponent,
  //   children:[
  //     {
  //       path:"sub-details",
  //       component:PredmetDisplayComponent
  //     },
  //     {
  //       path:"sub-realizations",
  //       component:RealizationListComponent
  //     }
  //   ]},



  { path: 'regHome', component: RegHomeComponent,canActivate:[AuthGuard],data:{
    "allowed-roles":["student","admin"]
}, children: [
    { path: 'myProfile', component: MyProfileComponent, outlet: 'regHomeOutlet' },
    { path: 'mySubjects', component: MySubjectsComponent, outlet: 'regHomeOutlet' },
    { path: 'studyHistory', component: StudyHistoryComponent, outlet: 'regHomeOutlet' },
  ] },
  {
    path: 'nastHome', component: NastavnikHomeComponent,canActivate:[AuthGuard],data:{
      "allowed-roles":["teacher","admin"]
  }, children:[
      { path: 'myProfile', component:NastavnikProfilComponent},
      { path: 'mySubjects', component:NastavnikPredmetiComponent, children:[
        {path: 'svi', component:NastavnikPredmetiSviComponent,},
        {path: ':subId', component:NastavnikPredmetComponent, children:[
          {path:'main',component:PredmetJedanComponent},
          {path:'silabus', component:NastavnikSilabusComponent, children:[
            {path:'main',component:SilabusTableWrapperComponent},
            {path:':ishodId',component:SilabusFormWrapperComponent},
            {path:'new',component:SilabusFormWrapperComponent}
          ]},
          {path:'ishod', component:NastavnikSilabusComponent},
          {path:'termini', component:NastavnikTerminiComponent, children:[
            {path:'svi', component:NastavnikTerminiSviComponent},
            {path:':terminId', component:NastavnikTerminJedanComponent}
          ]},
          {path:'instrumentiEvaluacije', component: InstrumentiEvaluacijeComponent, children:[
            {path:'svi', component:InstrumeniEvaluacijeSviComponent},
            {path:':insId', component:InstrumentEvaluacijeJedanComponent}
          ]},
          {path:'obavestenja', component: ObavestenjaComponent, children:[
            {path:':obId', component:ObavestenjeJednoComponent},
            {path:'novo', component:ObavestenjeJednoComponent}
          ]},
          {path:'studenti', component:StudentiKojiPohadjajuComponent, children:[
            {path:':studId', component:StudentJedanComponent, children:[
              {path:'ispiti', component:StudentIspitiComponent},
              {path:'upisi', component:UpisiComponent}
            ]}
          ]},
          {path:'ispiti', component:IspitiSviComponent, children:[
            {path:':ispId', component:IspitiJedanComponent, children:[
              {path:'polaganja', component:IspitiPolaganjaComponent}
            ]}
          ]}
        ]}
      ]},
    ]
  },
  { path: '', redirectTo: "home", pathMatch: 'full' }
];
