import { Ishod } from "./ishod";
import { RealizacijaPredmeta } from "./realizacijaPredmeta";
import { TipNastave } from "./tipNastave";


export interface TerminNastave{
    id:number;
    vremePocetka: Date;
    vremeZavrsetka: Date;
    realizacijaPredmeta: RealizacijaPredmeta;
    tipNastave: TipNastave;
    tipNastaveId:number;
    ishod:Ishod;
    
}