import { Adresa } from "./adresa";
import { Nastavnik } from "./nastavnik";
import { Univerzitet} from "./univerzitet";
import { StudijskiProgram } from "./studijskiProgram";


export interface Fakultet {
    id: number;
    naziv: string,
    univerzitet: Univerzitet,
    univerzitetId: number,
    adresa: Adresa;
    adresaId: number;
    dekan: Nastavnik;
    dekanId: number;
    studijskiProgram: StudijskiProgram[];
    brojTelefona: number;
    email: string
}
