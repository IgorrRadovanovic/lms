import { InstrumentEvaluacije } from "./instrumentEvaluacije";
import { Ishod } from "./ishod";
import { Polaganje } from "./polaganje";
import { RealizacijaPredmeta } from "./realizacijaPredmeta";
import { TipEvaluacije } from "./tipEvaluacije";

export interface EvaluacijaZnanja {
  id: number;
  ishodId: number;
  vremePocetka: Date;
  vremeZavrsetka: Date;
  tip: TipEvaluacije;
  instrument: InstrumentEvaluacije;
  polaganje: Polaganje | null;
  realizacijaPredmetaId: number;
  brojBodova: number;
}
export interface EvaluacijaZnanjaZaBazu {
  id: number;
  ishodId: number;
  vremePocetka: Date;
  vremeZavrsetka: Date;
  tip: TipEvaluacije;
  instrument: InstrumentEvaluacije;
  polaganje: Polaganje | null;
  realizacijaPredmetaId: number;
}
