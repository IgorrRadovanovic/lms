import { Obavestenje } from "./obavestenje";


export interface Fajl {
    id: number;
    opis: string;
    url: string;
    obavestenje?: Obavestenje;
}
