import { Ishod } from "./ishod";
export interface Silabus{
    id: number;
    ishodi: Ishod[];
}