import { Predmet } from "./predmet";
import { StudijskiProgram } from "./studijskiProgram";
import { StudentNaGodini } from "./studentNaGodini";


export interface GodinaStudija {
    id: number;
    godina: number;
    studijskiProgram?: StudijskiProgram[];
    studentNaGodini?: StudentNaGodini;
    predmeti?: Predmet[];
}
