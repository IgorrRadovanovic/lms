import { Nastavnik } from "./nastavnik";
import { TipNastave } from "./tipNastave";

export interface NastavnikNaRealizaciji{
    id: number;
    broj_casova: number;
    realizacijaPredmetaId: number;
    nastavnik: Nastavnik;
    tipNastave: TipNastave;
    
}