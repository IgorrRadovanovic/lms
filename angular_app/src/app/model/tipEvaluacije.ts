import { EvaluacijaZnanja } from "./evaluacijaZnanja";

export interface TipEvaluacije{
    id: number;
    naziv: string;
    evaluacije: EvaluacijaZnanja[];


}