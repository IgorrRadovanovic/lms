export class TablePredmet {
  id: number;
  nazivPredmeta: string;
  brojPolaganja: number;
  ocena: number;
  konacniBodovi: number;

  constructor(id: number, nazivPredmeta: string, brojPolaganja: number, ocena: number, konacniBodovi: number) {
    this.konacniBodovi = konacniBodovi;
    this.id = id;
    this.nazivPredmeta = nazivPredmeta;
    this.brojPolaganja = brojPolaganja;
    this.ocena = ocena;
  }
}
