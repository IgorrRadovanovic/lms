import { Adresa } from "./adresa";
import { Fakultet } from "./fakultet";
import { Nastavnik } from "./nastavnik";

export interface Univerzitet {
    id: number;
    naziv: string;
    datumOsnivanja: Date;
    adresa: Adresa;
    adresaId: number;
    rektor: Nastavnik;
    rektorId: number;
    fakulteti: Fakultet[];
    fakultetId: number;
    brojTelefona:number;
    email:string;
}
