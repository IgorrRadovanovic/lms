import { RealizacijaPredmeta } from "./realizacijaPredmeta";
import { StudentNaGodini } from "./studentNaGodini";

export interface PohadjanjePredmeta{
    id: number;
    konacnaOcena:number;
    studentNaGodini: StudentNaGodini[];
    realizacijaPredmeta: RealizacijaPredmeta;
    realizacijaPredmetaId: number;
}