export interface ONamaCard {
  header: string;
  content: string;
}

export let aegisOnamaCards: ONamaCard[] = [
  {
    header: "Универзитет",
    content:
      "Aeгис Универзитет је водећа образовна институција која се налази у сликовитом кампусу окруженом природним лепотама. Основан са мисијом да инспирише и образује будуће лидере, Aeгис Универзитет нуди широк спектар академских програма и истраживачких могућности.",
  },
  {
    header: "Наша Мисија",
    content:
      " Наша мисија је да пружимо студентима врхунско образовање које комбинује теоријско знање са практичним вештинама. Визија универзитета је да постане глобални лидер уобразовању, истраживању и иновацијама, стварајући окружење које подстиче креативност, критичко размишљање и интердисциплинарну сарадњу.",
  },
  {
    header: "Наш Ректор",
    content:
      " Ректор нашег универзитета, др. Милан Милановић, је човек са визијом и посвећеношћу образовању. Са више од 30 година искуства у академској заједници, др. Милановић је постао симбол иновација и напретка у високом образовању. Рођен у малом граду, др. Милановић је одувек био страствен према науци и образовању. Након што је завршио докторске студије из области физике на престижном универзитету у иностранству, вратио се у Србију са циљем да унапреди домаћи образовни систем. Као ректор, др. Милановић је иницирао бројне пројекте који су модернизовали наставне методе и унапредили истраживачке капацитете универзитета. Под његовим вођством, Универзитет “Светлост Знања” је постао један од водећих у региону, познат по својим иновативним програмима и међународној сарадњи. Др. Милановић је такође познат по својој приступачности и подршци студентима. Често организује отворене састанке са студентима, где слуша њихове идеје и предлоге, и активно ради на њиховој реализацији. Његова врата су увек отворена за све који желе да допринесу развоју универзитета. Поред академских успеха, др. Милановић је и велики хуманитарац. Организовао је бројне акције за помоћ угроженим групама и активно учествује у друштвеним пројектима који имају за циљ побољшање квалитета живота у заједници. Ректор Милан Милановић је прави пример лидера који својим радом и посвећеношћу инспирише друге и оставља трајан утицај на академску заједницу и друштво у целини.",
  },
];
export let aegisOnamaCardsEN: ONamaCard[] = [
  {
    header: "University",
    content:
      "Aegis University is a leading educational institution located in a picturesque campus surrounded by natural beauty. Founded with a mission to inspire and educate future leaders, Aegis University offers a wide range of academic programs and research opportunities.",
  },
  {
    header: "Our Mission",
    content:
      "Our mission is to provide students with top-notch education that combines theoretical knowledge with practical skills. The university's vision is to become a global leader in education, research, and innovation, creating an environment that fosters creativity, critical thinking, and interdisciplinary collaboration.",
  },
  {
    header: "Our Rector",
    content:
      "The rector of our university, Dr. Milan Milanović, is a man with vision and dedication to education. With over 30 years of experience in the academic community, Dr. Milanović has become a symbol of innovation and progress in higher education. Born in a small town, Dr. Milanović has always been passionate about science and education. After completing his doctoral studies in physics at a prestigious university abroad, he returned to Serbia with the goal of improving the domestic education system. As rector, Dr. Milanović has initiated numerous projects that have modernized teaching methods and enhanced the university's research capacities. Under his leadership, the University 'Light of Knowledge' has become one of the leading institutions in the region, known for its innovative programs and international collaboration. Dr. Milanović is also known for his accessibility and support for students. He often organizes open meetings with students, where he listens to their ideas and suggestions and actively works on their implementation. His door is always open to anyone who wants to contribute to the development of the university. In addition to his academic achievements, Dr. Milanović is also a great humanitarian. He has organized numerous actions to help vulnerable groups and actively participates in social projects aimed at improving the quality of life in the community. Rector Milan Milanović is a true example of a leader who inspires others with his work and dedication, leaving a lasting impact on the academic community and society as a whole.",
  },
];

export let astrumOnamaCards: ONamaCard[] = [
  {
    header: "Универзитет",
    content:
      "Аструм универзитет је водећа образовна институција која се истиче у пружању квалитетног образовања и истраживања у различитим научним и техничким дисциплинама. Са фокусом на иновације и академску изврсност, универзитет нуди разноврсне програме који покривају широк спектар области.",
  },
  {
    header: "Образовни Програми",
    content:
      "На Аструм универзитету, студенти могу да бирају између бројних студијских програма на основним, мастер и докторским студијама. Програми су дизајнирани да пруже дубоко теоријско знање и практичне вештине потребне за успех у савременом свету.",
  },
  {
    header: "Истраживачке Иницијативе",
    content:
      "Универзитет је посвећен промоцији истраживања и иновација кроз своје бројне истраживачке центре и лабораторије. Сарадња са водећим компанијама и међународним партнерима омогућава студентима и наставницима да раде на пројектима који имају глобални утицај.",
  },
  {
    header: "Наш Ректор",
    content:
      "Ректор Аструм универзитета, др. Милан Јовановић, је истакнути академик и лидер у области образовања и истраживања. Са више од 20 година искуства у академској заједници, др. Јовановић је познат по својим доприносима у области медицин. Др. Јовановић је докторирао на престижном универзитету у иностранству и објавио је бројне научне радове у међународним часописима. Његова каријера обухвата рад на различитим универзитетима и истраживачким институцијама, где је стекао богато искуство у настави и истраживању. Као ректор, др. Јовановић је посвећен унапређењу квалитета образовања и истраживања на Аструм универзитету. Његова визија укључује развој иновативних студијских програма који одговарају потребама савременог тржишта рада, као и подстицање сарадње са водећим компанијама и истраживачким институцијама. Др. Јовановић је познат по свом приступу који ставља студенте у центар пажње, пружајући им подршку и ресурсе за њихов професионални и лични развој. Под његовим вођством, Аструм универзитет је постигао значајне успехе у области истраживања и иновација, што га чини једним од водећих универзитета у региону.",
  },
];
export let astrumOnamaCardsEN: ONamaCard[] = [
  {
    header: "University",
    content:
      "Astrum University is a leading educational institution that excels in providing quality education and research in various scientific and technical disciplines. With a focus on innovation and academic excellence, the university offers diverse programs covering a wide range of fields.",
  },
  {
    header: "Educational Programs",
    content:
      "At Astrum University, students can choose from numerous study programs at the undergraduate, master's, and doctoral levels. The programs are designed to provide deep theoretical knowledge and practical skills needed for success in the modern world.",
  },
  {
    header: "Research Initiatives",
    content:
      "The university is dedicated to promoting research and innovation through its numerous research centers and laboratories. Collaboration with leading companies and international partners allows students and faculty to work on projects with global impact.",
  },
  {
    header: "Our Rector",
    content:
      "The rector of Astrum University, Dr. Milan Jovanović, is a distinguished academic and leader in the field of education and research. With over 20 years of experience in the academic community, Dr. Jovanović is known for his contributions to the field of medicine. Dr. Jovanović earned his doctorate at a prestigious university abroad and has published numerous scientific papers in international journals. His career includes work at various universities and research institutions, where he gained extensive experience in teaching and research. As rector, Dr. Jovanović is committed to enhancing the quality of education and research at Astrum University. His vision includes developing innovative study programs that meet the needs of the modern labor market and fostering collaboration with leading companies and research institutions. Dr. Jovanović is known for his student-centered approach, providing support and resources for their professional and personal development. Under his leadership, Astrum University has achieved significant success in research and innovation, making it one of the leading universities in the region.",
  },
];


export let stellaOnamaCards: ONamaCard[] = [
  {
    header: "Универзитет Стела",
    content:
      "Универзитет Стела је престижна образовна институција посвећена пружању врхунског образовања и истраживања у различитим научним и техничким областима. Основан са визијом да постане лидер у иновацијама и академској изврсности, Универзитет Стела нуди широк спектар студијских програма који обухватају инжењерство, природне науке, друштвене науке и хуманистичке науке.",
  },
  {
    header: "Студијски Програми",
    content:
      "На Универзитету Стела, студенти могу да бирају између бројних студијских програма на основним, мастер и докторским студијама. Програми су дизајнирани да пруже дубоко теоријско знање и практичне вештине потребне за успех у савременом свету.",
  },
  {
    header: "Истраживање и Иновације",
    content:
      "Универзитет Стела је посвећен промоцији истраживања и иновација кроз своје бројне истраживачке центре и лабораторије. Сарадња са водећим компанијама и међународним партнерима омогућава студентима и наставницима да раде на пројектима који имају глобални утицај.",
  },
  {
    header: "Наш Ректор",
    content:
      "Ректор Универзитета Стела, др. Ана Марковић, је истакнута академкиња и лидерка у области образовања и истраживања. Са више од 15 година искуства у академској заједници, др. Марковић је позната по својим доприносима у области биоинжењерства. Др. Марковић је докторирала на престижном универзитету у иностранству и објавила је бројне научне радове у међународним часописима. Њена каријера обухвата рад на различитим универзитетима и истраживачким институцијама, где је стекла богато искуство у настави и истраживању. Као ректорка, др. Марковић је посвећена унапређењу квалитета образовања и истраживања на Универзитету Стела. Њена визија укључује развој иновативних студијских програма који одговарају потребама савременог тржишта рада, као и подстицање сарадње са водећим компанијама и истраживачким институцијама. Др. Марковић је позната по свом приступу који ставља студенте у центар пажње, пружајући им подршку и ресурсе за њихов професионални и лични развој. Под њеним вођством, Универзитет Стела је постигао значајне успехе у области истраживања и иновација, што га чини једним од водећих универзитета у региону.",
  },
];
export let stellaOnamaCardsEN: ONamaCard[] = [
  {
    header: "Stella University",
    content:
      "Stella University is a prestigious educational institution dedicated to providing top-notch education and research in various scientific and technical fields. Founded with a vision to become a leader in innovation and academic excellence, Stella University offers a wide range of study programs covering engineering, natural sciences, social sciences, and humanities.",
  },
  {
    header: "Study Programs",
    content:
      "At Stella University, students can choose from numerous study programs at the undergraduate, master's, and doctoral levels. The programs are designed to provide deep theoretical knowledge and practical skills needed for success in the modern world.",
  },
  {
    header: "Research and Innovation",
    content:
      "Stella University is committed to promoting research and innovation through its numerous research centers and laboratories. Collaboration with leading companies and international partners allows students and faculty to work on projects with global impact.",
  },
  {
    header: "Our Rector",
    content:
      "The rector of Stella University, Dr. Ana Marković, is a distinguished academic and leader in the field of education and research. With over 15 years of experience in the academic community, Dr. Marković is known for her contributions to the field of bioengineering. Dr. Marković earned her doctorate at a prestigious university abroad and has published numerous scientific papers in international journals. Her career includes work at various universities and research institutions, where she gained extensive experience in teaching and research. As rector, Dr. Marković is dedicated to enhancing the quality of education and research at Stella University. Her vision includes developing innovative study programs that meet the needs of the modern labor market and fostering collaboration with leading companies and research institutions. Dr. Marković is known for her student-centered approach, providing support and resources for their professional and personal development. Under her leadership, Stella University has achieved significant success in research and innovation, making it one of the leading universities in the region.",
  },
];


export let engineeringOnamaCards: ONamaCard[] = [
  {
    header: "Инжењерски Факултет",
    content:
      "Инжењерски факултет је високошколска установа која пружа образовање у различитим областима инжењерства, као што су машинство, електротехника, грађевинарство, информатика и многе друге. Студенти на овом факултету стичу теоријска и практична знања која су неопходна за решавање сложених техничких проблема и развој нових технологија. Програм студија обухвата предмете из математике, физике, хемије, као и специјализоване инжењерске предмете. Поред тога, студенти имају прилику да учествују у лабораторијским вежбама, пројектима и праксама које им омогућавају да примене стечено знање у реалним ситуацијама. Инжењерски факултет такође подстиче развој критичког мишљења, креативности и тимског рада, што су кључне вештине за успешну каријеру у инжењерству. Након завршетка студија, дипломци могу радити у различитим индустријама, истраживачким институцијама или наставити своје образовање на мастер и докторским студијама.",
  },
  {
    header: "Наш Декан",
    content:
      "Ректор имагинарног инжењерског факултета, др. Александар Петровић, је истакнути стручњак у области електротехнике са преко 25 година искуства у академској заједници и индустрији. Докторирао је на престижном универзитету и објавио бројне научне радове у међународним часописима. Као ректор, др. Петровић је посвећен унапређењу квалитета образовања и истраживања на факултету. Његова визија укључује развој иновативних студијских програма који одговарају потребама савременог тржишта рада, као и подстицање сарадње са водећим компанијама и истраживачким институцијама. Др. Петровић је познат по свом приступу који ставља студенте у центар пажње, пружајући им подршку и ресурсе за њихов професионални и лични развој. Под његовим вођством, факултет је постигао значајне успехе у области истраживања и иновација, што га чини једним од водећих инжењерских факултета у региону. Да ли желиш да сазнаш више о некој специфичној области или пројекту на којем ради овај имагинарни факултет?",
  },
];
export let engineeringOnamaCardsEN: ONamaCard[] = [
  {
    header: "Faculty of Engineering",
    content:
      "The Faculty of Engineering is a higher education institution that provides education in various fields of engineering, such as mechanical engineering, electrical engineering, civil engineering, computer science, and many others. Students at this faculty acquire theoretical and practical knowledge necessary for solving complex technical problems and developing new technologies. The study program includes subjects in mathematics, physics, chemistry, as well as specialized engineering subjects. Additionally, students have the opportunity to participate in laboratory exercises, projects, and internships that allow them to apply the acquired knowledge in real situations. The Faculty of Engineering also encourages the development of critical thinking, creativity, and teamwork, which are key skills for a successful career in engineering. After graduation, alumni can work in various industries, research institutions, or continue their education at the master's and doctoral levels.",
  },
  {
    header: "Our Dean",
    content:
      "The dean of the imaginary Faculty of Engineering, Dr. Aleksandar Petrović, is a distinguished expert in the field of electrical engineering with over 25 years of experience in the academic community and industry. He earned his doctorate at a prestigious university and has published numerous scientific papers in international journals. As dean, Dr. Petrović is dedicated to improving the quality of education and research at the faculty. His vision includes developing innovative study programs that meet the needs of the modern labor market and fostering collaboration with leading companies and research institutions. Dr. Petrović is known for his student-centered approach, providing support and resources for their professional and personal development. Under his leadership, the faculty has achieved significant success in research and innovation, making it one of the leading engineering faculties in the region. Would you like to learn more about a specific area or project that this imaginary faculty is working on?",
  },
];


export let artsOnamaCards: ONamaCard[] = [
  {
    header: "Уметнички Факултет",
    content:
      "Уметнички факултет је престижна образовна институција која пружа образовање у различитим областима уметности, као што су ликовне уметности, музика, позориште и филм. Студенти на овом факултету стичу теоријска и практична знања која су неопходна за развој њихових уметничких вештина и креативности.",
  },
  {
    header: "Наш Декан",
    content:
      "Декан Уметничког факултета, проф. др. Јелена Петровић, је истакнута уметница и академкиња са богатим искуством у области ликовних уметности. Њена визија је да подстакне креативност и иновације међу студентима, пружајући им подршку и ресурсе за њихов уметнички развој. Под њеним вођством, факултет је постао један од водећих уметничких факултета у региону.",
  },
];
export let artsOnamaCardsEN: ONamaCard[] = [
  {
    header: "Faculty of Arts",
    content:
      "The Faculty of Arts is a prestigious educational institution that provides education in various fields of art, such as fine arts, music, theater, and film. Students at this faculty acquire theoretical and practical knowledge necessary for the development of their artistic skills and creativity.",
  },
  {
    header: "Our Dean",
    content:
      "The dean of the Faculty of Arts, Prof. Dr. Jelena Petrović, is a distinguished artist and academic with extensive experience in the field of fine arts. Her vision is to encourage creativity and innovation among students, providing them with support and resources for their artistic development. Under her leadership, the faculty has become one of the leading arts faculties in the region.",
  },
];




export let scienceOnamaCards: ONamaCard[] = [
  {
    header: "Хемијски Факултет",
    content:
      "Хемијски факултет је високошколска установа која пружа образовање у различитим областима хемије, као што су органска хемија, неорганска хемија, аналитичка хемија и биохемија. Студенти на овом факултету стичу теоријска и практична знања која су неопходна за разумевање и истраживање хемијских процеса и материја.",
  },
  {
    header: "Наш Декан",
    content:
      "Декан Хемијског факултета, проф. др. Марко Николић, је истакнути стручњак у области органске хемије са преко 20 година искуства у академској заједници и истраживању. Његова визија укључује развој иновативних студијских програма и подстицање сарадње са водећим истраживачким институцијама и организацијама. Под његовим вођством, факултет је постигао значајне успехе у области истраживања и образовања.",
  },
];
export let scienceOnamaCardsEN: ONamaCard[] = [
  {
    header: "Faculty of Chemistry",
    content:
      "The Faculty of Chemistry is a higher education institution that provides education in various fields of chemistry, such as organic chemistry, inorganic chemistry, analytical chemistry, and biochemistry. Students at this faculty acquire theoretical and practical knowledge necessary for understanding and researching chemical processes and substances.",
  },
  {
    header: "Our Dean",
    content:
      "The dean of the Faculty of Chemistry, Prof. Dr. Marko Nikolić, is a distinguished expert in the field of organic chemistry with over 20 years of experience in the academic community and research. His vision includes developing innovative study programs and fostering collaboration with leading research institutions and organizations. Under his leadership, the faculty has achieved significant success in research and education.",
  },
];


export let medicalOnamaCards: ONamaCard[] = [
  {
    header: "Медицински Факултет",
    content:
      "Медицински факултет је престижна високошколска установа која пружа образовање у области медицине и здравствених наука. Студенти овде стичу знања и вештине неопходне за пружање квалитетне здравствене неге и лечење пацијената. Програм студија обухвата предмете из анатомије, физиологије, биохемије, фармакологије и клиничке медицине. Поред теоријске наставе, студенти учествују у клиничким вежбама и праксама у болницама и здравственим установама, где стичу практично искуство у раду са пацијентима. Медицински факултет такође подстиче истраживачки рад и иновације у области медицине, што доприноси унапређењу здравствене заштите и лечења болести. Након завршетка студија, дипломци могу радити као лекари, истраживачи или наставити своје образовање на специјалистичким и докторским студијама.",
  },
  {
    header: "Наш Декан",
    content:
      "Декан медицинског факултета, др. Јелена Марковић, је угледни стручњак у области интерне медицине са преко 20 година искуства у клиничкој пракси и академској заједници. Докторирала је на реномираном универзитету и објавила бројне научне радове у престижним медицинским часописима. Као декан, др. Марковић је посвећена унапређењу квалитета образовања и истраживања на факултету. Њена визија укључује развој савремених студијских програма који одговарају потребама здравственог система и подстицање сарадње са водећим здравственим установама и истраживачким центрима. Др. Марковић је позната по свом приступу који ставља пацијенте и студенте у центар пажње, пружајући им подршку и ресурсе за њихов професионални и лични развој. Под њеним вођством, факултет је постигао значајне успехе у области медицинских истраживања и иновација, што га чини једним од водећих медицинских факултета у региону.",
  },
];
export let medicalOnamaCardsEN: ONamaCard[] = [
  {
    header: "Faculty of Medicine",
    content:
      "The Faculty of Medicine is a prestigious higher education institution that provides education in the field of medicine and health sciences. Students here acquire the knowledge and skills necessary to provide quality healthcare and treat patients. The study program includes subjects in anatomy, physiology, biochemistry, pharmacology, and clinical medicine. In addition to theoretical teaching, students participate in clinical exercises and internships in hospitals and healthcare institutions, where they gain practical experience in working with patients. The Faculty of Medicine also encourages research work and innovation in the field of medicine, contributing to the improvement of healthcare and disease treatment. After graduation, alumni can work as doctors, researchers, or continue their education in specialist and doctoral studies.",
  },
  {
    header: "Our Dean",
    content:
      "The dean of the Faculty of Medicine, Dr. Jelena Marković, is a renowned expert in the field of internal medicine with over 20 years of experience in clinical practice and the academic community. She earned her doctorate at a prestigious university and has published numerous scientific papers in esteemed medical journals. As dean, Dr. Marković is dedicated to improving the quality of education and research at the faculty. Her vision includes developing modern study programs that meet the needs of the healthcare system and fostering collaboration with leading healthcare institutions and research centers. Dr. Marković is known for her approach that places patients and students at the center, providing them with support and resources for their professional and personal development. Under her leadership, the faculty has achieved significant success in medical research and innovation, making it one of the leading medical faculties in the region.",
  },
];

export let lawOnamaCards: ONamaCard[] = [
  {
    header: "Правни Факултет",
    content:
      "Правни факултет је реномирана високошколска установа која пружа образовање у области права и правних наука. Студенти на овом факултету стичу знања и вештине неопходне за разумевање и примену правних норми у различитим областима права, као што су грађанско, кривично, привредно и међународно право. Програм студија обухвата предмете из теорије права, уставног права, правне историје, као и специјализоване правне предмете. Поред теоријске наставе, студенти учествују у симулацијама судских процеса, правним клиникама и праксама у адвокатским канцеларијама и судовима, где стичу практично искуство у раду са правним случајевима. Правни факултет такође подстиче развој критичког мишљења, аналитичких вештина и етичког приступа правним питањима, што су кључне вештине за успешну каријеру у праву. Након завршетка студија, дипломци могу радити као адвокати, судије, правни саветници или наставити своје образовање на мастер и докторским студијама.",
  },
  {
    header: "Наш Декан",
    content:
      "Декан правног факултета, проф. др. Милан Јовановић, је истакнути правни стручњак са преко 30 година искуства у академској заједници и правној пракси. Докторирао је на престижном универзитету и објавио бројне научне радове у међународним правним часописима. Као декан, проф. др. Јовановић је посвећен унапређењу квалитета образовања и истраживања на факултету. Његова визија укључује развој иновативних студијских програма који одговарају потребама савременог правног система и подстицање сарадње са водећим адвокатским канцеларијама и правним институцијама. Проф. др. Јовановић је познат по свом приступу који ставља студенте у центар пажње, пружајући им подршку и ресурсе за њихов професионални и лични развој. Под његовим вођством, факултет је постигао значајне успехе у области правних истраживања и иновација, што га чини једним од водећих правних факултета у региону.",
  },
];
export let lawOnamaCardsEN: ONamaCard[] = [
  {
    header: "Faculty of Law",
    content:
      "The Faculty of Law is a renowned higher education institution that provides education in the field of law and legal sciences. Students at this faculty acquire the knowledge and skills necessary to understand and apply legal norms in various areas of law, such as civil, criminal, commercial, and international law. The study program includes subjects in legal theory, constitutional law, legal history, as well as specialized legal subjects. In addition to theoretical teaching, students participate in mock trials, legal clinics, and internships in law firms and courts, where they gain practical experience in handling legal cases. The Faculty of Law also encourages the development of critical thinking, analytical skills, and an ethical approach to legal issues, which are key skills for a successful career in law. After graduation, alumni can work as lawyers, judges, legal advisors, or continue their education at the master's and doctoral levels.",
  },
  {
    header: "Our Dean",
    content:
      "The dean of the Faculty of Law, Prof. Dr. Milan Jovanović, is a distinguished legal expert with over 30 years of experience in the academic community and legal practice. He earned his doctorate at a prestigious university and has published numerous scientific papers in international legal journals. As dean, Prof. Dr. Jovanović is dedicated to improving the quality of education and research at the faculty. His vision includes developing innovative study programs that meet the needs of the modern legal system and fostering collaboration with leading law firms and legal institutions. Prof. Dr. Jovanović is known for his student-centered approach, providing support and resources for their professional and personal development. Under his leadership, the faculty has achieved significant success in legal research and innovation, making it one of the leading law faculties in the region.",
  },
];


export let businessOnamaCards: ONamaCard[] = [
  {
    header: "Бизнис Факултет",
    content:
      "Бизнис факултет је престижна високошколска установа која пружа образовање у области пословних и економских наука. Студенти на овом факултету стичу знања и вештине неопходне за успешно управљање пословним процесима и доношење стратешких одлука у различитим индустријама. Програм студија обухвата предмете из економије, менаџмента, маркетинга, финансија, рачуноводства и предузетништва. Поред теоријске наставе, студенти учествују у практичним вежбама, пројектима и праксама у компанијама, где стичу практично искуство у раду са пословним изазовима. Бизнис факултет такође подстиче развој лидерских вештина, креативности и иновативности, што су кључне вештине за успешну каријеру у пословном свету. Након завршетка студија, дипломци могу радити као менаџери, консултанти, предузетници или наставити своје образовање на мастер и докторским студијама.",
  },
  {
    header: "Наш Декан",
    content:
      "Декан бизнис факултета, проф. др. Марина Николић, је угледни стручњак у области менаџмента и маркетинга са преко 20 година искуства у академској заједници и индустрији. Докторирала је на реномираном универзитету и објавила бројне научне радове у престижним пословним часописима. Као декан, проф. др. Николић је посвећена унапређењу квалитета образовања и истраживања на факултету. Њена визија укључује развој савремених студијских програма који одговарају потребама тржишта рада и подстицање сарадње са водећим компанијама и истраживачким институцијама. Проф. др. Николић је позната по свом приступу који ставља студенте у центар пажње, пружајући им подршку и ресурсе за њихов професионални и лични развој. Под њеним вођством, факултет је постигао значајне успехе у области пословних истраживања и иновација, што га чини једним од водећих бизнис факултета у региону.",
  },
];
export let businessOnamaCardsEN: ONamaCard[] = [
  {
    header: "Faculty of Business",
    content:
      "The Faculty of Business is a prestigious higher education institution that provides education in the field of business and economic sciences. Students at this faculty acquire the knowledge and skills necessary for successfully managing business processes and making strategic decisions in various industries. The study program includes subjects in economics, management, marketing, finance, accounting, and entrepreneurship. In addition to theoretical teaching, students participate in practical exercises, projects, and internships in companies, where they gain practical experience in dealing with business challenges. The Faculty of Business also encourages the development of leadership skills, creativity, and innovation, which are key skills for a successful career in the business world. After graduation, alumni can work as managers, consultants, entrepreneurs, or continue their education at the master's and doctoral levels.",
  },
  {
    header: "Our Dean",
    content:
      "The dean of the Faculty of Business, Prof. Dr. Marina Nikolić, is a renowned expert in the field of management and marketing with over 20 years of experience in the academic community and industry. She earned her doctorate at a prestigious university and has published numerous scientific papers in esteemed business journals. As dean, Prof. Dr. Nikolić is dedicated to improving the quality of education and research at the faculty. Her vision includes developing modern study programs that meet the needs of the labor market and fostering collaboration with leading companies and research institutions. Prof. Dr. Nikolić is known for her student-centered approach, providing support and resources for their professional and personal development. Under her leadership, the faculty has achieved significant success in business research and innovation, making it one of the leading business faculties in the region.",
  },
];
