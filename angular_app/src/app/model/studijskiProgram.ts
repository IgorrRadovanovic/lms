import { Fakultet } from "./fakultet";
import { GodinaStudija } from "./godinaStudija";
import { Nastavnik } from "./nastavnik";
import { Predmet } from "./predmet";

export interface StudijskiProgram {
  id: number;
  naziv: string;
  opis: string;
  rukovodilac: Nastavnik;
  rukovodilacId: number;
  godinaStudija: GodinaStudija;
  godinaStudijaId: number;
  fakultet: Fakultet;
  fakultetId: number;
  predmeti: Predmet[];
}
