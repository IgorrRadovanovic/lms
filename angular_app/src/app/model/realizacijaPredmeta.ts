import { EvaluacijaZnanja } from "./evaluacijaZnanja";
import { NastavnikNaRealizaciji } from "./nastavnikNaRealizaciji";
import { PohadjanjePredmeta } from "./pohadjanjePredmeta";
import { Predmet } from "./predmet";
import { TerminNastave } from "./terminNastave";

export interface RealizacijaPredmeta{
    id: number;
    predmet: Predmet;
    predmetId: number;
    pohadjanjePredmeta?: PohadjanjePredmeta[];
    nastavnici?: NastavnikNaRealizaciji[];
    termini?: TerminNastave[];
    evaluacije?: EvaluacijaZnanja[];
    brPolaganja: number;

}