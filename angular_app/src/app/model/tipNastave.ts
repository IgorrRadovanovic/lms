import { NastavnikNaRealizaciji } from "./nastavnikNaRealizaciji";

export interface TipNastave{
    id:number;
    naziv: string;
    realizacije: NastavnikNaRealizaciji[];
    
}