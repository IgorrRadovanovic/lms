import { Adresa } from "./adresa";
import { PohadjanjePredmeta } from "./pohadjanjePredmeta";
import { StudentNaGodini } from "./studentNaGodini";


export interface Student{
    id: number;
    jmbg: string;
    ime: string;
    adresaId: number;
    adresa: Adresa;
    brojTelefona: number;
    email: string;
    studentNaGodinama:StudentNaGodini[];
}
export interface StudentZaBazu{
    id: number;
    jmbg: string;
    ime: string;
    adresa: Adresa;
    brojTelefona: number;
    email: string;
}