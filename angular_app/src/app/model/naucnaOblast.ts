import { Zvanje } from "./zvanje";

export interface NaucnaOblast{
    id: number;
    naziv: string;
    lista_zvanja: Zvanje[];
    
}