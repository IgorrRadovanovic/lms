import { NastavnikNaRealizaciji } from "./nastavnikNaRealizaciji";
import { Zvanje } from "./zvanje";

export interface Nastavnik{
    id: number;
    ime: string;
    biografija: string;
    jmbg: string;
    zvanja: Zvanje[];
    realizacije: NastavnikNaRealizaciji[];
    
}