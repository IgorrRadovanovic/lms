import { Nastavnik } from "./nastavnik";
import { NaucnaOblast } from "./naucnaOblast";
import { TipZvanja } from "./tipZvanja";

export interface Zvanje{
    id: number;
    oblast: NaucnaOblast;
    tip: TipZvanja;
    datum_izbora: string;
    datum_isteka:string;
    nosilac_zvanja: Nastavnik;
}