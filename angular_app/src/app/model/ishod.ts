import { EvaluacijaZnanja } from "./evaluacijaZnanja";
import { ObrazovniCilj } from "./obrazovniCilj";
import { Silabus } from "./silabus";
import { TerminNastave } from "./terminNastave";


export interface Ishod {
    id: number;
    naziv: string;
    silabus: Silabus;
    silabusId: number;
    obrazovniCilj: ObrazovniCilj;
    obrazovniCiljId: number;
    terminiNastave: TerminNastave[];

}
