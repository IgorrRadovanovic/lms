import { Mesto } from "./mesto";
import { Univerzitet } from "./univerzitet";
import { Fakultet } from "./fakultet";
import { Student } from "./student";
import { Nastavnik } from "./nastavnik";

export interface Adresa {
    id: number;
    ulica: string;
    broj: string;
    mesto: Mesto;
    univerziteti?: Univerzitet  [];
    fakulteti?: Fakultet[];
    studenti?: Student[];
    nastavnici?: Nastavnik[];
}
