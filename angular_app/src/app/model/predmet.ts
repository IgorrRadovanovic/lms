import { GodinaStudija } from "./godinaStudija";
import { RealizacijaPredmeta } from "./realizacijaPredmeta";
import { Silabus } from "./silabus";
import { StudijskiProgram } from "./studijskiProgram";

export interface Predmet{
    id:number;
    naziv: string;
    espb: number;
    obavezan: boolean;
    brojPredavanja: number;
    drugiObliciNastave: number;
    istrazivackiRad: number;
    ostaliCasovi: number;
    silabus: Silabus;
    silabusId: number;
    realizacijePredmeta: RealizacijaPredmeta[];
    godinaStudija: GodinaStudija;
    godinaStudijaId: number;
    preduslov: Predmet;
    preduslovId: number;
    studijskiProgram: StudijskiProgram;
    studijskiProgramId: number;
}