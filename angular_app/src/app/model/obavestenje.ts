import { Fajl } from "./fajl";

export interface Obavestenje{
    id: number;
    naslov: string;
    sadrzaj: string;
    vremePostavljanja: Date;
    fajlovi: Fajl[];
}