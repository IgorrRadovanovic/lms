import { Zvanje } from "./zvanje";

export interface TipZvanja{
    id:number;
    naziv: string;
    lista_zvanja: Zvanje[];
    
}