import { EvaluacijaZnanja } from "./evaluacijaZnanja";

export interface InstrumentEvaluacije{
    id:number;
    naziv: string;
    evaluacije:EvaluacijaZnanja[];
}