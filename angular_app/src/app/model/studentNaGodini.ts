import { GodinaStudija } from "./godinaStudija";
import { PohadjanjePredmeta } from "./pohadjanjePredmeta";
import { Polaganje } from "./polaganje";
import { Student } from "./student";

export interface StudentNaGodini{
    id: number;
    datumUpisa: Date;
    brojIndeksa: String;
    pohadjanjePredmeta: PohadjanjePredmeta[];
    godinaStudija: GodinaStudija;
    godinaStudijaId: number;
    polaganja: Polaganje[];
    student: Student;
}