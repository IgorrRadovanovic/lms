import { Drzava } from "./drzava"; 
import { Adresa } from "./adresa"; 

export interface Mesto {
    id: number;
    drzava?: Drzava;
    naziv: string;
    adrese?: Adresa[];
}
