import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { ColDef } from "ag-grid-community";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-quartz.css";
import { GenericService } from "../../services/generic/generic.service";
import { Polaganje } from "../../model/polaganje";
import { RealizacijaPredmeta } from "../../model/realizacijaPredmeta";
import { Predmet } from "../../model/predmet";
import { PohadjanjePredmeta } from "../../model/pohadjanjePredmeta";
import { TablePredmet } from "../../model/tablePredmet";
import { EvaluacijaZnanja } from "../../model/evaluacijaZnanja";
import { BrPolaganjaRealizacija } from "../../model/brPolaganjaRealizacija";
import { firstValueFrom } from "rxjs";
import { LoggedStudentService } from "../../services/loggedStudent/logged-student.service";
import { NgIf } from "@angular/common";
import { UserService } from "../../services/userService/user.service";
import { filter } from "lodash";
import { PolaganjeService } from "../../services/polaganjeService/polaganje.service";
import { StudentNaGodini } from "../../model/studentNaGodini";
import { StudentNaGodiniService } from "../../services/studentNaGodService/student-na-godini.service";
import { EvaluacijaService } from "../../services/evaluacijaService/evaluacija.service";
import { RealizacijeService } from "../../services/realizacijeService/realizacije.service";
import { PohadjanjaService } from "../../services/pohadjanjaService/pohadjanja.service";

@Component({
  selector: "app-history-table",
  standalone: true,
  imports: [AgGridAngular, NgIf],
  template: `
    <ag-grid-angular
      class="ag-theme-quartz"
      [style.height.px]="tableHeight"
      [rowData]="rowData"
      [columnDefs]="colDefs"
      [defaultColDef]="defaultColDef"
      [pagination]="pagination"
      [paginationPageSize]="paginationPageSize"
      [paginationPageSizeSelector]="paginationPageSizeSelector" />
    <div *ngIf="table == 'passed'">Ukupan broj osvojenih ESPB bodova tokom školovanja: {{ sviBodovi }}</div>
  `,
  styleUrl: "./history-table.component.css",
})
export class HistoryTableComponent implements OnInit {
  constructor(
    private myStudentService: LoggedStudentService,
    private evService: EvaluacijaService,
    private pohadjanjaService: GenericService<PohadjanjePredmeta>,
    private polService: PolaganjeService,
    private predmetService: GenericService<Predmet>,
    private relPredmetaService: GenericService<RealizacijaPredmeta>,
    private changeDetectorRef: ChangeDetectorRef,
    private studGodService: StudentNaGodiniService,
    private userService: UserService,
    private pohService: PohadjanjaService
  ) {}

  pagination = true;
  paginationPageSize = 10;
  paginationPageSizeSelector = [10, 20];

  defaultColDef: ColDef = {
    flex: 1,
    filter: true,
  };

  @Input()
  table: string = "";

  data: any[] = [];
  tableHeight: number = 525;
  columns: { field: string }[] = [];
  colDefs: ColDef[] = [];
  rowData: any[] = [];
  realizacijePredmetaId: number[] = [];
  studentPohadjanja: PohadjanjePredmeta[] = [];
  polozeniPredmeti: TablePredmet[] = [];
  nePolozeniPredmeti: TablePredmet[] = [];
  brPolaganjaRealizacija: BrPolaganjaRealizacija = [];
  ukupnoBodovaRp: { [key: number]: number } = {};
  sviBodovi: number = 0;
  userId: number | null = null;
  svaPolaganja: Polaganje[] = [];
  filteredEv: EvaluacijaZnanja[] = [];
  studGod: StudentNaGodini[] = [];
  po: any[] = [];
  pohadjanja: PohadjanjePredmeta[] = [];
  async ngOnInit() {
    await this.userService.getUserId().then((uid) => {
      if (uid) {
        this.userId = uid;
      }
    });
    this.updateData();
  }

  async updateData() {
    try {
      if (this.userId) {
        this.studGod = await firstValueFrom(this.studGodService.getByStudentID("studentinagodini", this.userId)); //Svi studNaGod tog studenta

        const predmeti = await this.myStudentService.getPredmetiForStudent(this.userId);
        for (const item of predmeti) {
          let polPredmet = new TablePredmet(item.id, item.naziv, 0, 0, 0);
          this.polozeniPredmeti.push(polPredmet); // Svi predmeti tog studenta i instanciranje njihovih objekata za tabelu
        }

        for (const sg of this.studGod) {
          const p = await firstValueFrom(this.pohService.getByStudGodID("pohadjanjapredmeta", sg.id));
          this.po.push(p);
        }
        for (const p of this.po) {
          for (const pohadjanje of p) {
            this.pohadjanja.push(pohadjanje); // Sva pohadjanja tog studenta
            this.realizacijePredmetaId.push(pohadjanje.realizacijaPredmetaId);
          }
        }
        for (const rpId of this.realizacijePredmetaId) {
          const ev = await firstValueFrom(this.evService.getByRelId(rpId, "evaluacijeznanja"));
          if (ev) {
            this.filteredEv.push(ev); // sve evaluacije znanja tog studenta
          }
        }

        // this.brPolaganjaRealizacija = this.filteredEv.reduce((acc: BrPolaganjaRealizacija, ev) => {
        //   // broj Polaganja
        //   console.log(ev, 've')
        //   acc[ev.realizacijaPredmetaId] = 1;
        //   return acc;
        // }, {});

        if (this.userId) {
          for (const sg of this.studGod) {
            const polaganje = await firstValueFrom(this.polService.getByStudGodID("polaganja", sg.id));
            for (const p of polaganje) {
              if (p) this.svaPolaganja.push(p);
            }
          }
        }
        this.filteredEv.forEach((fev) => {
          fev.brojBodova = 0;
        });

        console.log(this.svaPolaganja, "sva polaganja");
        this.filteredEv.forEach((fev) => {
          this.brPolaganjaRealizacija[fev.realizacijaPredmetaId] = 0;
        });
        this.svaPolaganja.forEach((p) => {
          this.filteredEv.forEach((fev) => {
            if (p.evaluacijaZnanja.realizacijaPredmetaId == fev.realizacijaPredmetaId) {
              this.brPolaganjaRealizacija[fev.realizacijaPredmetaId] += 1;
              fev.brojBodova += p.bodovi;
            }
          });
        });

        this.filteredEv.forEach((fev) => {
          this.ukupnoBodovaRp[fev.realizacijaPredmetaId] = fev.brojBodova;
        });


        
        for (const sp of this.pohadjanja) {
          const rp = await firstValueFrom(this.relPredmetaService.getById(sp.realizacijaPredmetaId, "realizacijepredmeta"));
          if (rp) {
            this.polozeniPredmeti.forEach((pp, index) => {
              if (pp.id === rp.predmet.id) {
                pp.brojPolaganja = this.brPolaganjaRealizacija[rp.id] || 0;
                pp.konacniBodovi = this.ukupnoBodovaRp[rp.id] || 0;
                if (sp.konacnaOcena == null) {
                  let p = this.polozeniPredmeti.splice(index, 1);
                  this.nePolozeniPredmeti.push(p[0]);
                } else {
                  pp.ocena = sp.konacnaOcena;
                }
              }
            });
          }
        }

        this.rowData = [];
        if (this.table == "passed") {
          this.polozeniPredmeti.forEach((pp) => {
            this.predmetService.getById(pp.id, "predmeti").subscribe((pp) => (this.sviBodovi += pp.espb));
            this.rowData.push(pp);
          });
        } else if (this.table == "nonPassed") {
          this.nePolozeniPredmeti.forEach((np) => this.rowData.push(np));
        }

        this.adjustTableHeight();
        this.colDefs = [{ field: "nazivPredmeta" }, { field: "brojPolaganja" }, { field: "konacniBodovi" }, { field: "ocena" }];
      }
    } catch (error) {
      console.error("Error updating data", error);
    }
  }

  isMobileDevice() {
    return /Mobi|Android/i.test(navigator.userAgent);
  }

  removeFormatter() {
    this.colDefs = this.colDefs.map((colDef) => {
      return {
        ...colDef,
        valueFormatter: undefined,
      };
    });

    this.changeDetectorRef.detectChanges();
  }

  godinaStudijaFormatter(params: any): string {
    const godStud = params.value;
    return `${godStud.godina}`;
  }

  preduslovFormatter(params: any): string {
    const preduslov = params.value;
    return `${preduslov.naziv}`;
  }

  obrazovniCiljFormatter(params: any): string {
    const obrCilj = params.value;
    return `${obrCilj.opis}`;
  }

  adjustTableHeight() {
    const rowCount = this.rowData.length;
    this.tableHeight = 150 + rowCount * 30;
    // this.tableHeight = rowCount >= 10 || rowCount <= 3 ? 425 : (rowCount / 8) * 525;
  }
}
