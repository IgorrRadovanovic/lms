import { Component, OnInit } from "@angular/core";
import { HistoryTableComponent } from "./history-table/history-table.component";
import { GenericService } from "../services/generic/generic.service";
import { Student } from "../model/student";
import { StudentNaGodini } from "../model/studentNaGodini";
import { NgIf } from "@angular/common";

@Component({
  selector: "app-study-history",
  standalone: true,
  imports: [HistoryTableComponent, NgIf],
  templateUrl: "./study-history.component.html",
  styleUrl: "./study-history.component.css",
})
export class StudyHistoryComponent {
  constructor() {}
  menu: string = "passed";

  setMenu(setMenu: string) {
    this.menu = setMenu;
  }
}
