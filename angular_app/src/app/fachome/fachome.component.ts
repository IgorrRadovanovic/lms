import { Component, OnInit, Renderer2 } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router, RouterLink, RouterOutlet } from "@angular/router";
import { Fakultet } from "../model/fakultet";
import { GenericService } from "../services/generic/generic.service";
import { MappingService } from "../services/mappingService/mapping.service";
import { SharedService } from "../services/sharedService/shared.service";
import { Univerzitet } from "../model/univerzitet";
import { switchMap, map, filter } from "rxjs";
import { NgClass, NgFor, NgIf, ViewportScroller } from "@angular/common";
import {
  artsOnamaCards,
  artsOnamaCardsEN,
  businessOnamaCards,
  engineeringOnamaCards,
  engineeringOnamaCardsEN,
  lawOnamaCards,
  lawOnamaCardsEN,
  medicalOnamaCards,
  medicalOnamaCardsEN,
  ONamaCard,
  scienceOnamaCards,
  scienceOnamaCardsEN,
} from "../model/oNama/onamaCard";
import { TranslateModule, TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-fachome",
  standalone: true,
  imports: [TranslateModule, RouterLink, RouterOutlet, NgIf, NgFor, NgClass],
  templateUrl: "./fachome.component.html",
  styleUrl: "./fachome.component.css",
})
export class FachomeComponent implements OnInit {
  fakultetId: number = 0;
  fakultet: Fakultet | null = null;
  bg_url: string = "";
  menu: string = "mainmenu";
  univerzitet?: Univerzitet | any;
  uniId: number = 0;
  oNama: ONamaCard[] = [];
  isRouterLinkActive = false;
  isStudijskiProgramActive = false;

  constructor(
    private translate: TranslateService,
    private vps: ViewportScroller,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private mappingService: MappingService,
    private facService: GenericService<Fakultet>
  ) {
    this.translate.setDefaultLang("sr");

    this.translate.onLangChange.subscribe(() => {
      if (this.fakultet) {
        this.setOnama(this.fakultet);
      }
    });

    this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe(() => {
      const currentUrl = this.router.url;
      this.isStudijskiProgramActive = currentUrl.includes("studijskiProgram");
    });

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isRouterLinkActive = this.router.url.includes("studijskiProgram");
      }
    });
  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit() {
    this.univerzitet = this.sharedService.getActiveUni();
    this.fakultet = this.sharedService.getActiveFac();
    if (this.univerzitet != null && this.fakultet != null) {
      this.bg_url = "assets/images/univerziteti/" + this.univerzitet.naziv.toLowerCase() + "/" + this.univerzitet.naziv.toLowerCase() + "_background.jpg";
    } else {
      // console.log(this.univerzitet, "univerzitet i fakultet su null");
      this.route.params
        .pipe(
          switchMap((params) => {
            this.fakultetId = params["facId"];
            return this.facService.getById(this.fakultetId, "fakulteti");
          }),
          switchMap((fac) => {
            this.setOnama(fac);

            this.uniId = fac.univerzitet.id;
            this.mappingService.mapFacById(fac.id).subscribe((e) => {
              this.fakultet = e;
            });
            return this.mappingService.mapUniById(this.uniId);
          }),
          map((uni) => {
            this.univerzitet = uni;
            this.bg_url = "assets/images/univerziteti/" + this.univerzitet.naziv.toLowerCase() + "/" + this.univerzitet.naziv.toLowerCase() + "_background.jpg";
          })
        )
        .subscribe();
    }
  }

  setOnama(fakultet: Fakultet) {
    if (this.translate.currentLang == undefined) {
      this.translate.currentLang = "sr";
    }
    if (fakultet.naziv.toLowerCase() == "faculty of engineering" && this.translate.currentLang == "sr") {
      this.oNama = engineeringOnamaCards;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of engineering" && this.translate.currentLang == "en") {
      this.oNama = engineeringOnamaCardsEN;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of arts" && this.translate.currentLang == "sr") {
      this.oNama = artsOnamaCards;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of arts" && this.translate.currentLang == "en") {
      this.oNama = artsOnamaCardsEN;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of science" && this.translate.currentLang == "sr") {
      this.oNama = scienceOnamaCards;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of science" && this.translate.currentLang == "en") {
      this.oNama = scienceOnamaCardsEN;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of medicine" && this.translate.currentLang == "sr") {
      this.oNama = medicalOnamaCards;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of medicine" && this.translate.currentLang == "en") {
      this.oNama = medicalOnamaCardsEN;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of law" && this.translate.currentLang == "sr") {
      this.oNama = lawOnamaCards;
    }
    if (fakultet.naziv.toLowerCase() == "faculty of bussiness" && this.translate.currentLang == "en") {
      this.oNama = lawOnamaCardsEN;
    }
  }

  mainMenu() {
    this.menu = "mainmenu";
  }
  noMenu() {
    this.menu = "nomenu";
  }
  onama(): void {
    if(this.fakultet){
    this.setOnama(this.fakultet);}
    this.router.navigate([], { fragment: "facHome" }).then(() => {
      this.vps.scrollToAnchor("top");
    });
    this.menu = "onama";
  }
}
