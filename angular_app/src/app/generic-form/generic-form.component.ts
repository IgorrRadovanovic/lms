import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, ReactiveFormsModule } from "@angular/forms";
import { InputDetail } from "./input-detail";
import { NgIf, NgFor, CommonModule } from "@angular/common";

@Component({
  selector: "app-generic-form",
  standalone: true,
  imports: [NgFor, ReactiveFormsModule, NgIf, CommonModule],
  templateUrl: "./generic-form.component.html",
  styleUrl: "./generic-form.component.css",
})
export class GenericFormComponent {
  @Input()
  forma?: FormGroup;
  @Input()
  details: InputDetail[] = [];
  @Input()
  readonly: boolean = false;
  myInput: any;

  constructor() {}

  @Output()
  cancelEvent = new EventEmitter<string>();
  @Output()
  submitEvent = new EventEmitter<[]>();

  edit() {
    this.readonly = false;
  }
  submit() {
    if (this.forma) this.submitEvent.emit(this.forma.value);
  }
  cancel() {
    this.readonly = true;
    this.forma?.reset();
    this.cancelEvent.emit("cancelled");
  }
  returnKeys(group: FormGroup): string[] {
    return Object.keys(group.controls);
  }
}
