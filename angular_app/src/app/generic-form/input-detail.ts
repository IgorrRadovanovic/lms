
export interface InputDetail{
    input_type:string,
    input_label?:string|null|undefined,
    input_num_min?:number|null|undefined,
    input_num_max?:number|null|undefined,
    input_num_step?:number|null|undefined,
    input_value?:any|null|undefined,
    input_id?:string|null|undefined,
    input_img_src?:string|null|undefined,
    input_img_alt?:string|null|undefined,
    input_width?:number|null|undefined,
    input_height?:number|null|undefined,
    input_pattern?:string|null|undefined,
    input_options?:any[]|null|undefined,
    input_ov_field?:string|null|undefined,
    input_ov_display?:string|null|undefined
}