import { TestBed } from '@angular/core/testing';

import { RealizacijeService } from './realizacije.service';

describe('RealizacijeService', () => {
  let service: RealizacijeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RealizacijeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
