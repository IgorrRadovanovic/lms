import { Injectable } from "@angular/core";
import { RealizacijaPredmeta } from "../../model/realizacijaPredmeta";
import { GenericService } from "../generic/generic.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class RealizacijeService extends GenericService<RealizacijaPredmeta> {
  constructor(protected override http: HttpClient) {
    super(http);
  }
  getByPredmetID(resourceEndpoint: string, otherId: number): Observable<any> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/bypredmet/${otherId}`);
  }
}
