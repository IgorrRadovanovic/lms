import { Injectable } from "@angular/core";
import { GenericService } from "../generic/generic.service";
import { HttpClient } from "@angular/common/http";
import { StudentNaGodini } from "../../model/studentNaGodini";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class StudentNaGodiniService extends GenericService<StudentNaGodini> {
  constructor(protected override http: HttpClient) {
    super(http);
  }
  getByStudentID(resourceEndpoint: string, otherId: number): Observable<any> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/bystudent/${otherId}`);
  }

  getBySsp(resourceEndpoint: string, studId: number, studProId: number): Observable<any> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/byssp/${studId}/${studProId}`);
  }
}
