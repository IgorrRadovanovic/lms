import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, catchError, map, of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class GenericService<T> {
  protected baseUrl: string = "http://localhost:8081/";
  protected url: String = "noUrlFound";
  univerzitetiServices: string[] = [
    "fajlovi",
    "fakulteti",
    "godinestudija",
    "ishodi",
    "obavestenja",
    "obrazovniciljevi",
    "predmeti",
    "realizacijepredmeta",
    "silabusi",
    "studijskiprogrami",
    "termininastave",
    "univerziteti",
  ];
  studentiServices: string[] = ["evaluacijeznanja", "instrumentievaluacije", "pohadjanjapredmeta", "polaganja", "studentinagodini", "studenti", "tipovievaluacije"];
  adreseServices: string[] = ["adrese", "drzave", "mesta"];
  nastavniciServices: string[] = ["nastavnicinarealizaciji", "nastavnici", "naucneoblasti", "tipovinastave", "tipovizvanja", "zvanja"];
  constructor(protected http: HttpClient) {}

  getServiceUrl(resourceEndpoint: string): String {
    if (this.univerzitetiServices.includes(resourceEndpoint)) {
      console.log(resourceEndpoint);
      return this.baseUrl + "univerziteti-service/api/" + resourceEndpoint;
    }
    if (this.studentiServices.includes(resourceEndpoint)) {
      return this.baseUrl + "studenti-service/api/" + resourceEndpoint;
    }
    if (this.adreseServices.includes(resourceEndpoint)) {
      return this.baseUrl + "adrese-service/api/" + resourceEndpoint;
    }
    if (this.nastavniciServices.includes(resourceEndpoint)) {
      return this.baseUrl + "nastavnici-service/api/" + resourceEndpoint;
    }
    console.log(resourceEndpoint)
    return "noServiceFound";
  }

  getHighestId(resourceEndpoint: string) {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/getHighestId`);
  }
  getById(id: number, resourceEndpoint: string): Observable<T> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/${id}`) as Observable<T>;
  }
 
  getAll(resourceEndpoint: string): Observable<T[]> {
    this.url = this.getServiceUrl(resourceEndpoint);
    // console.log(`${this.url}`);
    return this.http.get<T[]>(`${this.url}`);
  }

  patch(id: number, resourceEndpoint: string, item: T) {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.patch(`${this.url}/${id}`, item).pipe(
      map(() => true),
      catchError(() => {
        console.error(`Error editing item with ID ${id}`);
        return of(false);
      })
    );
  }
  renameProperty<T>(item: T, newName: string): { [key: string]: T } {
    return { [newName]: item };
  }

  post(resourceEndpoint: string, item: T) {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.post(`${this.url}`, item).pipe(
      map(() => true),
      catchError(() => {
        console.error(`Error creating new item`);
        return of(false);
      })
    );
  }

  getColumns<T extends Object>(resourceEndpoint: string): Observable<string[]> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get<T[]>(`${this.url}`).pipe(map((data) => (data.length > 0 ? Object.keys(data[0]).slice(1) : [])));
  }
  remove(id: number, resourceEndpoint: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}${resourceEndpoint}/${id}`).pipe(
      map(() => true),
      catchError(() => {
        console.error(`Error deleting item with ID ${id}`);
        return of(false);
      })
    );
  }
}
