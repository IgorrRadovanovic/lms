import { TestBed } from '@angular/core/testing';

import { EvaluacijaService } from './evaluacija.service';

describe('EvaluacijaService', () => {
  let service: EvaluacijaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EvaluacijaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
