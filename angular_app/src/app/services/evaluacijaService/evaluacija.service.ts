import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RealizacijaPredmeta } from "../../model/realizacijaPredmeta";
import { GenericService } from "../generic/generic.service";
import { EvaluacijaZnanja, EvaluacijaZnanjaZaBazu } from "../../model/evaluacijaZnanja";
import { map, catchError, of, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EvaluacijaService extends GenericService<EvaluacijaZnanja> {
  constructor(protected override http: HttpClient) {
    super(http);
  }
  getByRelId(id: number, resourceEndpoint: string): Observable<any> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/byrelid/${id}`);
  }
  override post(resourceEndpoint: string, item: EvaluacijaZnanjaZaBazu) {
    this.url = this.getServiceUrl(resourceEndpoint);
    console.log(this.url);
    return this.http.post(`${this.url}`, item).pipe(
      map(() => true),
      catchError(() => {
        console.error(`Error creating new item`);
        return of(false);
      })
    );
  }
}
