import { ReturnStatement } from "@angular/compiler";
import { Injectable } from "@angular/core";
import { KeycloakService } from "keycloak-angular";

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private kcService: KeycloakService) {}

  async getUserId(): Promise<number | null> {
    const p = await this.kcService.loadUserProfile();
    if (p.attributes) {
      const i = p.attributes["userDataID"] as number[];
      return i[0];
    }
    return null;
  }
  
}
