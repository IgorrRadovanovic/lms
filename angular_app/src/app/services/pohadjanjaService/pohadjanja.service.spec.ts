import { TestBed } from '@angular/core/testing';

import { PohadjanjaService } from './pohadjanja.service';

describe('PohadjanjaService', () => {
  let service: PohadjanjaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PohadjanjaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
