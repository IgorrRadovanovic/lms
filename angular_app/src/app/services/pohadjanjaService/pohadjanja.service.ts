import { Injectable } from '@angular/core';
import { PohadjanjePredmeta } from '../../model/pohadjanjePredmeta';
import { GenericService } from '../generic/generic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PohadjanjaService extends GenericService<PohadjanjePredmeta> {
  constructor(protected override http: HttpClient) {
    super(http);
  }
  getByStudGodID(resourceEndpoint: string, otherId: number): Observable<any> {
    this.url = this.getServiceUrl(resourceEndpoint);
    console.log(this.url)
    return this.http.get(`${this.url}/bystudgod/${otherId}`);
  }
}
