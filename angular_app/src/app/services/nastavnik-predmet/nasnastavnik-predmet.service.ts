import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Nastavnik } from '../../model/nastavnik';
import { GenericService } from '../generic/generic.service';
import { RealizacijaPredmeta } from '../../model/realizacijaPredmeta';
import { Predmet } from '../../model/predmet';
import { Silabus } from '../../model/silabus';
import { Obavestenje } from '../../model/obavestenje';

@Injectable({
  providedIn: 'root'
})
export class NasnastavnikPredmetService {

  constructor(private silabusService: GenericService<Silabus>,private predmetService: GenericService<Predmet>, private relService: GenericService<RealizacijaPredmeta>,private nastService: GenericService<Nastavnik>) { }

  async getPremetiForNastavnik(nastavnikId: number):Promise<Predmet[]> {
    let predmetiId: number[] = [];
    let predmeti: Predmet[] = [];

    const nastavnik = await firstValueFrom(this.nastService.getById(nastavnikId, "nastavnici"));

    for (const rel of nastavnik.realizacije) {
      const realizacija = await firstValueFrom(this.relService.getById(rel.realizacijaPredmetaId, "realizacijepredmeta"));
      predmetiId.push(realizacija.predmetId);
    }
    for (const pId of predmetiId) {
      const p = await firstValueFrom(this.predmetService.getById(pId, "predmeti"));
      predmeti.push(p);
    }
    return predmeti;
  }

  async getSilabusForPredmet(predmetId: number):Promise<Silabus>{
    const predmet = await firstValueFrom(this.predmetService.getById(predmetId, "predmeti"))

    console.log("PredmetSilabusId")
    console.log(predmet.silabus.id)

    let silabusId = predmet.silabus.id

    const silabus = await firstValueFrom(this.silabusService.getById(silabusId,"silabusi"))
    console.log(silabus)
    return silabus
  }
}
