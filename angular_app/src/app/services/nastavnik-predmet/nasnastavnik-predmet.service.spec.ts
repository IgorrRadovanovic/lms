import { TestBed } from '@angular/core/testing';

import { NasnastavnikPredmetService } from './nasnastavnik-predmet.service';

describe('NasnastavnikPredmetService', () => {
  let service: NasnastavnikPredmetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NasnastavnikPredmetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
