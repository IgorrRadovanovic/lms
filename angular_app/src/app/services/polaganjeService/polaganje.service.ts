import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericService } from '../generic/generic.service';
import { Polaganje } from '../../model/polaganje';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PolaganjeService extends GenericService<Polaganje>{

  constructor(protected override http: HttpClient) {
    super(http);
  }
  
  getByStudGodID(resourceEndpoint: string, otherId: number): Observable<any> {
    this.url = this.getServiceUrl(resourceEndpoint);
    return this.http.get(`${this.url}/bystudgod/${otherId}`);
  }

}
