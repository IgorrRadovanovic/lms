import { Injectable } from "@angular/core";
import { firstValueFrom } from "rxjs";
import { Polaganje } from "../../model/polaganje";
import { Predmet } from "../../model/predmet";
import { RealizacijaPredmeta } from "../../model/realizacijaPredmeta";
import { StudentNaGodini } from "../../model/studentNaGodini";
import { GenericService } from "../generic/generic.service";
import { MappingService } from "../mappingService/mapping.service";
import { PohadjanjePredmeta } from "../../model/pohadjanjePredmeta";
import { TablePredmet } from "../../model/tablePredmet";
import { BrPolaganjaRealizacija } from "../../model/brPolaganjaRealizacija";

@Injectable({
  providedIn: "root",
})
export class LoggedStudentService {
  constructor(
    private studMapService: MappingService,
    private relService: GenericService<RealizacijaPredmeta>,
    private predmetService: GenericService<Predmet>, 
    private studGodService: GenericService<StudentNaGodini>,
  ) {}
  sviPredmeti: Predmet[] = [];
  realizacijePredmetaId: number[] = [];
  studentPohadjanja: PohadjanjePredmeta[] = [];
  polozeniPredmeti: TablePredmet[] = [];
  nePolozeniPredmeti: TablePredmet[] = [];
  brPolaganjaRealizacija: BrPolaganjaRealizacija = [];
  ukupnoBodovaRp: { [key: number]: number } = {};
  rowData: any[] = [];

  async getPredmetiForStudent(studentId: number): Promise<Predmet[]> {
    let studentNaGodinamaId: number[] = [];
    let realizacijePredmetaId: number[] = [];
    let predmetiId: number[] = [];
    let predmeti: Predmet[] = [];
    
    const student = await firstValueFrom(this.studMapService.mapStudentById(studentId));
    student.studentNaGodinama.forEach((studentNaGodinama) => {
      studentNaGodinamaId.push(studentNaGodinama.id);
    });

    for (const s of studentNaGodinamaId) {
      const sg = await firstValueFrom(this.studGodService.getById(s, "studentinagodini"));
      sg.pohadjanjePredmeta.forEach((element) => {
        if(element.realizacijaPredmetaId !=null){
        realizacijePredmetaId.push(element.realizacijaPredmetaId);}
      });
    }

    for (const element of realizacijePredmetaId) {
      const e = await firstValueFrom(this.relService.getById(element, "realizacijepredmeta"));
      predmetiId.push(e.predmet.id);
    }
    for (const pId of predmetiId) {
      const p = await firstValueFrom(this.predmetService.getById(pId, "predmeti"));
      predmeti.push(p);
    }
    return predmeti;
  }

  // async getPolaganjaIdForStudent(studentId: number): Promise<number[]> {
  //   let studentNaGodinamaId: number[] = [];
  //   let polaganjaId: number[] = [];
  //   const student = await firstValueFrom(this.studMapService.mapStudentById(studentId));
  //   student.studentNaGodinama.forEach((studentNaGodinama) => {
  //     studentNaGodinamaId.push(studentNaGodinama.id);
  //   });
  //   for (const s of studentNaGodinamaId) {
  //     const sg = await firstValueFrom(this.studGodService.getById(s, "studentinagodini"));
  //     sg.polaganja.forEach((element) => {
  //       polaganjaId.push(element.id);
  //     });
  //   }
  //   return polaganjaId;
  // }

}
