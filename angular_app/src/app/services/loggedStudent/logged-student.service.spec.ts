import { TestBed } from '@angular/core/testing';

import { LoggedStudentService } from './logged-student.service';

describe('LoggedStudentService', () => {
  let service: LoggedStudentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoggedStudentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
