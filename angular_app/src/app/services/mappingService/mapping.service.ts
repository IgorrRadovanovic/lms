import { Injectable, provideZoneChangeDetection } from "@angular/core";
import { GenericService } from "../generic/generic.service";
import { HttpClient } from "@angular/common/http";
import { Univerzitet } from "../../model/univerzitet";
import { switchMap, forkJoin, map, Observable, of } from "rxjs";
import { Adresa } from "../../model/adresa";
import { Nastavnik } from "../../model/nastavnik";
import { Fakultet } from "../../model/fakultet";
import { StudijskiProgram } from "../../model/studijskiProgram";
import { GodinaStudija } from "../../model/godinaStudija";
import { Predmet } from "../../model/predmet";
import { Ishod } from "../../model/ishod";
import { Student } from "../../model/student";
import { StudentNaGodini } from "../../model/studentNaGodini";

@Injectable({
  providedIn: "root",
})
export class MappingService {
  constructor(
    private nastavniciService: GenericService<Nastavnik>,
    private studService: GenericService<Student>,
    private studGodService: GenericService<StudentNaGodini>,
    private uniService: GenericService<Univerzitet>,
    private adresaService: GenericService<Adresa>,
    private facService: GenericService<Fakultet>,
    private studProService: GenericService<StudijskiProgram>,
    private godStudService: GenericService<GodinaStudija>,
    private predmetService: GenericService<Predmet>,
    private ishodService: GenericService<Ishod>
  ) {}

  mapUni(): Observable<Univerzitet[]> {
    return this.uniService.getAll("univerziteti").pipe(
      switchMap((univerziteti: Univerzitet[]) =>
        forkJoin(
          univerziteti.map((element) =>
            forkJoin({
              adresa: this.adresaService.getById(element.adresaId, "adrese"),
              rektor: this.nastavniciService.getById(element.rektorId, "nastavnici"),
            }).pipe(
              map(({ adresa, rektor }) => ({
                id: element.id,
                naziv: element.naziv,
                datumOsnivanja: element.datumOsnivanja,
                fakultetId: element.fakultetId,
                fakulteti: element.fakulteti,
                adresaId: element.adresaId,
                adresa: adresa,
                rektorId: element.rektorId,
                rektor: rektor,
                brojTelefona: element.brojTelefona,
                email: element.email,
              }))
            )
          )
        )
      )
    );
  }

  mapFacById(id: number): Observable<Fakultet> {
    return this.facService.getById(id, "fakulteti").pipe(
      switchMap((element: Fakultet) =>
        forkJoin({
          adresa: this.adresaService.getById(element.adresaId, "adrese"),
          dekan: this.nastavniciService.getById(element.dekanId, "nastavnici"),
        }).pipe(
          map(({ adresa, dekan }) => ({
            id: element.id,
            naziv: element.naziv,
            univerzitet: element.univerzitet,
            univerzitetId: element.univerzitet.id,
            adresa: adresa,
            adresaId: element.adresaId,
            dekan: dekan,
            dekanId: element.dekanId,
            studijskiProgram: element.studijskiProgram,
            brojTelefona: element.brojTelefona,
            email: element.email,
          }))
        )
      )
    );
  }
  mapFac(): Observable<Fakultet[]> {
    return this.facService.getAll("fakulteti").pipe(
      switchMap((fakulteti: Fakultet[]) =>
        forkJoin(
          fakulteti.map((element) =>
            forkJoin({
              adresa: this.adresaService.getById(element.adresaId, "adrese"),
              dekan: this.nastavniciService.getById(element.dekanId, "nastavnici"),
            }).pipe(
              map(({ adresa, dekan }) => ({
                id: element.id,
                naziv: element.naziv,
                univerzitet: element.univerzitet,
                univerzitetId: element.univerzitet.id,
                adresa: adresa,
                adresaId: element.adresaId,
                dekan: dekan,
                dekanId: element.dekanId,
                studijskiProgram: element.studijskiProgram,
                brojTelefona: element.brojTelefona,
                email: element.email,
              }))
            )
          )
        )
      )
    );
  }
  mapStudPro(): Observable<StudijskiProgram[]> {
    return this.studProService.getAll("studijskiprogrami").pipe(
      switchMap((studijskiProgrami: StudijskiProgram[]) =>
        forkJoin(
          studijskiProgrami.map((element) =>
            forkJoin({
              godStud: this.godStudService.getById(element.godinaStudija.id, "godinestudija"),
              rukovodilac: this.nastavniciService.getById(element.rukovodilacId, "nastavnici"),
            }).pipe(
              map(({ godStud, rukovodilac }) => ({
                id: element.id,
                naziv: element.naziv,
                opis: element.opis,
                rukovodilac: rukovodilac,
                rukovodilacId: rukovodilac.id,
                godinaStudija: godStud,
                godinaStudijaId: element.godinaStudijaId,
                fakultet: element.fakultet,
                fakultetId: element.fakultetId,
                predmeti: element.predmeti,
              }))
            )
          )
        )
      )
    );
  }
  mapPredmet(): Observable<Predmet[]> {
    return this.predmetService.getAll("predmeti").pipe(
      switchMap((predmet: Predmet[]) =>
        forkJoin(
          predmet.map((element) =>
            of({
              id: element.id,
              naziv: element.naziv,
              espb: element.espb,
              obavezan: element.obavezan,
              brojPredavanja: element.brojPredavanja,
              drugiObliciNastave: element.drugiObliciNastave,
              istrazivackiRad: element.istrazivackiRad,
              ostaliCasovi: element.ostaliCasovi,
              silabus: element.silabus,
              silabusId: element.silabus.id,
              realizacijePredmeta: element.realizacijePredmeta,
              godinaStudija: element.godinaStudija,
              godinaStudijaId: element.godinaStudija.id,
              preduslov: element.preduslov,
              preduslovId: element.preduslov.id,
              studijskiProgram: element.studijskiProgram,
              studijskiProgramId: element.studijskiProgram.id,
            })
          )
        )
      )
    );
  }
  mapIshod(): Observable<Ishod[]> {
    return this.ishodService.getAll("ishodi").pipe(
      switchMap((ishodi: Ishod[]) => {
        return forkJoin(
          ishodi.map((element) =>
            of({
              id: element.id,
              naziv: element.naziv,
              silabus: element.silabus,
              silabusId: element.silabus.id,
              obrazovniCilj: element.obrazovniCilj,
              obrazovniCiljId: element.obrazovniCilj.id,
              terminiNastave: element.terminiNastave,
            })
          )
        );
      })
    );
  }
  mapUniById(id: number): Observable<Univerzitet> {
    return this.uniService.getById(id, "univerziteti").pipe(
      switchMap((element: Univerzitet) =>
        forkJoin({
          adresa: this.adresaService.getById(element.adresaId, "adrese"),
          rektor: this.nastavniciService.getById(element.rektorId, "nastavnici"),
        }).pipe(
          map(({ adresa, rektor }) => ({
            id: element.id,
            naziv: element.naziv,
            datumOsnivanja: element.datumOsnivanja,
            fakultetId: element.fakultetId,
            fakulteti: element.fakulteti,
            adresaId: element.adresaId,
            adresa: adresa,
            rektorId: element.rektorId,
            rektor: rektor,
            brojTelefona: element.brojTelefona,
            email: element.email,
          }))
        )
      )
    );
  }

  mapStudent(): Observable<Student[]> {
    return this.studService.getAll("studenti").pipe(
      switchMap((studenti: Student[]) => 
        forkJoin(
          studenti.map((element) =>
            forkJoin({
              adresa: this.adresaService.getById(element.adresaId, "adrese"),
            }).pipe(
              map(({ adresa}) => ({
                id: element.id,
                jmbg: element.jmbg,
                ime: element.ime,
                adresaId: element.adresaId,
                adresa: adresa,
                brojTelefona: element.brojTelefona,
                email: element.email,
                studentNaGodinama:element.studentNaGodinama
              }))
            )
          )
        )
      )
    );
  }

  mapStudentById(id: number): Observable<Student> {
    return this.studService.getById(id, "studenti").pipe(
      switchMap((element: Student) => 
            forkJoin({
              adresa: this.adresaService.getById(element.adresaId, "adrese"),
            }).pipe(
              map(({ adresa}) => ({
                id: element.id,
                jmbg: element.jmbg,
                ime: element.ime,
                adresaId: element.adresaId,
                adresa: adresa,
                brojTelefona: element.brojTelefona,
                email: element.email,
                studentNaGodinama:element.studentNaGodinama,
              }))
            )
          )
        );
      }
    

}