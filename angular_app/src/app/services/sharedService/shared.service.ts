import { Injectable } from "@angular/core";
import { Fakultet } from "../../model/fakultet";
import { Univerzitet } from "../../model/univerzitet";
import { GenericService } from "../generic/generic.service";
import { Student } from "../../model/student";
import { MappingService } from "../mappingService/mapping.service";
import { StudentNaGodini } from "../../model/studentNaGodini";
import { RealizacijaPredmeta } from "../../model/realizacijaPredmeta";
import { Polaganje } from "../../model/polaganje";
import { firstValueFrom } from "rxjs";
import { Predmet } from "../../model/predmet";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  activeFac?: Fakultet;
  activeUni?: Univerzitet;
  constructor(
  ) {}

   parseJwt(token: string): any {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
}

  isIspitniRokActive(predmetId:number): boolean{ // Stub - Studentska sluzba  
    return true;
  }

  setActiveFac(fakultet: Fakultet) {
    this.activeFac = fakultet;
  }
  setActiveUni(univerzitet: Univerzitet) {
    this.activeUni = univerzitet;
  }
  getActiveFac(): Fakultet | null {
    if (this.activeFac) {
      return this.activeFac;
    }
    return null;
  }
  getActiveUni(): Univerzitet | null {
    if (this.activeUni) {
      return this.activeUni;
    }
    return null;
  }
}
