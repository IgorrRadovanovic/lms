import { Injectable } from "@angular/core";
import { Fakultet } from "../model/fakultet";
import { Univerzitet } from "../model/univerzitet";
import { GenericService } from "./generic/generic.service";
import { Student } from "../model/student";
import { MappingService } from "./mappingService/mapping.service";
import { StudentNaGodini } from "../model/studentNaGodini";
import { RealizacijaPredmeta } from "../model/realizacijaPredmeta";
import { Polaganje } from "../model/polaganje";
import { firstValueFrom } from "rxjs";
import { Nastavnik } from "../model/nastavnik";
import { forEach } from "lodash";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  activeFac?: Fakultet;
  activeUni?: Univerzitet;
  constructor(
    private studMapService: MappingService,
    private polaganjaService: GenericService<Polaganje>,
    private relService: GenericService<RealizacijaPredmeta>,
    private studGodService: GenericService<StudentNaGodini>,
    private nastService: GenericService<Nastavnik>
  ) { }

  async getPredmetiIdForStudent(studentId: number): Promise<number[]> {
    let studentNaGodinamaId: number[] = [];
    let realizacijePredmetaId: number[] = [];
    let predmetiId: number[] = [];
    const student = await firstValueFrom(this.studMapService.mapStudentById(studentId));

    student.studentNaGodinama.forEach((studentNaGodinama) => {
      studentNaGodinamaId.push(studentNaGodinama.id);
    });

    for (const s of studentNaGodinamaId) {
      const sg = await firstValueFrom(this.studGodService.getById(s, "studentinagodini"));
      sg.pohadjanjePredmeta.forEach((element) => {
        realizacijePredmetaId.push(element.realizacijaPredmetaId);
      });
    }

    for (const element of realizacijePredmetaId) {
      const e = await firstValueFrom(this.relService.getById(element, "realizacijepredmeta"));
      predmetiId.push(e.predmet.id);
    }
    return predmetiId;
  }

  async getPremetiIdForNastavnik(nastavnikId: number) {
    let predmetiId: number[] = [];

    const nastavnik = await firstValueFrom(this.nastService.getById(nastavnikId, "nastavnici"));

    for (const rel of nastavnik.realizacije) {
      const realizacija = await firstValueFrom(this.relService.getById(rel.realizacijaPredmetaId, "realizacijepredmeta"));
      console.log(realizacija)
      predmetiId.push(realizacija.predmet.id);
      
    }
    return predmetiId;
  }

  async getPolaganjaIdForStudent(studentId: number): Promise<number[]> {
    let studentNaGodinamaId: number[] = [];
    let polaganjaId: number[] = [];
    const student = await firstValueFrom(this.studMapService.mapStudentById(studentId));
    student.studentNaGodinama.forEach((studentNaGodinama) => {
      studentNaGodinamaId.push(studentNaGodinama.id);
    });
    for (const s of studentNaGodinamaId) {
      const sg = await firstValueFrom(this.studGodService.getById(s, "studentinagodini"));
      sg.polaganja.forEach((element) => {
        polaganjaId.push(element.id);
      });
    }
    // console.log(polaganjaId);
    return polaganjaId;
  }


  parseJwt(token: string): any {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

  setActiveFac(fakultet: Fakultet) {
    this.activeFac = fakultet;
  }
  setActiveUni(univerzitet: Univerzitet) {
    this.activeUni = univerzitet;
  }
  getActiveFac(): Fakultet | null {
    if (this.activeFac) {
      return this.activeFac;
    }
    return null;
  }
  getActiveUni(): Univerzitet | null {
    if (this.activeUni) {
      return this.activeUni;
    }
    return null;
  }
}
