# Keycloak container image

U ovom direktorijumu se nalazi container image za keycloak, sa inicijalnom konfiguracijom

## Link za preuzimanje fajla:
 - https://drive.google.com/file/d/1C5c3zck0ON_-kdoQ2J3PEpzWfEBNZmtF/view?usp=drive_link

## Pokretanje:
Da bi se dati container napravio, potrebno je izvrsiti sledecu komande:

 1. $ podman import KC-container.tar kc-container-image 
 2. $ podman create --name kc-container --entrypoint "/opt/keycloak/bin/kc.sh" -e KEYCLOAK_ADMIN=4DM1N -e KEYCLOAK_ADMIN_PASSWORD=a1k5h2f4d8 --expose 8080 --expose 8443 --publish 8080 --publish 8443 docker.io/library/kc-container-image:latest start-dev
 3. $ podman start kc-container
 4. $ podman ps

Komanda broj 1 se koristi za importovanje image arhive.
Komanda broj 2 kreira container  i pokrece ga u development rezimu
Komanda broj 3 pokrece container
Komanda broj 4 se koristi da bi se izlistali podaci o pokrenutim container-ima, koristi se da bi se pronasla mapiranja portova (da bi se pronaslo preko kojih portova se komunicira sa Keycloak aplikacijom).

Nakon sto je container pokrenut, treba uci u konfiguraciju od gateway-a(application.yml) i izmeniti issuer-uri za my-keycloak-provider(Deo koji treba izmeniti je naznacen sa "==ToBeReplaced==", treba ga zameniti sa adresom i portom container-a)
