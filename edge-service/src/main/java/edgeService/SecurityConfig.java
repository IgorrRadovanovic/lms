package edgeService;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.web.server.SecurityWebFilterChain;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {
	
	
	public static final String ADMIN = "admin";
    public static final String STUDENT = "student";
    public static final String TEACHER = "teacher";
    private final JwtConverter jwtConverter = new JwtConverter(new JwtConverterProperties());
//	@Bean
//	public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
//		http
//			.authorizeExchange((exchanges)->
//				exchanges
//				.pathMatchers(HttpMethod.GET, "/nastavnici-service/api/nastavnici/**")
//				.permitAll())
////			.and()
//			.authorizeExchange((exchanges)->
//				exchanges
//				.pathMatchers(HttpMethod.GET, "/adrese-service/api/adrese/**")
//				.permitAll())
////			.and()
//			.authorizeExchange((exchanges)->
//				exchanges
//				.pathMatchers(HttpMethod.GET, "/fakulteti-service/api/silabusi/**")
//				.permitAll())
////			.and()
////			.authorizeExchange((exchanges)->
////				exchanges
////				.pathMatchers(HttpMethod.GET, "/fakulteti-service/api/nastavni_materijali/**") //Nastavni materijali kao entiteti nisu definisani
////				.permitAll())
////			.and()
//			.authorizeExchange((exchanges)->
//				exchanges
//				.anyExchange()
//				.authenticated()
//				)
//			.oauth2Login(Customizer.withDefaults());
//			
//			
//
//		return http.build();
//		
//	}
//	
//	@Bean
//	public GrantedAuthoritiesMapper userAuthoritiesMapperForKeycloak() {
//	        return authorities -> {
//	            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();
//	            var authority = authorities.iterator().next();
//	            boolean isOidc = authority instanceof OidcUserAuthority;
//
//	            if (isOidc) {
//	                var oidcUserAuthority = (OidcUserAuthority) authority;
//	                var userInfo = oidcUserAuthority.getUserInfo();
//
//	                if (userInfo.hasClaim("realm_access")) {
//	                    var realmAccess = userInfo.getClaimAsMap("realm_access");
//	                    var roles = (Collection<String>) realmAccess.get("roles");
//	                    mappedAuthorities.addAll(generateAuthoritiesFromClaim(roles));
//	                }
//	            } else {
//	                var oauth2UserAuthority = (OAuth2UserAuthority) authority;
//	                Map<String, Object> userAttributes = oauth2UserAuthority.getAttributes();
//
//	                if (userAttributes.containsKey("realm_access")) {
//	                    var realmAccess =  (Map<String,Object>) userAttributes.get("realm_access");
//	                    var roles =  (Collection<String>) realmAccess.get("roles");
//	                    mappedAuthorities.addAll(generateAuthoritiesFromClaim(roles));
//	                }
//	            }
//
//	            return mappedAuthorities;
//	        };
//	    }
//
//	Collection<GrantedAuthority> generateAuthoritiesFromClaim(Collection<String> roles) {
//	        return roles.stream()
//	                .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
//	                .collect(Collectors.toList());
//	}
	@Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.GET,"/nastavnici-service/api/nastavnici/**").hasAnyRole(TEACHER,ADMIN))
        .authorizeExchange(exchanges -> exchanges.pathMatchers(HttpMethod.GET).permitAll())
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.OPTIONS).permitAll())
        
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/nastavnici-service/api/**").hasAnyRole(TEACHER,ADMIN))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/nastavnici-service/api/**").hasAnyRole(TEACHER,ADMIN))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/nastavnici-service/api/**").hasAnyRole(TEACHER,ADMIN))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/nastavnici-service/api/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/adrese-service/api/**").hasAnyRole(ADMIN,TEACHER,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/adrese-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/adrese-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/adrese-service/api/**").hasAnyRole(ADMIN))
        
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/univerziteti-service/api/ishodi/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/univerziteti-service/api/ishodi/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/univerziteti-service/api/ishodi/**").hasAnyRole(ADMIN,TEACHER))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/univerziteti-service/api/ishodi/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN,TEACHER))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/univerziteti-service/api/obavestenja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/univerziteti-service/api/obavestenja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/univerziteti-service/api/obavestenja/**").hasAnyRole(ADMIN,TEACHER))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/univerziteti-service/api/obrazovniciljevi/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/univerziteti-service/api/obrazovniciljevi/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/univerziteti-service/api/obrazovniciljevi/**").hasAnyRole(ADMIN,TEACHER))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/univerziteti-service/api/fajlovi/**").hasAnyRole(ADMIN,TEACHER,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/univerziteti-service/api/fajlovi/**").hasAnyRole(ADMIN,TEACHER,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/univerziteti-service/api/fajlovi/**").hasAnyRole(ADMIN,TEACHER,STUDENT))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/univerziteti-service/api/termininastave/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/univerziteti-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/univerziteti-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/univerziteti-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/univerziteti-service/api/**").hasAnyRole(ADMIN))
        
        //nastanvici-service
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/nastavnici-service/api/nastavnici/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/nastavnici-service/api/nastavnici/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/nastavnici-service/api/nastavnici/**").hasAnyRole(ADMIN,TEACHER))
//        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/nastavnici-service/api/nastavnici/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/nastavnici-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/nastavnici-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/nastavnici-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/nastavnici-service/api/**").hasAnyRole(ADMIN))
        
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/studenti-service/api/studenti/**").hasAnyRole(ADMIN,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/studenti-service/api/studenti/**").hasAnyRole(ADMIN,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/studenti-service/api/studenti/**").hasAnyRole(ADMIN,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/studenti-service/api/studenti/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/studenti-service/api/evaluacijeznanja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/studenti-service/api/evaluacijeznanja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/studenti-service/api/evaluacijeznanja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/studenti-service/api/evaluacijeznanja/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/studenti-service/api/instrumentievaluacije/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/studenti-service/api/instrumentievaluacije/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/studenti-service/api/instrumentievaluacije/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/studenti-service/api/instrumentievaluacije/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/studenti-service/api/tipovievaluacije/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/studenti-service/api/tipovievaluacije/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/studenti-service/api/tipovievaluacije/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/studenti-service/api/tipovievaluacije/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/studenti-service/api/polaganja/**").hasAnyRole(ADMIN,TEACHER,STUDENT))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/studenti-service/api/polaganja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/studenti-service/api/polaganja/**").hasAnyRole(ADMIN,TEACHER))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/studenti-service/api/polaganja/**").hasAnyRole(ADMIN))
        
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.POST, "/studenti-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PUT, "/studenti-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.PATCH, "/studenti-service/api/**").hasAnyRole(ADMIN))
        .authorizeExchange(exchanges->exchanges.pathMatchers(HttpMethod.DELETE, "/studenti-service/api/**").hasAnyRole(ADMIN))
        
        
        
        
        .authorizeExchange(exchanges->exchanges.anyExchange().authenticated())
        .oauth2ResourceServer((oauth2)-> oauth2.jwt(jwt -> jwt.jwtAuthenticationConverter(jwtConverter)))
        .csrf((csrf)->csrf.disable());
        return http.build();
    }

}
