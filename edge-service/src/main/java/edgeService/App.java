package edgeService;

import java.util.ArrayList;
import java.util.Arrays;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
@EnableDiscoveryClient
public class App {
	public static void main(String[] args) {
			new SpringApplicationBuilder(App.class)
        .web(WebApplicationType.REACTIVE)
        .run(args);
	}
	
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:4200");
//			}
//		};
//	}
	

	@Bean 
	public GlobalFilter fixCors() {
		return new CorsFix();
	} 
	
//	@Bean
//    public CorsFilter corsWebFilter() {
//
//    final CorsConfiguration corsConfig = new CorsConfiguration();
//    ArrayList<String> origins = new ArrayList<>();
//    origins.add("http://localhost:4200");
//    origins.add("http://localhost:8081");
//    corsConfig.setAllowedOrigins(origins);
//    corsConfig.setMaxAge(3600L);
//    corsConfig.setAllowedMethods(Arrays.asList("GET", "POST"));
//    corsConfig.addAllowedHeader("*");
//    
//
//    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//    source.registerCorsConfiguration("/**", corsConfig);
//
//    return new CorsFilter(source);
//	}
	
	

}