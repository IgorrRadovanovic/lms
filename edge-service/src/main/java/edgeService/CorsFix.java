package edgeService;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.env.Environment;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;
@Component
public class CorsFix implements GlobalFilter {


	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		System.out.println("-----");
		System.out.println("Started Cors Filter");
		System.out.println("-----");
		return chain.filter(exchange)
	              .then(Mono.fromRunnable(() ->{
	            	  
	            	  ServerHttpResponse response = exchange.getResponse();
//	            	  Object x = response.getHeaders().get("access-control-allow-origin");
	            	  System.out.println(response.toString());
	            	  System.out.println("++++++++++++++++++++++++++++++++++++++++");
	            	  if(response.getHeaders().get("access-control-allow-origin")!= null) {
	            		  System.out.println("!!!!!");
	            		  File myObj = new File("src/main/frontend-adresa.txt");
	            		  Scanner myReader = null;
	            		  String adresa = "http://localhost:4200";
	            	      try {
							myReader = new Scanner(myObj);
							adresa = myReader.nextLine();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
//							e.printStackTrace();
							System.out.println("No File");
						}
	            		  System.out.println(response.getHeaders().get("access-control-allow-origin"));
	            		  System.out.println("!!!!!");
	            		  response.getHeaders().remove("access-control-allow-origin");
	            		  response.getHeaders().add("access-control-allow-origin", adresa);
	            		  System.out.println(response.toString());
	            	  }
	              }));
	}

}
