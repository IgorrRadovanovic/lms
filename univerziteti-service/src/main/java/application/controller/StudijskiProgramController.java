package application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import application.model.StudijskiProgram;
import application.model.dto.StudijskiProgramDTO;
import application.service.GenericService;

@Controller
@RequestMapping("/api/studijskiprogrami")
public class StudijskiProgramController
		extends GenericController<StudijskiProgram, StudijskiProgramDTO, GenericService<StudijskiProgram, Integer>> {
	public StudijskiProgramController(GenericService<StudijskiProgram, Integer> service) {
		super(service);
	}
	

}