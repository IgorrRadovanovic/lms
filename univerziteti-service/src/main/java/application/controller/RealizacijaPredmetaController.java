package application.controller;

import application.model.GenericConverter;
import application.model.RealizacijaPredmeta;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.RealizacijaPredmetaDTO;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import application.service.GenericService;

@Controller
@RequestMapping("/api/realizacijepredmeta")
public class RealizacijaPredmetaController extends
		GenericController<RealizacijaPredmeta, RealizacijaPredmetaDTO, GenericService<RealizacijaPredmeta, Integer>> {
	public RealizacijaPredmetaController(GenericService<RealizacijaPredmeta, Integer> service) {
		super(service);
	}

	@GetMapping("/bypredmet/{pId}")
	public ResponseEntity<Iterable<RealizacijaPredmetaDTO>> getByPredmetId(@PathVariable("pId") Integer pId) {
//		System.out.println("test");
		@SuppressWarnings("unchecked")
		Class<RealizacijaPredmetaDTO> dtoClass = (Class<RealizacijaPredmetaDTO>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[1];
		ArrayList<RealizacijaPredmetaDTO> dtos = new ArrayList<>();
		Iterable<RealizacijaPredmeta> realizacije = this.service.findAll();
		realizacije.forEach(r -> {
			if (r.getPredmet().getId()==pId) {
				GenericConverter<RealizacijaPredmeta, RealizacijaPredmetaDTO> genConv = new GenericConverter<>();
				RealizacijaPredmetaDTO dto = genConv.convertToDTO(r, dtoClass);
				dtos.add(dto);
			}
		});
        return new ResponseEntity<>(dtos, HttpStatus.OK);

	}
}