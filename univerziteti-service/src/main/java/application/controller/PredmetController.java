package application.controller;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import application.model.Predmet;
import application.model.dto.PredmetDTO;
import application.service.GenericService;
import application.model.GenericConverter;

@Controller
@RequestMapping("/api/predmeti")
public class PredmetController extends GenericController<Predmet, PredmetDTO, GenericService<Predmet, Integer>> {
	public PredmetController(GenericService<Predmet, Integer> service) {
		super(service);
	}

//	@GetMapping("/byevid/{evId}")
//	public ResponseEntity<Iterable<PredmetDTO>> getByPredmetId(@PathVariable("predmetId") Integer predmetId) {
//		@SuppressWarnings("unchecked")
//		Class<PredmetDTO> dtoClass = (Class<PredmetDTO>) ((ParameterizedType) getClass()
//				.getGenericSuperclass()).getActualTypeArguments()[1];
//		ArrayList<PredmetDTO> dtos = new ArrayList<>();
//		Iterable<Predmet> evaluacije = this.service.findAll();
//		evaluacije.forEach(e -> {
////			System.out.println(s.getStudent().getId() + " " + studId);
//			if (e.getEv().getId() == studGodId) {
//				GenericConverter<Predmet, PredmetDTO> genConv = new GenericConverter<>();
//				PredmetDTO dto = genConv.convertToDTO(p, dtoClass);
//				dtos.add(dto);
//			}
//		});
//		return new ResponseEntity<>(dtos, HttpStatus.OK);
//
//	}

}