package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Fajl; 
 public interface FajlRepository extends CrudRepository<Fajl, Integer> {

}