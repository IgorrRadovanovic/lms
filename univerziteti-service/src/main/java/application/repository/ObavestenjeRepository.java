package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Obavestenje; 
 public interface ObavestenjeRepository extends CrudRepository<Obavestenje, Integer> {

}