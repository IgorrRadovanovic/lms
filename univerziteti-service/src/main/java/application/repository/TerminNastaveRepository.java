package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.TerminNastave; 
 public interface TerminNastaveRepository extends CrudRepository<TerminNastave, Integer> {

}