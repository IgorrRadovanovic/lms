package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Univerzitet; 
 public interface UniverzitetRepository extends CrudRepository<Univerzitet, Integer> {

}