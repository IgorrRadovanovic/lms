package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.ObrazovniCilj; 
 public interface ObrazovniCiljRepository extends CrudRepository<ObrazovniCilj, Integer> {

}