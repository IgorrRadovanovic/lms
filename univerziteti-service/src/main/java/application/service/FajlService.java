package application.service; 
import org.springframework.stereotype.Service; 
import application.model.Fajl;
import application.repository.FajlRepository;  
@Service
public class FajlService extends GenericService<Fajl, Integer> {
public FajlService(FajlRepository repository){super(repository);
}
}