package application.service; 
import org.springframework.stereotype.Service; 
import application.model.Univerzitet;
import application.repository.UniverzitetRepository;  
@Service
public class UniverzitetService extends GenericService<Univerzitet, Integer> {
public UniverzitetService(UniverzitetRepository repository){super(repository);
}
}