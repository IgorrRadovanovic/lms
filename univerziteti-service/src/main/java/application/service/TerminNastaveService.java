package application.service; 
import org.springframework.stereotype.Service; 
import application.model.TerminNastave;
import application.repository.TerminNastaveRepository;  
@Service
public class TerminNastaveService extends GenericService<TerminNastave, Integer> {
public TerminNastaveService(TerminNastaveRepository repository){super(repository);
}
}