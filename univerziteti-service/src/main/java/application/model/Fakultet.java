package application.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Fakultet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(optional = false)
	private Univerzitet univerzitet;

	@Column
	private Integer adresaId;

	@Column
	private Integer dekanId;

	@OneToMany(mappedBy = "fakultet")
	private List<StudijskiProgram> studijski_programi = new ArrayList<StudijskiProgram>();

	@Column(nullable = false)
	private String brojTelefona;

	@Column(nullable = false)
	private String email;

	public Fakultet() {
		super();
	}

	@Column
	private String naziv;

	public Fakultet(Univerzitet univerzitet, String naziv, Integer adresaId, Integer dekanId,
			List<StudijskiProgram> studijski_programi, String brojTelefona, String email) {
		super();
		this.univerzitet = univerzitet;
		this.naziv = naziv;
		this.adresaId = adresaId;
		this.dekanId = dekanId;
		this.studijski_programi = studijski_programi;
		this.brojTelefona = brojTelefona;
		this.email = email;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public Integer getDekanId() {
		return dekanId;
	}

	public void setDekanId(Integer dekanId) {
		this.dekanId = dekanId;
	}

	public List<StudijskiProgram> getStudijski_programi() {
		return studijski_programi;
	}

	public void setStudijski_programi(List<StudijskiProgram> studijski_programi) {
		this.studijski_programi = studijski_programi;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
