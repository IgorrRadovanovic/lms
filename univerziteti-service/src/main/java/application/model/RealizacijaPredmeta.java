package application.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class RealizacijaPredmeta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne()
	private Predmet predmet;

	@OneToMany(mappedBy = "realizacijaPredmeta")
	private List<TerminNastave> termini = new ArrayList<TerminNastave>();

	public RealizacijaPredmeta() {
		super();
	}

	public RealizacijaPredmeta(Integer id, ArrayList<TerminNastave> termini) {
		super();
		this.id = id;
		this.termini = termini;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public List<TerminNastave> getTermini() {
		return termini;
	}

	public void setTermini(List<TerminNastave> termini) {
		this.termini = termini;
	}

	public Predmet getPredmet() {
		return predmet;
	}

	public void setPredmet(Predmet predmet) {
		this.predmet = predmet;
	}


}
