package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
//import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
@Entity
public class Univerzitet {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private Integer adresaId;
	
	@Column
	private String naziv;
	
	@Column
	private LocalDateTime datumOsnivanja;
	
	@OneToMany(mappedBy = "univerzitet")
	private List<Fakultet> fakulteti = new ArrayList<>();
	
	@Column
	private Integer rektorId;

	@Column(nullable=false)
	private String brojTelefona;
	
	@Column(nullable=false)
	private String email;
	
	@Column(columnDefinition="TEXT")
	private String opis;
	
	public Univerzitet(Integer id, Integer adresaId, String naziv, LocalDateTime datumOsnivanja,
			List<Fakultet> fakulteti, Integer rektorId, String brojTelefona, String email, String opis) {
		super();
		this.id = id;
		this.adresaId = adresaId;
		this.naziv = naziv;
		this.datumOsnivanja = datumOsnivanja;
		this.fakulteti = fakulteti;
		this.rektorId = rektorId;
		this.brojTelefona=brojTelefona;
		this.email=email;
		this.opis=opis;
	}
	
	public Univerzitet() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getDatumOsnivanja() {
		return datumOsnivanja;
	}

	public void setDatumOsnivanja(LocalDateTime datumOsnivanja) {
		this.datumOsnivanja = datumOsnivanja;
	}

	public List<Fakultet> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(List<Fakultet> fakulteti) {
		this.fakulteti = fakulteti;
	}

	public Integer getRektorId() {
		return rektorId;
	}

	public void setRektorId(Integer rektorId) {
		this.rektorId = rektorId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
	
	



}