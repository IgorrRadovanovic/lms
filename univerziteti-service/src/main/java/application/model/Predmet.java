package application.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Predmet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String naziv;

    @Column(nullable = false)
    private Integer espb;

    @Column(nullable = false)
    private Boolean obavezan;

    @Column(nullable = false)
    private Integer brojPredavanja;

    @Column(nullable = false)
    private Integer brojVezbi;

    @Column(nullable = false)
    private Integer drugiObliciNastave;

    @Column(nullable = false)
    private Integer istrazivackiRad;

    @Column(nullable = false)
    private Integer ostaliCasovi;

    @ManyToOne
    private Silabus silabus;

    @OneToMany(mappedBy = "predmet")
    private List<RealizacijaPredmeta> realizacijePredmeta= new ArrayList<RealizacijaPredmeta>();
    
    @ManyToOne
    private StudijskiProgram studijskiProgram;
    
    @ManyToOne
    private GodinaStudija godinaStudija;
    
    @ManyToOne
    private Predmet preduslov;

	public Predmet(Integer id, String naziv, Integer espb, Boolean obavezan, Integer brojPredavanja, Integer brojVezbi,
			Integer drugiObliciNastave, Integer istrazivackiRad, Integer ostaliCasovi, Silabus silabus,
			List<RealizacijaPredmeta> realizacijePredmeta, StudijskiProgram studijskiProgram,
			GodinaStudija godinaStudija, Predmet preduslov) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.espb = espb;
		this.obavezan = obavezan;
		this.brojPredavanja = brojPredavanja;
		this.brojVezbi = brojVezbi;
		this.drugiObliciNastave = drugiObliciNastave;
		this.istrazivackiRad = istrazivackiRad;
		this.ostaliCasovi = ostaliCasovi;
		this.silabus = silabus;
		this.realizacijePredmeta = realizacijePredmeta;
		this.studijskiProgram = studijskiProgram;
		this.godinaStudija = godinaStudija;
		this.preduslov = preduslov;
	}

	public Predmet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getEspb() {
		return espb;
	}

	public void setEspb(Integer espb) {
		this.espb = espb;
	}

	public Boolean getObavezan() {
		return obavezan;
	}

	public void setObavezan(Boolean obavezan) {
		this.obavezan = obavezan;
	}

	public Integer getBrojPredavanja() {
		return brojPredavanja;
	}

	public void setBrojPredavanja(Integer brojPredavanja) {
		this.brojPredavanja = brojPredavanja;
	}

	public Integer getBrojVezbi() {
		return brojVezbi;
	}

	public void setBrojVezbi(Integer brojVezbi) {
		this.brojVezbi = brojVezbi;
	}

	public Integer getDrugiObliciNastave() {
		return drugiObliciNastave;
	}

	public void setDrugiObliciNastave(Integer drugiObliciNastave) {
		this.drugiObliciNastave = drugiObliciNastave;
	}

	public Integer getIstrazivackiRad() {
		return istrazivackiRad;
	}

	public void setIstrazivackiRad(Integer istrazivackiRad) {
		this.istrazivackiRad = istrazivackiRad;
	}

	public Integer getOstaliCasovi() {
		return ostaliCasovi;
	}

	public void setOstaliCasovi(Integer ostaliCasovi) {
		this.ostaliCasovi = ostaliCasovi;
	}

	public Silabus getSilabus() {
		return silabus;
	}

	public void setSilabus(Silabus silabus) {
		this.silabus = silabus;
	}

	public List<RealizacijaPredmeta> getRealizacijePredmeta() {
		return realizacijePredmeta;
	}

	public void setRealizacijePredmeta(List<RealizacijaPredmeta> realizacijePredmeta) {
		this.realizacijePredmeta = realizacijePredmeta;
	}

	public StudijskiProgram getStudijskiProgram() {
		return studijskiProgram;
	}

	public void setStudijskiProgram(StudijskiProgram studijskiProgram) {
		this.studijskiProgram = studijskiProgram;
	}

	public GodinaStudija getGodinaStudija() {
		return godinaStudija;
	}

	public void setGodinaStudija(GodinaStudija godinaStudija) {
		this.godinaStudija = godinaStudija;
	}

	public Predmet getPreduslov() {
		return preduslov;
	}

	public void setPreduslov(Predmet preduslov) {
		this.preduslov = preduslov;
	}

	
	
}
