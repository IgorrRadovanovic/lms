package application.model;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
//import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class GodinaStudija {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(nullable=false)
	private Integer godina;

	@OneToMany(mappedBy="godinaStudija")
	private List<StudijskiProgram> studijskiProgrami = new ArrayList<>();
	
	@OneToMany(mappedBy="godinaStudija")
	private List<Predmet> predmeti=new ArrayList<Predmet>();
	
	public GodinaStudija() {
		super();
	}

	public GodinaStudija(Integer id, Integer godina, List<StudijskiProgram> studijskiProgram,
			/* List<StudentNaGodini> studentiNaGodini, */ List<Predmet> predmeti) {
		super();
		this.id = id;
		this.godina = godina;
		this.studijskiProgrami = studijskiProgram;
		//this.studentiNaGodini = studentiNaGodini;
		this.predmeti = predmeti;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGodina() {
		return godina;
	}

	public void setGodina(Integer godina) {
		this.godina = godina;
	}

	public List<Predmet> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(List<Predmet> predmeti) {
		this.predmeti = predmeti;
	}

	public List<StudijskiProgram> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(List<StudijskiProgram> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}	
	

}
