package application.model.dto;

import java.time.LocalDateTime;




public class TerminNastaveDTO {
	public TerminNastaveDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

    private Integer id;

	private LocalDateTime vremePocetka;

	private LocalDateTime vremeZavrsetka;
	
	private RealizacijaPredmetaDTO realizacijaPredmeta;
//	
//	private TipNastaveDTO tipNastave;
	private IshodDTO ishod;
	
	private Integer tipNastaveId;
	
	public TerminNastaveDTO(Integer id, LocalDateTime vremePocetka, LocalDateTime vremeZavrsetka,
			RealizacijaPredmetaDTO realizacijaPredmeta, IshodDTO ishod, /*TipNastaveDTO*/Integer tipNastave) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.realizacijaPredmeta = realizacijaPredmeta;
		this.ishod = ishod;
		this.tipNastaveId = tipNastave;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(LocalDateTime vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public LocalDateTime getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(LocalDateTime vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public RealizacijaPredmetaDTO getRealizacija() {
		return realizacijaPredmeta;
	}

	public void setRealizacija(RealizacijaPredmetaDTO realizacija) {
		this.realizacijaPredmeta = realizacija;
	}

	public RealizacijaPredmetaDTO getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmetaDTO realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public Integer getTipNastaveId() {
		return tipNastaveId;
	}

	public void setTipNastaveId(Integer tipNastaveId) {
		this.tipNastaveId = tipNastaveId;
	}

	public IshodDTO getIshod() {
		return ishod;
	}

	public void setIshod(IshodDTO ishod) {
		this.ishod = ishod;
	}

//	public TipNastaveDTO getTipNastave() {
//		return tipNastave;
//	}
//
//	public void setTipNastave(TipNastaveDTO tipNastave) {
//		this.tipNastave = tipNastave;
//	}
	
	
	
	
	


}
