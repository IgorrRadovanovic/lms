package application.model.dto;


import java.util.ArrayList;
import java.util.List;


public class SilabusDTO {
	
	public SilabusDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	private Integer id;

    private List<IshodDTO> ishodi = new ArrayList<IshodDTO>();
	public SilabusDTO(Integer id, List<IshodDTO> ishodi) {
		super();
		this.id = id;
		this.ishodi = ishodi;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<IshodDTO> getIshodi() {
		return ishodi;
	}
	public void setIshodi(List<IshodDTO> ishodi) {
		this.ishodi = ishodi;
	}
	
	
	

}
