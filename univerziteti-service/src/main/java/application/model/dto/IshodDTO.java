package application.model.dto;

import java.util.ArrayList;
import java.util.List;

public class IshodDTO {//Tema
	

	private Integer id;

	private String naziv;
	
	private SilabusDTO silabus;
	//Mozda treba obrnuti vezu izmedju Ishoda i Obrazovnog cilja
	
	private ObrazovniCiljDTO obrazovniCilj;
	
	private List<TerminNastaveDTO> terminiNastave = new ArrayList<>(); // obrnuti vezu
	public IshodDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public IshodDTO(Integer id, String naziv, SilabusDTO silabus, ObrazovniCiljDTO obrazovniCilj, List<TerminNastaveDTO> terminNastave) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.silabus = silabus;
		this.obrazovniCilj = obrazovniCilj;
		this.terminiNastave = terminNastave;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public SilabusDTO getSilabus() {
		return silabus;
	}

	public void setSilabus(SilabusDTO silabus) {
		this.silabus = silabus;
	}

	public ObrazovniCiljDTO getObrazovniCilj() {
		return obrazovniCilj;
	}

	public void setObrazovniCilj(ObrazovniCiljDTO obrazovniCilj) {
		this.obrazovniCilj = obrazovniCilj;
	}

	public List<TerminNastaveDTO> getTerminiNastave() {
		return terminiNastave;
	}

	public void setTerminNastave(List<TerminNastaveDTO> terminNastave) {
		this.terminiNastave = terminNastave;
	}

}
