package application.model.dto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class GodinaStudijaDTO {

	

	private Integer id;

	private Integer godina;


	private List<StudijskiProgramDTO> studijskiProgrami = new ArrayList<>();



	private List<PredmetDTO> predmeti=new ArrayList<PredmetDTO>();
	
	public GodinaStudijaDTO() {
		super();
	}

	public GodinaStudijaDTO(Integer id, Integer godina, List<StudijskiProgramDTO> studijskiProgram,
		 List<PredmetDTO> predmeti) {
		super();
		this.id = id;
		this.godina = godina;
		this.studijskiProgrami = studijskiProgram;
		this.predmeti = predmeti;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGodina() {
		return godina;
	}

	public void setGodina(Integer godina) {
		this.godina = godina;
	}


	public List<PredmetDTO> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(List<PredmetDTO> predmeti) {
		this.predmeti = predmeti;
	}

	public List<StudijskiProgramDTO> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(List<StudijskiProgramDTO> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}

	
	
	
	

}
