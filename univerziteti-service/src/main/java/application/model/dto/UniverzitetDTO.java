package application.model.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;

public class UniverzitetDTO {

	private Integer id;
	private Integer adresaId;
	private String naziv;
	private LocalDateTime datumOsnivanja;
	private List<FakultetDTO> fakulteti = new ArrayList<>();
	private Integer rektorId;
	private String brojTelefona;
	private String email;
	private String opis;
	
	public UniverzitetDTO(Integer id, Integer adresaId, String naziv, LocalDateTime datumOsnivanja,
			List<FakultetDTO> fakulteti, Integer rektorId, String brojTelefona, String email,String opis) {
		super();
		this.id = id;
		this.adresaId = adresaId;
		this.naziv = naziv;
		this.datumOsnivanja = datumOsnivanja;
		this.fakulteti = fakulteti;
		this.rektorId = rektorId;
		this.brojTelefona=brojTelefona;
		this.email=email;
		this.opis=opis;
	}
	
	public UniverzitetDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getDatumOsnivanja() {
		return datumOsnivanja;
	}

	public void setDatumOsnivanja(LocalDateTime datumOsnivanja) {
		this.datumOsnivanja = datumOsnivanja;
	}

	public List<FakultetDTO> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(List<FakultetDTO> fakulteti) {
		this.fakulteti = fakulteti;
	}

	public Integer getRektorId() {
		return rektorId;
	}

	public void setRektorId(Integer rektorId) {
		this.rektorId = rektorId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
	
	


}