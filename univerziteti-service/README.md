# Learning Management System

Ovo je repozitorijum za Learning Management System (LMS).
## Upozorenje:
**Na ovoj(main) grani se nalazi sav kod koji je vezan za LMS.**
**Dati kod nije podešen da funkcioniše.**

Za kod koji funkcioniše potrebno je preuzeti date grane:
   - cloud_server
   - edge_service
   - adrese_component
   - fakulteti_component
   - studenti_component
   - nastavnici_component

## Načini na koje se grane preuzimaju

### Komandnom linijom
Da bi ste pomoću komandne linije preuzeli granu koja vam je potrebna,
trebate pokrenuti komandu:
   - git clone -b {naziv-grane} {url-repozitorijuma}

Primer komande:
   - git clone -b adrese_service https://gitlab.com/IgorrRadovanovic/lms.git

### Grafičkim interfejsom
Da bi ste preuzeli kod kroz grafički interfejs gitlab platforme,
trebate se prebaciti na granu na kojoj se nalazi vama potreban kod,
(Izbor grane se vrši pomoću menija koji se nalazi iznad naziva direktorijuma/fajlova)
trebate otvoriti Code meni na desnoj strani interfejsa, 
i izabrati format komresije(nalazi se na dnu menija, primeri formata:zip,tar)
Izborom formata će te pokrenuti preuzimanje arhive u kojoj se nalazi kod sa grane repozitorijuma.

