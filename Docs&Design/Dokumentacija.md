# Dokumentacija

Ideja prikaza web aplikacije za neregistrovane korisnike jeste da se pri otvaranju stranice, korisnik može odlučiti da

1. Pregleda univerzitete

2. Pregleda forume

3. Da se prijavi na sistem.

Ukoliko odabare pregled univerziteta, generišu se onoliko kartica koliko ima univerziteta, sa njihovim slikama I nazivima iznad. Kada se korisnik odluči za univerzitet, otvara se Unihome, koji u zavisnosti od odabira univerziteta, izvlači logo I pozadinu specifičnu za taj uni, i postavlja se njemu specifična css tema. Tada korisnik može da pregleda fakultete, Ili da poseti stranicu “O nama”. Ukoliko se odluči da pregleda fakultete, Unihome generiše listu fakulteta specifičnih za taj univerzitet. Kada se odabere fakultet ruter nas šalje na FacHome componentu, u njoj korisnik može da pregledama studijske programe, ili da poseti stranicu “O nama”. Ukoliko se odluči da pregleda studijske programe, otvara se meni sa studijskim programima, kojima može da pristupi kako bi saznao više informacija. Kada odabere neki studijski program, otvara se tabela sa predmetima na tom studijskom programu njegov opis kao I opis rukovodioca smera. Duplim klikom na predmet se tabla menja u tabelu silabusa odabranog predmeta. Odnosno spisak njegovih ishoda(tema).

## Menu
Sve menu komponente se koriste sličnim šablonom, tri kartice, srednja je logo, osim u slučaju odabira fakulteta i univerziteta tada sve tri kartice predstavljaju date fakultet ili univerzitete. Svi meniji imaju tri reda, prvi je za naslov stranice, drugi je za meni, treći je backArrow dugme. Sadržaj sva tri reda se menjaju dinamički. Naprimer U zavisnosti od odabira korisnika menja se aktivni meni a u zavisnosti od aktivnog menija, menja se naslov i funkcija backArrow dugmeta.

## O Nama
Unihome i FacHome imaju dodatan red, koji predstavlja stranu O nama koja se sastoji od liste onama objekata. ONama je objekat koji ima svoj header i content. U istom fajlu su i instancirani svi potrebni OnamaCards koji opisuju O Nama stranu svih fakulteta i univerziteta. U komponenti, pri otvaranju onama stranice i postavljalju odgovarajući OnamaCards. Tu su instancirani i OnamaCards za Engleski i Srpski jezik, pomoću ngx translate-a manipulišemo prevodima. Svaki put kada se pozove setOnama(), proverava se .currentLang(), i ukoliko je undefined(a biće undefined ukoliko je osvežena stranica ili nije biran jezik ni jednom, iako je postavljen defaultLang, currrentLang ga ignoriše), postavlja se ’sr’ kao currentLang. Pošto ne možemo da se koristimo uobičajenim ngx translate mehanizmima zbog prirode onama komponente, u konstruktoru se pretplaćujemo na onLangChange event i pozivamo setOnama koji po jeziku i objektu koji se opisuje zaključuje koji oNamaCards da povuče.

## Generic Service
Imamo četiri liste stringova, za svaki microservis. U toj listi stringova se nalaze nazivi endpointa. Kada se negde u aplikaciji pozove funkcija generičkog servisa kao na primer getAll, iz prosleđenog endpointa se poziva getService() I on zakljucuje koji nam mikroservis treba, I vraca nam njegov ceo URL.

## Mapping Servis
Poziva generic service metode getAll ili getById, I mapira to na odgovarajuće objekte i vraća, Observable ili liste Observable tih objekata.
 
## SharedService  
U njemu skladištimo korisnički odabrane fakultete i univerzitete, kako ne bi bilo potrebno svaki put kad se otvara stranica, da se povlače svi fakultetiuniverziteti iz baze i filtriraju nama potrebni, već samo povučemo iz shared service zabeležen fakultet ili univerzitet. U URL-u se i dalje šalje ID univerzitetafakulteta kao backup, jer ukoliko korisnik refreshuje aplikaciju, sharedService gubi podatke o selektovanom univerzitetufakultetu i tada aplikacija ipak ponovo učitava podatke.

## Generic-Table

Od argumenata generic table zahteva

1. table – ovde se unosi endpoint objekata koji će biti prikazani.
2. colLimit –  ovde se unose columne objekta koje bi smo želeli da prikažemo.
3. dataLimit – ovde se unosi ono po čemu će se identifikovati objekat, na primer po studijskom programu. Primer  
               [dataLimit]={ studijskiProgramId selectedStudPro.id }
4. table2ColLimits – colLimits za tabelu o drugom objektu.
5. table2 – endpoint objekata druge tabele.
6. dataLimit2 – ovde se samo nalazi string naziv onoga po čemu ćemo identifikovati objekat u drugoj tabeli. Primer
                [dataLimit2]='silabus'

7. menuChoice – ovo je signal koji govori da li smo u prvoj ili drugoj tabeli zbog prikaza naslova i drugih stvari.

## (studPro) Generic-Table

_getData()_  
pomoću mapping servisa, učitava objekte, i filtrira ih prema tome da li atribut objekta nazvan dataLimitKey, sadrži dataLimitValue.  
_(DataLimit se deli na key i value [dataLimit]={ studijskiProgramId selectedStudPro.id })  
_Nakon toga mapiramo prevode koji su nam potrebni u objekat. Na kraju vraćamo isfiltrirane i mapirane objekte AG Grid-u.

_getColumns()_  
Nazive svih kolumna izvlači pomoću getColumns metode koju imamo u generičkom servisu koja između ostalog, izostavlja ID kao kolumnu. Kada izvuče sve kolumne filtrira ih po colLimit i onda ih mapira na field (sam naziv columne) , valueFormatter (formater koji je potreban za formatiranje podataka iz objekta koji su vrednost kolumne), headerName Naziv columne koji dobijamo iz ngx translate po potrebi jezika, minWidth Minimalne širine polja po njihovim imenima(npr. Nije nam potrebna ista širina za ESPB i za Naziv predmeta.  
Da bi smo dobili potreban formater neophodno je da ga prvo i napravimo Naziv formattera definišemo na sledeći način `colName`+”Formater”, primer Column Name = godinaStudija, Formater= godinaStudijaFormatter(). Formater funkcioniše tako što mu se prosleđuje ceo objekat koji je potrebno formatirati, a vraća se taj deo objekta koji želimo da prikažemo u tabeli.  
U samom postavljanju potrebnog formatera u valueFormatter, kastujemo (this as any) na [formatterName] koji je da se podsetimo `colName`+”Formater”, da bi smo izbegli upozorenje TypeScripta na sigurnost tipova. Onda koristimo kondicionalni operator koji proverava da li formatterName postoji, ako postoji bindujemo ga na sadašnji kontekst, što znači da kada je pozvan imaće odgovarajući this kontekst. Ako ne postoji vraća undefined. Formateri su samo funkcije operišu nad objektima koje dobije getData, kako u tabeli ne bi bio prikazan Object object, već štagod mi uradimo sa tim objektom.

Zatim postavljamo i event listenere onCellClicked za pametne telefone i onCellDoubleClicked za lične računare koji će na date akcije učitati ugnježdenu tabelu.  
Za početak uklanjaju postojeće Formatere, iz objekta nad kojim se dogodio događaj uzima se datalimit2Value _(event.data[this.dataLimit2].id;)_, i njegov ključ this.dataLimit2(_Argument prosledjen pri instanciranju tabele u roditeljskoj komponenti_.). Emitujemo signal koji je uhvaćen u roditeljskoj komponenti kako bi se generisali odgovarajući naslovi i podnaslovi. Postavljamo I this.silabus na true zbog translate.OnLangChange event listenera koji nam služi za prevod.

getNewDataCols()

Objedinjuje getData I getCols I na sličan način puni tabelu podacima ugnježdene tabele.

## GenericForm

Za instanciranje generičke forme neophodno je definisati sledeće argumente

1. details

details je lista objekata input_details, ovo je primer jednog takvog objetka

{

input_type text,

input_label Ime,

},

1. group

Potrebno je napraviti formsGroup sa istim brojem polja kao što je definisano u details.

1. infObj

Ovde se prosledjuje objekat koji će ako postoji, ispunjavati value polja.

Imamo tri funkcije getNastavnikForm, getStudentForm, getRegKorisnikForm koje na osnovu korisnika koji pristupa stranici generisu gore navedene argumente.

Assets URLS

- _ROOT – assetsimages  
    _U root-u se nalaze elementi koje koristimo ili postoji mogućnost da ćemo koristiti kroz celu aplikaciju.
- _Univerziteti – rootuniverziteti_

_unicards_ – slike za card-ove prilikom odabira univerziteta

_uniName –_ naziv univerziteta, gde se dalje nalaze njegovi asset-i kao što su logo i pozadina.

- _Fakulteti – rootfakulteti  
    unicards_ – slike za card-ove prilikom odabira univerziteta  
    _uniName –_ naziv univerziteta, gde se dalje nalaze njegovi asset-i kao što su logo i pozadina.
- _Komponente – rootcomponentName_ – naziv komponente I unutra njeni asset-i_._

Assets Nazivanje

_Univerziteta_

- Pozadine  
    name+”_background.jpg”
- Logotipi  
    name+”_logo.png”

_Fakulteta_

Igor TODO

_neReg_  
realizacijaPredmeta

_Reg_  
Uraditi Student 23, 33  

23  
    Istorija studiranja je i dalje tabela predmeta, samo će sadržati drugačije kolumne, specificirane u dizajnu.
    colLimit je disabled

Messaging app  
Nastavni materijal  

Uroš TODO   
Nastavnik