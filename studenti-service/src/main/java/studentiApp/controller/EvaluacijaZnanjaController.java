package studentiApp.controller;

import java.lang.reflect.ParameterizedType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.EvaluacijaZnanja;
import studentiApp.model.GenericConverter;
import studentiApp.model.dto.EvaluacijaZnanjaDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/evaluacijeznanja")
public class EvaluacijaZnanjaController
		extends GenericController<EvaluacijaZnanja, EvaluacijaZnanjaDTO, GenericService<EvaluacijaZnanja, Integer>> {
	public EvaluacijaZnanjaController(GenericService<EvaluacijaZnanja, Integer> service) {
		super(service);
	}

	private int highestId = 0;

	@GetMapping("/byrelid/{relId}")
	public ResponseEntity<EvaluacijaZnanjaDTO> getByStudentId(@PathVariable("relId") Integer relId) {
	    EvaluacijaZnanjaDTO dto = null;
	    @SuppressWarnings("unchecked")
	    Class<EvaluacijaZnanjaDTO> dtoClass = (Class<EvaluacijaZnanjaDTO>) ((ParameterizedType) getClass()
	            .getGenericSuperclass()).getActualTypeArguments()[1];
	    Iterable<EvaluacijaZnanja> evaluacije = this.service.findAll();
	    for (EvaluacijaZnanja e : evaluacije) {
	        if (e.getRealizacijaPredmetaId() == relId) {
	            GenericConverter<EvaluacijaZnanja, EvaluacijaZnanjaDTO> genConv = new GenericConverter<>();
	            dto = genConv.convertToDTO(e, dtoClass);
	            break;
	        }
	    }
	    return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping("/getHighestId")
	public ResponseEntity<Integer> getHighestId() {
		int highestId = 0;
		Iterable<EvaluacijaZnanja> evaluacije = this.service.findAll();
		evaluacije.forEach(ev -> {
			if (highestId == 0) {
				this.highestId = ev.getId();
			} else if (highestId < ev.getId()) {
				this.highestId = ev.getId();
			}
		});
		return new ResponseEntity<>(this.highestId, HttpStatus.OK);
	}


}