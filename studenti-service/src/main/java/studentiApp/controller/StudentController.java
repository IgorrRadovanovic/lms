package studentiApp.controller;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.GenericConverter;
import studentiApp.model.Student;
import studentiApp.model.dto.StudentDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/studenti")
public class StudentController extends GenericController<Student, StudentDTO, GenericService<Student, Integer>> {
	public StudentController(GenericService<Student, Integer> service) {
		super(service);
	}

	@CrossOrigin(origins = { "http://localhost:4200", "http://localhost:8081" })
	@PatchMapping("/{id}")
	public ResponseEntity<StudentDTO> update(@PathVariable("id") Integer id, @RequestBody StudentDTO dto)
			throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		Student student =this.service.findById(id).get();
		System.out.println("Patch called");
		System.out.println(dto.toString()+ " "+ student.toString());
	     if (dto.getJmbg() != null) {
	            student.setJmbg(dto.getJmbg());
	        }
	        if (dto.getIme() != null) {
	            student.setIme(dto.getIme());
	        }
	        if (dto.getAdresaId() != null) {
	            student.setAdresaId(dto.getAdresaId());
	        }
	        if (dto.getBrojTelefona() != null) {
	            student.setBrojTelefona(dto.getBrojTelefona());
	        }
	        if (dto.getEmail() != null) {
	            student.setEmail(dto.getEmail());
	        }
		service.save(student);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}

}