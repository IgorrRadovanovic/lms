package studentiApp.controller;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.GenericConverter;
import studentiApp.model.Polaganje;
import studentiApp.model.dto.PolaganjeDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/polaganja")
public class PolaganjeController
		extends GenericController<Polaganje, PolaganjeDTO, GenericService<Polaganje, Integer>> {
	public PolaganjeController(GenericService<Polaganje, Integer> service) {
		super(service);
	}

	@GetMapping("/bystudgod/{studGodId}")
	public ResponseEntity<Iterable<PolaganjeDTO>> getByStudentId(@PathVariable("studGodId") Integer studGodId) {

		@SuppressWarnings("unchecked")
		Class<PolaganjeDTO> dtoClass = (Class<PolaganjeDTO>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
		Iterable<Polaganje> polaganja = this.service.findAll();
		ArrayList<PolaganjeDTO> dtos = new ArrayList<>();
		polaganja.forEach(e -> {
			if (e.getStudentNaGodini().getId() == studGodId) {
				PolaganjeDTO dto = null;
				GenericConverter<Polaganje, PolaganjeDTO> genConv = new GenericConverter<>();
				dto = genConv.convertToDTO(e, dtoClass);
				dtos.add(dto);
			}
		});

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

}