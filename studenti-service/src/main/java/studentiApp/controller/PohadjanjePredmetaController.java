package studentiApp.controller;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.GenericConverter;
import studentiApp.model.PohadjanjePredmeta;
import studentiApp.model.dto.PohadjanjePredmetaDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/pohadjanjapredmeta")
public class PohadjanjePredmetaController extends
		GenericController<PohadjanjePredmeta, PohadjanjePredmetaDTO, GenericService<PohadjanjePredmeta, Integer>> {
	public PohadjanjePredmetaController(GenericService<PohadjanjePredmeta, Integer> service) {
		super(service);
	}

	@GetMapping("/bystudgod/{studGodId}")
	public ResponseEntity<Iterable<PohadjanjePredmetaDTO>> getByStudentId(@PathVariable("studGodId") Integer studGodId) {
		@SuppressWarnings("unchecked")
		Class<PohadjanjePredmetaDTO> dtoClass = (Class<PohadjanjePredmetaDTO>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[1];
		ArrayList<PohadjanjePredmetaDTO> dtos = new ArrayList<>();
		Iterable<PohadjanjePredmeta> pohadjanja = this.service.findAll();
		pohadjanja.forEach(p -> {
//			System.out.println(s.getStudent().getId() + " " + studId);
			if (p.getStudentNaGodini().getId() == studGodId) {
				GenericConverter<PohadjanjePredmeta, PohadjanjePredmetaDTO> genConv = new GenericConverter<>();
				PohadjanjePredmetaDTO dto = genConv.convertToDTO(p, dtoClass);
				dtos.add(dto);
			}
		});
		return new ResponseEntity<>(dtos, HttpStatus.OK);

	}
}