package studentiApp.controller;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


import studentiApp.model.GenericConverter;
import studentiApp.model.StudentNaGodini;
import studentiApp.model.dto.StudentNaGodiniDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/studentinagodini")
public class StudentNaGodiniController
		extends GenericController<StudentNaGodini, StudentNaGodiniDTO, GenericService<StudentNaGodini, Integer>> {
	public StudentNaGodiniController(GenericService<StudentNaGodini, Integer> service) {
		super(service);
	}

	@GetMapping("/bystudent/{studId}")
	public ResponseEntity<Iterable<StudentNaGodiniDTO>> getByStudentId(@PathVariable("studId") Integer studId) {
//		System.out.println("test");
		@SuppressWarnings("unchecked")
		Class<StudentNaGodiniDTO> dtoClass = (Class<StudentNaGodiniDTO>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[1];
		ArrayList<StudentNaGodiniDTO> dtos = new ArrayList<>();
		Iterable<StudentNaGodini> studNaGod = this.service.findAll();
		studNaGod.forEach(s -> {
//			System.out.println(s.getStudent().getId() + " " + studId);
			if (s.getStudent().getId() == studId) {
				GenericConverter<StudentNaGodini, StudentNaGodiniDTO> genConv = new GenericConverter<>();
				StudentNaGodiniDTO dto = genConv.convertToDTO(s, dtoClass);
				dtos.add(dto);
			}
		});
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@GetMapping("/byssp/{studId}/{studProId}")
	public ResponseEntity<Iterable<StudentNaGodiniDTO>> getBySsp(@PathVariable("studId") Integer studId,
		    @PathVariable("studProId") Integer studProID) { 
//		System.out.println("test");
		@SuppressWarnings("unchecked")
		Class<StudentNaGodiniDTO> dtoClass = (Class<StudentNaGodiniDTO>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[1];
		ArrayList<StudentNaGodiniDTO> dtos = new ArrayList<>();
		Iterable<StudentNaGodini> studNaGod = this.service.findAll();
		studNaGod.forEach(s -> {
//			System.out.println(s.getStudent().getId() + " " + studId);
			if (s.getStudijskiProgramId()==studProID && s.getStudent().getId() == studId  ) {
				GenericConverter<StudentNaGodini, StudentNaGodiniDTO> genConv = new GenericConverter<>();
				StudentNaGodiniDTO dto = genConv.convertToDTO(s, dtoClass);
				dtos.add(dto);
			}
		});
        return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

}