package studentiApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


import studentiApp.model.InstrumentEvaluacije;
import studentiApp.model.dto.InstrumentEvaluacijeDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/instrumentievaluacije")
public class InstrumentEvaluacijeController extends
		GenericController<InstrumentEvaluacije, InstrumentEvaluacijeDTO, GenericService<InstrumentEvaluacije, Integer>> {
	public InstrumentEvaluacijeController(GenericService<InstrumentEvaluacije, Integer> service) {
		super(service);
	}
}