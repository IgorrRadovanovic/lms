package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.TipEvaluacije; 
 public interface TipEvaluacijeRepository extends CrudRepository<TipEvaluacije, Integer> {

}