package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.InstrumentEvaluacije; 
 public interface InstrumentEvaluacijeRepository extends CrudRepository<InstrumentEvaluacije, Integer> {

}