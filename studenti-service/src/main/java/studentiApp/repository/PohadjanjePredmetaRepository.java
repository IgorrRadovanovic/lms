package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.PohadjanjePredmeta; 
 public interface PohadjanjePredmetaRepository extends CrudRepository<PohadjanjePredmeta, Integer> {

}