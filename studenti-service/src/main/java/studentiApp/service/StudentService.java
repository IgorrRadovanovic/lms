package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.Student;
import studentiApp.repository.StudentRepository;  
@Service
public class StudentService extends GenericService<Student, Integer> {
public StudentService(StudentRepository repository){super(repository);
}


}