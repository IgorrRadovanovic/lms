package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.StudentNaGodini;
import studentiApp.repository.StudentNaGodiniRepository;  
@Service
public class StudentNaGodiniService extends GenericService<StudentNaGodini, Integer> {
public StudentNaGodiniService(StudentNaGodiniRepository repository){super(repository);
}
}