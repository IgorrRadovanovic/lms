package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.InstrumentEvaluacije;
import studentiApp.repository.InstrumentEvaluacijeRepository;  
@Service
public class InstrumentEvaluacijeService extends GenericService<InstrumentEvaluacije, Integer> {
public InstrumentEvaluacijeService(InstrumentEvaluacijeRepository repository){super(repository);
}
}