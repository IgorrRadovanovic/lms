package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.TipEvaluacije;
import studentiApp.repository.TipEvaluacijeRepository;  
@Service
public class TipEvaluacijeService extends GenericService<TipEvaluacije, Integer> {
public TipEvaluacijeService(TipEvaluacijeRepository repository){super(repository);
}
}