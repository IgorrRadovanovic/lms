package studentiApp.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Polaganje {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	@Column(nullable=false)
	private Integer bodovi;
	@Column(nullable=false)
	private String napomena;
	@ManyToOne
	private StudentNaGodini studentNaGodini;
	@ManyToOne
	private EvaluacijaZnanja evaluacijaZnanja;
	
	
	public Polaganje() {
		super();
    }
	
	public Polaganje(Integer id, int bodovi, String napomena,StudentNaGodini studentNaGodini, EvaluacijaZnanja evaluacijaZnanja) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.napomena = napomena;
		this.studentNaGodini=studentNaGodini;
		this.evaluacijaZnanja=evaluacijaZnanja;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getBodovi() {
		return bodovi;
	}
	public void setBodovi(Integer bodovi) {
		this.bodovi = bodovi;
	}
	public String getNapomena() {
		return napomena;
	}
	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}
	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}
	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	@Override
	public String toString() {
		return "Polaganje [id=" + id + ", bodovi=" + bodovi + ", napomena=" + napomena + ", studentNaGodini="
				+ studentNaGodini + ", evaluacijaZnanja=" + evaluacijaZnanja + "]";
	}
	
	
	
	
}
