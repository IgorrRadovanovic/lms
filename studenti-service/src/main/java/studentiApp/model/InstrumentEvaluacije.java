package studentiApp.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class InstrumentEvaluacije {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String naziv;
	
	@OneToMany(mappedBy="instrument")
	private List<EvaluacijaZnanja> evaluacije = new ArrayList<>();

	public InstrumentEvaluacije() {
		super();
	}

	public InstrumentEvaluacije(Integer id, String naziv, List<EvaluacijaZnanja> evaluacije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.evaluacije = evaluacije;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<EvaluacijaZnanja> getEvaluacije() {
		return evaluacije;
	}

	public void setEvaluacije(List<EvaluacijaZnanja> evaluacije) {
		this.evaluacije = evaluacije;
	}
	
	
}