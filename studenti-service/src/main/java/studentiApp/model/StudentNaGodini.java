package studentiApp.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class StudentNaGodini {
	public StudentNaGodini() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private LocalDate datumUpisa;
	@Column(nullable = false)
	private String brojIndeksa;
	
	@OneToMany(mappedBy = "studentNaGodini")
	private List<PohadjanjePredmeta> pohadjanjePredmeta = new ArrayList<PohadjanjePredmeta>();
	
	@Column
	private Integer godinaStudijaId;
	@OneToMany
	private List<Polaganje> polaganja = new ArrayList<Polaganje>();
	@ManyToOne
	private Student student;
	@Column(nullable = false)
	private Integer studijskiProgramId;

	public StudentNaGodini(LocalDate datumUpisa, String brojIndeksa,
			ArrayList<PohadjanjePredmeta> pohadjanjePredmeta, Integer godinaStudijaId,
			ArrayList<Polaganje> polaganja, Integer studijskiProgramId) {
		this.datumUpisa = datumUpisa;
		this.brojIndeksa = brojIndeksa;
		this.godinaStudijaId = godinaStudijaId;
		this.pohadjanjePredmeta = pohadjanjePredmeta;
		this.polaganja = polaganja;
		this.studijskiProgramId = studijskiProgramId;
	}

	public LocalDate getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(LocalDate datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public List<PohadjanjePredmeta> getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(List<PohadjanjePredmeta> pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Polaganje> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(List<Polaganje> polaganja) {
		this.polaganja = polaganja;
	}

	public Integer getGodinaStudijaId() {
		return godinaStudijaId;
	}

	public void setGodinaStudijaId(Integer godinaStudijaId) {
		this.godinaStudijaId = godinaStudijaId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Integer getStudijskiProgramId() {
		return studijskiProgramId;
	}

	public void setStudijskiProgramId(Integer studijskiProgramId) {
		this.studijskiProgramId = studijskiProgramId;
	}



}
