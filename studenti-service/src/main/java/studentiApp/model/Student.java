package studentiApp.model;



import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
//import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
@Entity
public class Student {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String jmbg;
	@Column(nullable=false)
	private String ime;
	@Column
	private Integer adresaId;
	@OneToMany(mappedBy = "student")
	private List<StudentNaGodini> studentNaGodinama = new ArrayList<>();
	@Column(nullable=false)
	private String brojTelefona;
	@Column(nullable=false)
	private String email;
	
	public Student(Integer id, String jmbg, String ime, Integer adresaId, ArrayList<StudentNaGodini> studentNaGodinama,
			String brojTelefona, String email) {
		this.id = id;
		this.jmbg = jmbg;
		this.ime = ime;
		this.adresaId = adresaId;
		this.studentNaGodinama = studentNaGodinama;
		this.brojTelefona = brojTelefona;
		this.email = email;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<StudentNaGodini> getStudentNaGodinama() {
		return studentNaGodinama;
	}

	public void setStudentNaGodinama(List<StudentNaGodini> studentNaGodinama) {
		this.studentNaGodinama = studentNaGodinama;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", jmbg=" + jmbg + ", ime=" + ime + ", adresaId=" + adresaId
				+ ", studentNaGodinama=" + studentNaGodinama + ", brojTelefona=" + brojTelefona + ", email=" + email
				+ "]";
	}
	
	
	
	
	
	
	
	

}
