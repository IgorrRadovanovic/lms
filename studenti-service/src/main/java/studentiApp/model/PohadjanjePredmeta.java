package studentiApp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class PohadjanjePredmeta {

	public PohadjanjePredmeta() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column()
	private Integer konacnaOcena;
	@ManyToOne
	@JoinColumn(name = "studentNaGodini_id", nullable = false)
	private StudentNaGodini studentNaGodini;

	@Column()
	private Integer realizacijaPredmetaId;

	public PohadjanjePredmeta(Integer id, int konacnaOcena, StudentNaGodini studentNaGodini,
			Integer realizacijaPredmeta) {
		super();
		this.id = id;
		this.konacnaOcena = konacnaOcena;
		this.studentNaGodini = studentNaGodini;
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public Integer getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(Integer konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(Integer realizacijaPredmeta) {
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

}
