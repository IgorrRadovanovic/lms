package studentiApp.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class TipEvaluacije {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column()
	private String naziv;
	
	@OneToMany(mappedBy="tip")
	private List<EvaluacijaZnanja> evaluacije = new ArrayList<>();

	public TipEvaluacije(Integer id,String tipEvaluacije) {
		super();
		this.id = id;
		this.naziv = tipEvaluacije;
	}

	public TipEvaluacije() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

}