package studentiApp.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class EvaluacijaZnanja {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private LocalDateTime vremePocetka;
	@Column(nullable = false)
	private LocalDateTime vremeZavrsetka;
	@ManyToOne
	private TipEvaluacije tip;
	@ManyToOne
	private InstrumentEvaluacije instrument;
	@Column(nullable = false)
	private Integer realizacijaPredmetaId;
	@Column(nullable = false)
	private Integer ishodId;

	public EvaluacijaZnanja() {
		super();
	}

	public EvaluacijaZnanja(Integer id, LocalDateTime vremePocetka, LocalDateTime vremeZavrsetka, TipEvaluacije tip,
			InstrumentEvaluacije instrument, Integer ishodId, Integer realizacijaPredmetaId) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.tip = tip;
		this.instrument = instrument;
		this.ishodId = ishodId;
		this.realizacijaPredmetaId = realizacijaPredmetaId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(LocalDateTime vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public LocalDateTime getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(LocalDateTime vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public TipEvaluacije getTip() {
		return tip;
	}

	public void setTip(TipEvaluacije tip) {
		this.tip = tip;
	}

	public InstrumentEvaluacije getInstrument() {
		return instrument;
	}

	public void setInstrument(InstrumentEvaluacije instrument) {
		this.instrument = instrument;
	}

	public Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(Integer realizacijaPredmetaID) {
		this.realizacijaPredmetaId = realizacijaPredmetaID;
	}

	public Integer getIshodId() {
		return ishodId;
	}

	public void setIshodId(Integer ishodId) {
		this.ishodId = ishodId;
	}

//	public RealizacijaPredmeta getRealizacijaPredmeta() {
//		return realizacijaPredmeta;
//	}
//
//	public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
//		this.realizacijaPredmeta = realizacijaPredmeta;
//	}

}
