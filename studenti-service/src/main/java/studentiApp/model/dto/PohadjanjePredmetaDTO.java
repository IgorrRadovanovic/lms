package studentiApp.model.dto;

public class PohadjanjePredmetaDTO {

	public PohadjanjePredmetaDTO() {
		super();
	}

	

	private Integer id;

	private Integer konacnaOcena;
	
    private StudentNaGodiniDTO studentNaGodini;

    private Integer realizacijaPredmetaId;
	
	public PohadjanjePredmetaDTO(Integer id,Integer konacnaOcena,StudentNaGodiniDTO studentNaGodini,Integer realizacijaPredmeta) {
		super();
		this.id = id;
		this.konacnaOcena=konacnaOcena;
		this.studentNaGodini = studentNaGodini;
		this.realizacijaPredmetaId=realizacijaPredmeta;
	}

	public Integer getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(Integer konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(Integer realizacijaPredmeta) {
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentNaGodiniDTO getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodiniDTO studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}
	
	
	
}
