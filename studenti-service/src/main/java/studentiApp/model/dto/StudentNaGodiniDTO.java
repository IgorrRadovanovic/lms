package studentiApp.model.dto;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;

public class StudentNaGodiniDTO {
	public StudentNaGodiniDTO() {
		super();
	}


	private Integer id;

	private LocalDate datumUpisa;

	private String brojIndeksa;

	private List<PohadjanjePredmetaDTO> pohadjanjePredmeta=new ArrayList<PohadjanjePredmetaDTO>();

	private Integer godinaStudijaId;

	private List<PolaganjeDTO> polaganja=new ArrayList<PolaganjeDTO>();
	
	private Integer studijskiProgramId;
	
	private StudentDTO student;
	
	public StudentNaGodiniDTO(LocalDate datumUpisa, String brojIndeksa, ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta,Integer godinaStudijaId, ArrayList<PolaganjeDTO> polaganja,Integer studijskiProgramId) {
		this.datumUpisa=datumUpisa;
		this.brojIndeksa=brojIndeksa;
		this.godinaStudijaId=godinaStudijaId;
		this.pohadjanjePredmeta=pohadjanjePredmeta;
		this.polaganja=polaganja;
		this.studijskiProgramId=studijskiProgramId;
	}

	public LocalDate getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(LocalDate datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	

	public List<PohadjanjePredmetaDTO> getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(List<PohadjanjePredmetaDTO> pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<PolaganjeDTO> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(List<PolaganjeDTO> polaganja) {
		this.polaganja = polaganja;
	}

	public Integer getGodinaStudijaId() {
		return godinaStudijaId;
	}

	public void setGodinaStudijaId(Integer godinaStudijaId) {
		this.godinaStudijaId = godinaStudijaId;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public Integer getStudijskiProgramId() {
		return studijskiProgramId;
	}

	public void setStudijskiProgramId(Integer studijskiProgramId) {
		this.studijskiProgramId = studijskiProgramId;
	}


	
	

	
}
