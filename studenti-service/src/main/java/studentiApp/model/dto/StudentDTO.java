package studentiApp.model.dto;



import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;

public class StudentDTO {
	

	private Integer id;

	private String jmbg;

	private String ime;

	private Integer adresaId;

	private List<StudentNaGodiniDTO> studentNaGodinama = new ArrayList<>();
	
	private String brojTelefona;
	
	private String email;
	
	public StudentDTO(Integer id,String jmbg, String ime, Integer adresaId,  ArrayList<StudentNaGodiniDTO> studentNaGodini, String brojTelefona, String email) {
		this.id = id;
		this.jmbg=jmbg;
		this.ime=ime;
		this.adresaId=adresaId;
		this.studentNaGodinama=studentNaGodini;
		this.brojTelefona=brojTelefona;
		this.email=email;
	}

	public StudentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<StudentNaGodiniDTO> getStudentNaGodinama() {
		return studentNaGodinama;
	}

	public void setStudentNaGodinama(ArrayList<StudentNaGodiniDTO> studentNaGodinama) {
		this.studentNaGodinama = studentNaGodinama;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "StudentDTO [id=" + id + ", jmbg=" + jmbg + ", ime=" + ime + ", adresaId=" + adresaId
				+ ", studentNaGodinama=" + studentNaGodinama + ", brojTelefona=" + brojTelefona + ", email=" + email
				+ "]";
	}
	

}
