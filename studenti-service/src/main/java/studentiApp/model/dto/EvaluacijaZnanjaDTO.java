package studentiApp.model.dto;

import java.time.LocalDateTime;


public class EvaluacijaZnanjaDTO {
	

	private Integer id;

	private LocalDateTime vremePocetka;

	private LocalDateTime vremeZavrsetka;
	
	private TipEvaluacijeDTO tip;
	
	
	private InstrumentEvaluacijeDTO instrument;
	
	private Integer realizacijaPredmetaId;

	private Integer ishodId;

	
	public EvaluacijaZnanjaDTO() {
		super();
	}
	
	public EvaluacijaZnanjaDTO(Integer id, LocalDateTime vremePocetka, LocalDateTime vremeZavrsetka, TipEvaluacijeDTO tip,
			InstrumentEvaluacijeDTO instrument,Integer ishodId,Integer realizacijaPredmetaId) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.tip = tip;
		this.instrument = instrument;
		this.ishodId = ishodId;
		this.realizacijaPredmetaId = realizacijaPredmetaId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(LocalDateTime vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public LocalDateTime getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(LocalDateTime vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public TipEvaluacijeDTO getTip() {
		return tip;
	}

	public void setTip(TipEvaluacijeDTO tip) {
		this.tip = tip;
	}


	public InstrumentEvaluacijeDTO getInstrument() {
		return instrument;
	}

	public void setInstrument(InstrumentEvaluacijeDTO instrument) {
		this.instrument = instrument;
	}

	public Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(Integer realizacijaPredmetaID) {
		this.realizacijaPredmetaId = realizacijaPredmetaID;
	}

	public Integer getIshodId() {
		return ishodId;
	}

	public void setIshodId(Integer ishodId) {
		this.ishodId = ishodId;
	}

//	public RealizacijaPredmetaDTO getRealizacijaPredmeta() {
//		return realizacijaPredmeta;
//	}
//
//	public void setRealizacijaPredmeta(RealizacijaPredmetaDTO realizacijaPredmeta) {
//		this.realizacijaPredmeta = realizacijaPredmeta;
//	}

}
