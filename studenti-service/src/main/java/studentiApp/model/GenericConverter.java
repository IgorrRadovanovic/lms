package studentiApp.model;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;

//import org.springframework.security.crypto.password.PasswordEncoder;

//import root.security.SecurityConfiguration;

public class GenericConverter<T, DTO> {


	public DTO convertToDTO(T entity, Class<DTO> dtoClass) { // deprecated, moving to static methods
		DTO dto = null;
		try {
			dto = dtoClass.getDeclaredConstructor().newInstance();
			// Kreiram klasu beanInfo koja daje sve detalje bean-a
			BeanInfo beanInfo = Introspector.getBeanInfo(dtoClass);
			// Prolazim kroz properti deskriptore
			for (PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) {
				// uzimam naziv propertija i ignorišem ako je property class
				String propertyName = pd.getName();
				Field entityField = null;
				if ("class".equals(propertyName)) {
					continue;
				}
				// Trazim isto to polje u običnoj klasi

				try { // Pokusavam da nadjem polje
					entityField = entity.getClass().getDeclaredField(propertyName);
					entityField.setAccessible(true);
					// Ako ne nadjem trazim u nad klasi
				} catch (NoSuchFieldException e) {
					entityField = entity.getClass().getSuperclass().getDeclaredField(propertyName);
				}
				// Uzimam vrednost
				Object entityValue = entityField.get(entity);
				// Ako nije iz paketa Java, računa se kao complex type i poziva se deepCopy
				// ako jeste samo se upisuje vrednost
//				System.out.println(entityField.getType()+"TEST");
				if (isComplexType(entityField.getType())) {
					Object dtoValue = deepCopy(entityValue);
					pd.getWriteMethod().invoke(dto, dtoValue);
				} else {
//					System.out.println("dto "+dto+" entityValue "+entityValue);
					pd.getWriteMethod().invoke(dto, entityValue);
				}
			}
		} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException
				| IntrospectionException | NoSuchFieldException e) {
			e.printStackTrace();
		}
		return dto;
	}

	private static Object deepCopy(Object object) {
		if (object instanceof List) {
			List<?> originalList = (List<?>) object;
			List<Object> copyList = new ArrayList<>();
			for (Object listItem : originalList) {
				Object listItemCopy = deepCopy(listItem);
				copyList.add(listItemCopy);
			}
			return copyList;
		} else {
//			LOG.info("test 2.3");
			Object copy = null;
			try {
				// Uzimamo klasu objekta
				if (object != null) {
					Class<?> objectClass = object.getClass();
					// Uzimamo odgovarajući DTO
					String dtoClassName = objectClass.getName().replace("model.", "model.dto.");
					Class<?> dtoClass = Class.forName(dtoClassName + "DTO");
					// Kreiramo novu instancu DTO klase
					copy = dtoClass.getDeclaredConstructor().newInstance();
					BeanUtils.copyProperties(object, copy);
				}
			} catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException
					| ClassNotFoundException e) {

				e.printStackTrace();
			}
			return copy;
		}
	}



	public <T> T convertToEntity(Object dto, Class<T> entityClass)
			throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		T entity = null;
		System.out.println(dto);
		try {
			entity = entityClass.getDeclaredConstructor().newInstance();
			BeanUtils.copyProperties(dto, entity);
			for (Field field : dto.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object nestedDto = field.get(dto);
				if (nestedDto != null && isComplexType(nestedDto.getClass())
						&& !isJavaTimeType(nestedDto.getClass())) {
					Field entityField = entityClass.getDeclaredField(field.getName());
					entityField.setAccessible(true);
					Object nestedEntity = entityField.getType().getDeclaredConstructor().newInstance();
					BeanUtils.copyProperties(nestedDto, nestedEntity);
					entityField.set(entity, nestedEntity);
				}
			}
		} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
			e.printStackTrace();
		}
		return entity;
	}
	
	
	//is CopmlexType i isPrimitiveOrWrapper rade slican posao
	
	private static boolean isComplexType(Class<?> type) {
		return !type.getPackage().getName().startsWith("java") || List.class.isAssignableFrom(type);
	}

	private boolean isJavaTimeType(Class<?> type) {
		return type == LocalDateTime.class || type == LocalDate.class || type == LocalTime.class
				|| type == ZonedDateTime.class || type == OffsetDateTime.class || type == OffsetTime.class;
	}
}