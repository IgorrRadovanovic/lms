package adreseApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@SpringBootApplication( /*exclude = {SecurityAutoConfiguration.class }*/ )//to be changed
@EnableDiscoveryClient
public class AdreseApp {
	public static void main(String[] args) {
		SpringApplication.run(AdreseApp.class, args);
	}

}
