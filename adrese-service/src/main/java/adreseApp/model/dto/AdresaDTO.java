package adreseApp.model.dto;

//import java.util.ArrayList;

import jakarta.persistence.Column;

public class AdresaDTO implements DTOObject{
	
	

	private Integer id;
	
	@Column
	private String ulica;
	
	@Column
	private String broj;
	
	public AdresaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	private MestoDTO mesto;
	

//	private ArrayList<UniverzitetDTO> univerziteti;
//	

//	private ArrayList<FakultetDTO> fakulteti;
//	

//	private ArrayList<StudentDTO> studenti;
//	

//	private ArrayList<NastavnikDTO> nastavnici;

	

	public AdresaDTO(Integer id, String ulica, String broj, MestoDTO mesto/*, ArrayList<UniverzitetDTO> univerziteti,
			ArrayList<FakultetDTO> fakulteti, ArrayList<StudentDTO> studenti, ArrayList<NastavnikDTO> nastavnici*/) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
//		this.univerziteti = univerziteti;
//		this.fakulteti = fakulteti;
//		this.studenti = studenti;
//		this.nastavnici = nastavnici;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public MestoDTO getMesto() {
		return mesto;
	}

	public void setMesto(MestoDTO mesto) {
		this.mesto = mesto;
	}

//	public ArrayList<UniverzitetDTO> getUniverziteti() {
//		return univerziteti;
//	}
//
//	public void setUniverziteti(ArrayList<UniverzitetDTO> univerziteti) {
//		this.univerziteti = univerziteti;
//	}
//
//	public ArrayList<FakultetDTO> getFakulteti() {
//		return fakulteti;
//	}
//
//	public void setFakulteti(ArrayList<FakultetDTO> fakulteti) {
//		this.fakulteti = fakulteti;
//	}
//
//	public ArrayList<StudentDTO> getStudenti() {
//		return studenti;
//	}
//
//	public void setStudenti(ArrayList<StudentDTO> studenti) {
//		this.studenti = studenti;
//	}
//
//	public ArrayList<NastavnikDTO> getNastavnici() {
//		return nastavnici;
//	}
//
//	public void setNastavnici(ArrayList<NastavnikDTO> nastavnici) {
//		this.nastavnici = nastavnici;
//	}

	
}
