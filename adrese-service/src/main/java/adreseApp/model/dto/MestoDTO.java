package adreseApp.model.dto;

import java.util.ArrayList;
import java.util.List;


public class MestoDTO implements DTOObject{
	public MestoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	private Integer id;
	

	private DrzavaDTO drzava;
	

	private String naziv;
	

	private List<AdresaDTO> adrese = new ArrayList<AdresaDTO>();

	public MestoDTO(Integer id, DrzavaDTO drzava, String naziv,List<AdresaDTO> adrese) {
		super();
		this.id = id;
		this.drzava = drzava;
		this.naziv = naziv;
		this.adrese = adrese;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DrzavaDTO getDrzava() {
		return drzava;
	}

	public void setDrzava(DrzavaDTO drzava) {
		this.drzava = drzava;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<AdresaDTO> getAdrese() {
		return adrese;
	}

	public void setAdrese(List<AdresaDTO> adrese) {
		this.adrese = adrese;
	}

	

}
