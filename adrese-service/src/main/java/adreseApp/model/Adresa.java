package adreseApp.model;

//import java.util.ArrayList;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
//import jakarta.persistence.OneToMany;
@Entity
public class Adresa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String ulica;
	
	@Column
	private String broj;
	
	public Adresa() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ManyToOne(optional = true)
	private Mesto mesto;
	
//	@OneToMany(mappedBy="adresa")
//	private ArrayList<Univerzitet> univerziteti;
//	
//	@OneToMany(mappedBy="adresa")
//	private ArrayList<Fakultet> fakulteti;
//	
//	@OneToMany(mappedBy="adresa")
//	private ArrayList<Student> studenti;
//	
//	@OneToMany(mappedBy="adresa")
//	private ArrayList<Nastavnik> nastavnici;

	

	public Adresa(Integer id, String ulica, String broj, Mesto mesto/*, ArrayList<Univerzitet> univerziteti,
			ArrayList<Fakultet> fakulteti, ArrayList<Student> studenti, ArrayList<Nastavnik> nastavnici*/) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
//		this.univerziteti = univerziteti;
//		this.fakulteti = fakulteti;
//		this.studenti = studenti;
//		this.nastavnici = nastavnici;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public Mesto getMesto() {
		return mesto;
	}

	public void setMesto(Mesto mesto) {
		this.mesto = mesto;
	}

//	public ArrayList<Univerzitet> getUniverziteti() {
//		return univerziteti;
//	}
//
//	public void setUniverziteti(ArrayList<Univerzitet> univerziteti) {
//		this.univerziteti = univerziteti;
//	}
//
//	public ArrayList<Fakultet> getFakulteti() {
//		return fakulteti;
//	}
//
//	public void setFakulteti(ArrayList<Fakultet> fakulteti) {
//		this.fakulteti = fakulteti;
//	}
//
//	public ArrayList<Student> getStudenti() {
//		return studenti;
//	}
//
//	public void setStudenti(ArrayList<Student> studenti) {
//		this.studenti = studenti;
//	}
//
//	public ArrayList<Nastavnik> getNastavnici() {
//		return nastavnici;
//	}
//
//	public void setNastavnici(ArrayList<Nastavnik> nastavnici) {
//		this.nastavnici = nastavnici;
//	}

	
}
