package adreseApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import adreseApp.model.Adresa; 
 public interface AdresaRepository extends CrudRepository<Adresa, Integer> {

}