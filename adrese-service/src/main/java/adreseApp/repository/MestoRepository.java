package adreseApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import adreseApp.model.Mesto; 
 public interface MestoRepository extends CrudRepository<Mesto, Integer> {

}