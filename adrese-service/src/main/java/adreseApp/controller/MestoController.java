package adreseApp.controller;

import java.lang.reflect.ParameterizedType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import adreseApp.model.Mesto;
import adreseApp.model.dto.MestoDTO;
import adreseApp.service.GenericService;


@Controller
@RequestMapping("/api/mesta")
public class MestoController extends GenericController<Mesto, MestoDTO, GenericService<Mesto, Integer>> {
	public MestoController(GenericService<Mesto, Integer> service) {
		super(service);
	}
	@GetMapping("/byrelid/{relId}")
	public ResponseEntity<MestoDTO> getByStudentId(@PathVariable("relId") Integer relId) {
		MestoDTO dto = null;
	    @SuppressWarnings("unchecked")
	    Class<MestoDTO> dtoClass = (Class<MestoDTO>) ((ParameterizedType) getClass()
	            .getGenericSuperclass()).getActualTypeArguments()[1];
	    Iterable<Mesto> evaluacije = this.service.findAll();
	    
	    
//	    for (EvaluacijaZnanja e : evaluacije) {
//	        if (e.getRealizacijaPredmetaId() == relId) {
//	            GenericConverter<EvaluacijaZnanja, EvaluacijaZnanjaDTO> genConv = new GenericConverter<>();
//	            dto = genConv.convertToDTO(e, dtoClass);
//	            break;
//	        }
//	    }
	    return new ResponseEntity<>(dto, HttpStatus.OK);
	}



}