package adreseApp.controller;

import java.lang.reflect.InvocationTargetException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import adreseApp.model.Adresa;
import adreseApp.model.GenericConverter;
import adreseApp.model.Mesto;
import adreseApp.model.dto.AdresaDTO;
import adreseApp.model.dto.MestoDTO;
import adreseApp.service.GenericService;


@Controller
@RequestMapping("/api/adrese")
public class AdresaController extends GenericController<Adresa, AdresaDTO, GenericService<Adresa, Integer>> {
	public AdresaController(GenericService<Adresa, Integer> service) {
		super(service);
	}
	private int highestId = 0;


	@CrossOrigin(origins = { "http://localhost:4200", "http://localhost:8081" })
	@PatchMapping("/{id}")
	public ResponseEntity<AdresaDTO> update(@PathVariable("id") Integer id, @RequestBody AdresaDTO dto)
			throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		GenericConverter<Mesto, MestoDTO> genConv = new GenericConverter<Mesto, MestoDTO>();
		Adresa adresa = this.service.findById(id).get();
		if (dto.getBroj() != null) {
			adresa.setBroj(dto.getBroj());
		}
		if (dto.getMesto() != null) {
			adresa.setMesto(genConv.convertToEntity(dto.getMesto(), Mesto.class));
		}
		if (dto.getUlica() != null) {
			adresa.setUlica(dto.getUlica());
		}
		service.save(adresa);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/getHighestId")
	public ResponseEntity<Integer> getHighestId() {
		int highestId = 0;
		Iterable<Adresa> obj = this.service.findAll();
		obj.forEach(o -> {
			if (highestId == 0) {
				this.highestId = o.getId();
			} else if (highestId < o.getId()) {
				this.highestId = o.getId();
			}
		});
		return new ResponseEntity<>(this.highestId, HttpStatus.OK);
	}

}