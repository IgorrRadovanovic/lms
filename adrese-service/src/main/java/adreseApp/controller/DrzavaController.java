package adreseApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import adreseApp.model.Drzava;
import adreseApp.model.dto.DrzavaDTO;
import adreseApp.service.GenericService;
@Controller 
@RequestMapping("/api/drzave")
public class DrzavaController extends GenericController<Drzava,DrzavaDTO,GenericService<Drzava, Integer>>{
public DrzavaController(GenericService<Drzava, Integer> service){
 super(service);
}}