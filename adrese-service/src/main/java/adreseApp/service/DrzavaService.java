package adreseApp.service; 
import org.springframework.stereotype.Service;

import adreseApp.model.Drzava;
import adreseApp.repository.DrzavaRepository;  
@Service
public class DrzavaService extends GenericService<Drzava, Integer> {
public DrzavaService(DrzavaRepository repository){super(repository);
}
}