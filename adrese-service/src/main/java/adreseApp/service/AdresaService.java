package adreseApp.service; 
import org.springframework.stereotype.Service;

import adreseApp.model.Adresa;
import adreseApp.repository.AdresaRepository;  
@Service
public class AdresaService extends GenericService<Adresa, Integer> {
public AdresaService(AdresaRepository repository){super(repository);
}
}