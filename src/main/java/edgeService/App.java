package edgeService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class App {
	public static void main(String[] args) {
            //ovo je gateway(rutira ka mikro servisima)
            // cloud zavisnosti iste kao mikro servis, + spring-cloud-starter-loadbalancer + s.c.s.-gateway-mvc
            //config
            // ime
            // port
            // rute:
            //  jedinstveni id rute
            //  uri=lb://MICROSERVICE1 - lb: load balancing
            // predikati
            // predikat path=/microservice/** - putanja za koju se request preusmerava na uri
            // filteri - priprema pre slanja requesta
            // trenutni filter : prepraviti putanju /microservice1/xyz u /xyz
		SpringApplication.run(App.class, args);
	}

}