package application.service; 
import org.springframework.stereotype.Service; 
import application.model.Obavestenje;
import application.repository.ObavestenjeRepository;  
@Service
public class ObavestenjeService extends GenericService<Obavestenje, Integer> {
public ObavestenjeService(ObavestenjeRepository repository){super(repository);
}
}