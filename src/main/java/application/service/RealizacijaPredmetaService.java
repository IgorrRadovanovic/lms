package application.service; 
import org.springframework.stereotype.Service; 
import application.model.RealizacijaPredmeta;
import application.repository.RealizacijaPredmetaRepository;  
@Service
public class RealizacijaPredmetaService extends GenericService<RealizacijaPredmeta, Integer> {
public RealizacijaPredmetaService(RealizacijaPredmetaRepository repository){super(repository);
}
}