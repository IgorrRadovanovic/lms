package application.service; 
import org.springframework.stereotype.Service; 
import application.model.Fakultet;
import application.repository.FakultetRepository;  
@Service
public class FakultetService extends GenericService<Fakultet, Integer> {
public FakultetService(FakultetRepository repository){super(repository);
}
}