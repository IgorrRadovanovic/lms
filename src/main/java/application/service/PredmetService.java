package application.service; 
import org.springframework.stereotype.Service; 
import application.model.Predmet;
import application.repository.PredmetRepository;  
@Service
public class PredmetService extends GenericService<Predmet, Integer> {
public PredmetService(PredmetRepository repository){super(repository);
}
}