package application.service; 
import org.springframework.stereotype.Service; 
import application.model.GodinaStudija;
import application.repository.GodinaStudijaRepository;  
@Service
public class GodinaStudijaService extends GenericService<GodinaStudija, Integer> {
public GodinaStudijaService(GodinaStudijaRepository repository){super(repository);
}
}