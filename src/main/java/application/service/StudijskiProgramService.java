package application.service; 
import org.springframework.stereotype.Service; 
import application.model.StudijskiProgram;
import application.repository.StudijskiProgramRepository;  
@Service
public class StudijskiProgramService extends GenericService<StudijskiProgram, Integer> {
public StudijskiProgramService(StudijskiProgramRepository repository){super(repository);
}
}