package application.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Fakultet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(optional = false)
	private Univerzitet univerzitet;
	
//	@ManyToOne()
//	private Adresa adresa;
	@Column
	private Integer adresaId;
	
//	@ManyToOne()
//	private Nastavnik dekan;
	@Column
	private Integer dekanId;
	
	@OneToMany(mappedBy="fakultet")
	private List<StudijskiProgram> studijski_programi = new ArrayList<StudijskiProgram>();
	
	public Fakultet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column
	private String naziv;
	
	public Fakultet(Univerzitet univerzitet, String naziv, /* Adresa */Integer adresaId,
			/* Nastavnik */ Integer dekanId,List<StudijskiProgram> studijski_programi) {
		super();
		this.univerzitet = univerzitet;
		this.naziv = naziv;
		this.adresaId = adresaId;
		this.dekanId = dekanId;
		this.studijski_programi = studijski_programi;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public Integer getDekanId() {
		return dekanId;
	}

	public void setDekanId(Integer dekanId) {
		this.dekanId = dekanId;
	}

	public List<StudijskiProgram> getStudijski_programi() {
		return studijski_programi;
	}

	public void setStudijski_programi(List<StudijskiProgram> studijski_programi) {
		this.studijski_programi = studijski_programi;
	}

//	public Adresa getAdresa() {
//		return adresa;
//	}
//
//	public void setAdresa(Adresa adresa) {
//		this.adresa = adresa;
//	}
//
//	public Nastavnik getDekan() {
//		return dekan;
//	}
//
//	public void setDekan(Nastavnik dekan) {
//		this.dekan = dekan;
//	}

}
