package application.model;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class TerminNastave {
	public TerminNastave() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	@Column(nullable=false)
	private LocalDateTime vremePocetka;
	@Column(nullable=false)
	private LocalDateTime vremeZavrsetka;
	@ManyToOne
	private RealizacijaPredmeta realizacijaPredmeta;
//	@ManyToOne
//	private TipNastave tipNastave;
	@Column 
	private Integer tipNastaveId;
	
	public TerminNastave(Integer id, LocalDateTime vremePocetka, LocalDateTime vremeZavrsetka,
			RealizacijaPredmeta realizacijaPredmeta, /*TipNastave*/Integer tipNastave) {
		super();
		this.id = id;
		this.vremePocetka = vremePocetka;
		this.vremeZavrsetka = vremeZavrsetka;
		this.realizacijaPredmeta = realizacijaPredmeta;
		this.tipNastaveId = tipNastave;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getVremePocetka() {
		return vremePocetka;
	}

	public void setVremePocetka(LocalDateTime vremePocetka) {
		this.vremePocetka = vremePocetka;
	}

	public LocalDateTime getVremeZavrsetka() {
		return vremeZavrsetka;
	}

	public void setVremeZavrsetka(LocalDateTime vremeZavrsetka) {
		this.vremeZavrsetka = vremeZavrsetka;
	}

	public RealizacijaPredmeta getRealizacija() {
		return realizacijaPredmeta;
	}

	public void setRealizacija(RealizacijaPredmeta realizacija) {
		this.realizacijaPredmeta = realizacija;
	}

	public RealizacijaPredmeta getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(RealizacijaPredmeta realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}

	public Integer getTipNastaveId() {
		return tipNastaveId;
	}

	public void setTipNastaveId(Integer tipNastaveId) {
		this.tipNastaveId = tipNastaveId;
	}

//	public TipNastave getTipNastave() {
//		return tipNastave;
//	}
//
//	public void setTipNastave(TipNastave tipNastave) {
//		this.tipNastave = tipNastave;
//	}
	
	
	
	
	


}
