package application.model.dto;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
@Entity
public class StudentNaGodini {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private LocalDateTime datumUpisa;
	@Column(nullable=false)
	private String brojIndeksa;
	@ManyToOne()
	private Student student;
	@OneToMany(mappedBy="studentNaGodini")
	private List<GodinaStudijaDTO> godineStudija=new ArrayList<GodinaStudijaDTO>();
	
	public StudentNaGodini(LocalDateTime datumUpisa, String brojIndeksa, ArrayList<GodinaStudijaDTO> godineStudija, Student student) {
		this.datumUpisa=datumUpisa;
		this.brojIndeksa=brojIndeksa;
		this.godineStudija=godineStudija;
		this.student=student;
	}

	public LocalDateTime getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(LocalDateTime datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public List<GodinaStudijaDTO> getGodineStudija() {
		return godineStudija;
	}

	public void setGodineStudija(ArrayList<GodinaStudijaDTO> godineStudija) {
		this.godineStudija = godineStudija;
	}

	
	

	
}
