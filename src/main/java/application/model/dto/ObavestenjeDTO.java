package application.model.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



public class ObavestenjeDTO {

    

    private Integer id;

    private String naslov;

    private String sadrzaj;

    private LocalDateTime vremePostavljanja;

    private List<FajlDTO> fajlovi=new ArrayList<FajlDTO>();
    
    

	public ObavestenjeDTO(Integer id, String naslov, String sadrzaj, LocalDateTime vremePostavljanja,ArrayList<FajlDTO> fajlovi) {
		super();
		this.id = id;
		this.naslov = naslov;
		this.sadrzaj = sadrzaj;
		this.vremePostavljanja = vremePostavljanja;
		this.fajlovi=fajlovi;
		
	}

	public ObavestenjeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getSadrzaj() {
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}

	public LocalDateTime getVremePostavljanja() {
		return vremePostavljanja;
	}

	public void setVremePostavljanja(LocalDateTime vremePostavljanja) {
		this.vremePostavljanja = vremePostavljanja;
	}

	public List<FajlDTO> getFajlovi() {
		return fajlovi;
	}

	public void setFajlovi(List<FajlDTO> fajlovi) {
		this.fajlovi = fajlovi;
	}




}