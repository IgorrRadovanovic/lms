package application.model.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;

import jakarta.persistence.Column;

public class UniverzitetDTO {
	public UniverzitetDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	private Integer id;
	

//	private AdresaDTO adresa;
	@Column
	private Integer adresaId;
	
	@Column
	private String naziv;
	
	@Column
	private LocalDateTime datumOsnivanja;
	

	private ArrayList<FakultetDTO> fakulteti = new ArrayList<>();
	

//	private NastavnikDTO rektor;
	@Column
	private Integer rektorId;

	public UniverzitetDTO(Integer id, /*AdresaDTO*/Integer adresaId, String naziv, LocalDateTime datumOsnivanja,
			ArrayList<FakultetDTO> fakulteti,/*NastavnikDTO*/Integer rektorId) {
		super();
		this.id = id;
		this.adresaId = adresaId;
		this.naziv = naziv;
		this.datumOsnivanja = datumOsnivanja;
		this.fakulteti = fakulteti;
		this.rektorId = rektorId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	public AdresaDTO getAdresa() {
//		return adresa;
//	}
//
//	public void setAdresa(AdresaDTO adresa) {
//		this.adresa = adresa;
//	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getDatumOsnivanja() {
		return datumOsnivanja;
	}

	public void setDatumOsnivanja(LocalDateTime datumOsnivanja) {
		this.datumOsnivanja = datumOsnivanja;
	}

	public ArrayList<FakultetDTO> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(ArrayList<FakultetDTO> fakulteti) {
		this.fakulteti = fakulteti;
	}

	public Integer getRektorId() {
		return rektorId;
	}

	public void setRektorId(Integer rektorId) {
		this.rektorId = rektorId;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

//	public NastavnikDTO getRektor() {
//		return rektor;
//	}
//
//	public void setRektor(NastavnikDTO rektor) {
//		this.rektor = rektor;
//	}

}