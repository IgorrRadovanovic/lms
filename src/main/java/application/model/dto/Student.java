package application.model.dto;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
@Entity
public class Student {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String jmbg;
	@Column(nullable=false)
	private String ime;
	@Column(nullable=false)
	private Object adresa;
	@OneToMany(mappedBy="student")
	private List<PohadjanjePredmeta> pohadjanjePredmeta=new ArrayList<PohadjanjePredmeta>();
	@ManyToOne()
	private StudentNaGodini studentNaGodini;
	
	public Student(String jmbg, String ime, Object adresa, ArrayList<PohadjanjePredmeta> pohadjanjePredmeta, StudentNaGodini studentNaGodini) {
		this.jmbg=jmbg;
		this.ime=ime;
		this.adresa=adresa;
		this.pohadjanjePredmeta=pohadjanjePredmeta;
		this.studentNaGodini=studentNaGodini;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Object getAdresa() {
		return adresa;
	}

	public void setAdresa(Object adresa) {
		this.adresa = adresa;
	}

	public List<PohadjanjePredmeta> getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(ArrayList<PohadjanjePredmeta> pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}
	
	
	
	
	
	

}
