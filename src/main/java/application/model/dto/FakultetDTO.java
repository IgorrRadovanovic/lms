package application.model.dto;

import java.util.ArrayList;
import java.util.List;

import application.model.StudijskiProgram;
import jakarta.persistence.Column;


public class FakultetDTO {
	

	private Integer id;
	

	private UniverzitetDTO univerzitet;
	

//	private AdresaDTO adresa;
	@Column
	private Integer adresaId;
	

//	private NastavnikDTO dekan;
	@Column
	private Integer dekanId;
	
	private List<StudijskiProgramDTO> studijski_programi=new ArrayList<>();
	
	public FakultetDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column
	private String naziv;
	
	public FakultetDTO(UniverzitetDTO univerzitet, String naziv, /* AdresaDTO */Integer adresaId,
			/* NastavnikDTO */ Integer dekanId,List<StudijskiProgramDTO> studijski_programi) {
		super();
		this.univerzitet = univerzitet;
		this.naziv = naziv;
		this.adresaId = adresaId;
		this.dekanId = dekanId;
		this.studijski_programi = studijski_programi;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

	public Integer getDekanId() {
		return dekanId;
	}

	public void setDekanId(Integer dekanId) {
		this.dekanId = dekanId;
	}

	public List<StudijskiProgramDTO> getStudijski_programi() {
		return studijski_programi;
	}

	public void setStudijski_programi(List<StudijskiProgramDTO> studijski_programi) {
		this.studijski_programi = studijski_programi;
	}

//	public AdresaDTO getAdresa() {
//		return adresa;
//	}
//
//	public void setAdresa(AdresaDTO adresa) {
//		this.adresa = adresa;
//	}
//
//	public NastavnikDTO getDekan() {
//		return dekan;
//	}
//
//	public void setDekan(NastavnikDTO dekan) {
//		this.dekan = dekan;
//	}

}
