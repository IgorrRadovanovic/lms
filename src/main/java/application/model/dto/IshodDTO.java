package application.model.dto;

import studentiApp.model.dto.EvaluacijaZnanjaDTO;

public class IshodDTO {//Tema
	

	private Integer id;

	private String naziv;
	
	private SilabusDTO silabus;
	//Mozda treba obrnuti vezu izmedju Ishoda i Obrazovnog cilja
	
	private ObrazovniCiljDTO obrazovniCilj;
	
	private TerminNastaveDTO terminNastave; // obrnuti vezu
	public IshodDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	private EvaluacijaZnanjaDTO evaluacijaZnanja; // ova veza se uklanja


	public IshodDTO(Integer id, String naziv, SilabusDTO silabus, ObrazovniCiljDTO obrazovniCilj, TerminNastaveDTO terminNastave,
			EvaluacijaZnanjaDTO evaluacijaZnanja) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.silabus = silabus;
		this.obrazovniCilj = obrazovniCilj;
		this.terminNastave = terminNastave;
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public SilabusDTO getSilabus() {
		return silabus;
	}

	public void setSilabus(SilabusDTO silabus) {
		this.silabus = silabus;
	}

	public ObrazovniCiljDTO getObrazovniCilj() {
		return obrazovniCilj;
	}

	public void setObrazovniCilj(ObrazovniCiljDTO obrazovniCilj) {
		this.obrazovniCilj = obrazovniCilj;
	}

	public TerminNastaveDTO getTerminNastave() {
		return terminNastave;
	}

	public void setTerminNastave(TerminNastaveDTO terminNastave) {
		this.terminNastave = terminNastave;
	}

	public EvaluacijaZnanjaDTO getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(EvaluacijaZnanjaDTO evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}

}
