package application.model.dto;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class GodinaStudijaDTO {

	

	private Integer id;

	private LocalDateTime godina;


	private List<StudijskiProgramDTO> studijskiProgrami = new ArrayList<>();

//	private List<StudentNaGodiniDTO> studentiNaGodini=new ArrayList<>();

	private List<PredmetDTO> predmeti=new ArrayList<PredmetDTO>();
	
	public GodinaStudijaDTO() {
		super();
	}

	public GodinaStudijaDTO(Integer id, LocalDateTime godina, List<StudijskiProgramDTO> studijskiProgram,
			/* List<StudentNaGodiniDTO> studentiNaGodini, */ List<PredmetDTO> predmeti) {
		super();
		this.id = id;
		this.godina = godina;
		this.studijskiProgrami = studijskiProgram;
		//this.studentiNaGodini = studentiNaGodini;
		this.predmeti = predmeti;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getGodina() {
		return godina;
	}

	public void setGodina(LocalDateTime godina) {
		this.godina = godina;
	}

//	public List<StudentNaGodiniDTO> getStudentiNaGodini() {
//		return studentiNaGodini;
//	}
//
//	public void setStudentiNaGodini(List<StudentNaGodiniDTO> studentiNaGodini) {
//		this.studentiNaGodini = studentiNaGodini;
//	}

	public List<PredmetDTO> getPredmeti() {
		return predmeti;
	}

	public void setPredmeti(List<PredmetDTO> predmeti) {
		this.predmeti = predmeti;
	}

	public List<StudijskiProgramDTO> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(List<StudijskiProgramDTO> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}

	
	
	
	

}
