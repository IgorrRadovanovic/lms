package application.model.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class PohadjanjePredmeta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private int konacnaOcena;
	@Column(nullable=false)
	private Object realizacijaPredmeta;
	
	public PohadjanjePredmeta(int konacnaOcena, Object realizacijaPredmeta) {
		super();
		this.konacnaOcena=konacnaOcena;
		this.realizacijaPredmeta=realizacijaPredmeta;
	}

	public int getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(int konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public Object getRealizacijaPredmeta() {
		return realizacijaPredmeta;
	}

	public void setRealizacijaPredmeta(Object realizacijaPredmeta) {
		this.realizacijaPredmeta = realizacijaPredmeta;
	}
	
	
	
}
