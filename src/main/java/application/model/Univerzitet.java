package application.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
//import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
@Entity
public class Univerzitet {
	public Univerzitet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
//	@ManyToOne(optional = true)
//	private Adresa adresa;
	@Column
	private Integer adresaId;
	
	@Column
	private String naziv;
	
	@Column
	private LocalDateTime datumOsnivanja;
	
	@OneToMany(mappedBy = "univerzitet")
	private ArrayList<Fakultet> fakulteti = new ArrayList<>();
	
//	@ManyToOne()
//	private Nastavnik rektor;
	@Column
	private Integer rektorId;

	public Univerzitet(Integer id, /*Adresa*/Integer adresaId, String naziv, LocalDateTime datumOsnivanja,
			ArrayList<Fakultet> fakulteti,/*Nastavnik*/Integer rektorId) {
		super();
		this.id = id;
		this.adresaId = adresaId;
		this.naziv = naziv;
		this.datumOsnivanja = datumOsnivanja;
		this.fakulteti = fakulteti;
		this.rektorId = rektorId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	public Adresa getAdresa() {
//		return adresa;
//	}
//
//	public void setAdresa(Adresa adresa) {
//		this.adresa = adresa;
//	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDateTime getDatumOsnivanja() {
		return datumOsnivanja;
	}

	public void setDatumOsnivanja(LocalDateTime datumOsnivanja) {
		this.datumOsnivanja = datumOsnivanja;
	}

	public ArrayList<Fakultet> getFakulteti() {
		return fakulteti;
	}

	public void setFakulteti(ArrayList<Fakultet> fakulteti) {
		this.fakulteti = fakulteti;
	}

	public Integer getRektorId() {
		return rektorId;
	}

	public void setRektorId(Integer rektorId) {
		this.rektorId = rektorId;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

//	public Nastavnik getRektor() {
//		return rektor;
//	}
//
//	public void setRektor(Nastavnik rektor) {
//		this.rektor = rektor;
//	}

}