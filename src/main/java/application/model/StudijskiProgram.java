package application.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;


@Entity
public class StudijskiProgram {

	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer id;
	 	@Column(nullable=false)
	 	private String naziv;
	 	
//	 	@ManyToOne
//	 	private Nastavnik rukovodilac;
	 	@Column
	 	private Integer rukovodilacId;
	 	@ManyToOne
	 	private GodinaStudija godinaStudija;
	 	@ManyToOne
	 	private Fakultet fakultet;

		public StudijskiProgram(Integer id, String naziv, /*Nastavnik*/Integer nastavnik,GodinaStudija g,Fakultet f) {
			super();
			this.id = id;
			this.naziv = naziv;
			this.rukovodilacId = nastavnik;
			this.godinaStudija = g;
			this.fakultet = f;
		}
		public StudijskiProgram() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getNaziv() {
			return naziv;
		}
		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}
//		public Nastavnik getRukovodilac() {
//			return rukovodilac;
//		}
//		public void setRukovodilac(Nastavnik nastavnik) {
//			this.rukovodilac = nastavnik;
//		}
		public GodinaStudija getGodinaStudija() {
			return godinaStudija;
		}
		public void setGodinaStudija(GodinaStudija godinaStudija) {
			this.godinaStudija = godinaStudija;
		}
		public Integer getRukovodilacId() {
			return rukovodilacId;
		}
		public void setRukovodilacId(Integer rukovodilacId) {
			this.rukovodilacId = rukovodilacId;
		}
		public Fakultet getFakultet() {
			return fakultet;
		}
		public void setFakultet(Fakultet fakultet) {
			this.fakultet = fakultet;
		}
		
		
	 	
	    
	    

}
