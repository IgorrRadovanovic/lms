package application.controller; 
 import application.model.RealizacijaPredmeta; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.RealizacijaPredmetaDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/realizacijePredmeta")
public class RealizacijaPredmetaController extends GenericController<RealizacijaPredmeta,RealizacijaPredmetaDTO,GenericService<RealizacijaPredmeta, Integer>>{
public RealizacijaPredmetaController(GenericService<RealizacijaPredmeta, Integer> service){
 super(service);
}}