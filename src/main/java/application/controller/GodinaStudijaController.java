package application.controller; 
 import application.model.GodinaStudija; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.GodinaStudijaDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/godinestudija")
public class GodinaStudijaController extends GenericController<GodinaStudija,GodinaStudijaDTO,GenericService<GodinaStudija, Integer>>{
public GodinaStudijaController(GenericService<GodinaStudija, Integer> service){
 super(service);
}}