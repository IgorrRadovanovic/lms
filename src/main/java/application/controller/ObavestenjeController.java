package application.controller; 
 import application.model.Obavestenje; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.ObavestenjeDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/obavestenja")
public class ObavestenjeController extends GenericController<Obavestenje,ObavestenjeDTO,GenericService<Obavestenje, Integer>>{
public ObavestenjeController(GenericService<Obavestenje, Integer> service){
 super(service);
}}