package application.controller; 
 import application.model.Univerzitet; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.UniverzitetDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/univerziteti")
public class UniverzitetController extends GenericController<Univerzitet,UniverzitetDTO,GenericService<Univerzitet, Integer>>{
public UniverzitetController(GenericService<Univerzitet, Integer> service){
 super(service);
}}