package application.controller; 
 import application.model.StudijskiProgram; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.StudijskiProgramDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/studijskiprogrami")
public class StudijskiProgramController extends GenericController<StudijskiProgram,StudijskiProgramDTO,GenericService<StudijskiProgram, Integer>>{
public StudijskiProgramController(GenericService<StudijskiProgram, Integer> service){
 super(service);
}}