package application.controller; 
 import application.model.Predmet; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.PredmetDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/predmeti")
public class PredmetController extends GenericController<Predmet,PredmetDTO,GenericService<Predmet, Integer>>{
public PredmetController(GenericService<Predmet, Integer> service){
 super(service);
}}