package application.controller; 
 import application.model.Fajl; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.FajlDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/fajlovi")
public class FajlController extends GenericController<Fajl,FajlDTO,GenericService<Fajl, Integer>>{
public FajlController(GenericService<Fajl, Integer> service){
 super(service);
}}