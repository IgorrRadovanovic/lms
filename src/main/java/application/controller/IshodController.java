package application.controller; 
 import application.model.Ishod; 
 import org.springframework.web.bind.annotation.RequestMapping;
import application.model.dto.IshodDTO; 
import org.springframework.stereotype.Controller; 
import application.service.GenericService;
@Controller 
@RequestMapping("/api/ishodi")
public class IshodController extends GenericController<Ishod,IshodDTO,GenericService<Ishod, Integer>>{
public IshodController(GenericService<Ishod, Integer> service){
 super(service);
}}