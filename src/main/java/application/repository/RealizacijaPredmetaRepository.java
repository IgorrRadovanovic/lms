package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.RealizacijaPredmeta; 
 public interface RealizacijaPredmetaRepository extends CrudRepository<RealizacijaPredmeta, Integer> {

}