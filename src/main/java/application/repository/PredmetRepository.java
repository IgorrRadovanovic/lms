package application.repository; 
 import org.springframework.data.repository.CrudRepository; 
 import application.model.Predmet; 
 public interface PredmetRepository extends CrudRepository<Predmet, Integer> {

}