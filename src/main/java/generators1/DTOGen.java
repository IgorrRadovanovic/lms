package generators1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DTOGen {

	public static void generateFile(String className, ArrayList<String> class2) throws IOException {


		String DTOPath = "src/main/java/application/model/dto" + System.getProperty("file.separator") + className
				+ "DTO.java";
		String classPath = "src/main/java/application/model/" + System.getProperty("file.separator") + className+".java";
		String content = new String(Files.readAllBytes(Paths.get(classPath)));
		content = content.replaceAll(".*@\\w+.*\\(.*\\).*", "");
		content = content.replaceAll("@Entity", "");
		content = content.replaceAll("@ManyToOne", "");
		content = content.replaceAll("@Id", "");
		content = content.replaceAll("application.model", "application.model.dto");
        for(String r : class2) {
        	if(r==className)
        		continue;
        content = content.replaceAll("\\b" + r + "\\b", r + "DTO");
        }
        content = content.replaceAll("\\b" + className + "\\b", className + "DTO");

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(DTOPath))) {
			writer.write(content);
			System.out.println("DTO for " + className + " has been generated.");
		} catch (IOException e) {
			System.err.println("An error occurred while writing to the file: " + e.getMessage());
		}
	}

	public static void main(String[] args) throws IOException {
		List<String> klase = new ArrayList<>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String className = "";
		ArrayList<String> class2 = new ArrayList<>();
		
		while (true) {
			System.out.print("Enter class2 name: ");
			String class2Name = reader.readLine();
			if (class2Name.equals("next")) {
				break;
			}else if(class2Name.equals("fromFile")) {

				try {
					List<String> cNames = Files.readAllLines(Paths.get("src/main/java/generators1/ClassNameList"));
					for(String cName:cNames) {
						class2.add(cName);
					}
					break;
				} catch (IOException e) {
					e.printStackTrace();
				}

			} 
			else {
				class2.add(class2Name);
			}
		}
		
		while (true) {
			System.out.print("Enter class name: ");
			className = reader.readLine();
			if (className.equals("make")) {
				break;
			}
			if (className.equals("")) {
				System.out.println("Enter class name!!");
			}else if(className.equals("fromFile")) {

				try {
					List<String> cNames = Files.readAllLines(Paths.get("src/main/java/generators1/ClassNameList"));
					for(String cName:cNames) {
						klase.add(cName);
					}
					break;
				} catch (IOException e) {
					e.printStackTrace();
				}

			} 
			else {
				klase.add(className);
			}
		}
		for (String r : klase) {
			generateFile(r,class2);
		}
	
	}
}
