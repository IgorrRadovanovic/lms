package nastavniciApp.model;


import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class NaucnaOblast {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String naziv;
	@OneToMany(mappedBy="oblast")
	private List<Zvanje> lista_zvanja=new ArrayList<Zvanje>();
	public NaucnaOblast() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NaucnaOblast(Integer id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public List<Zvanje> getLista_zvanja() {
		return lista_zvanja;
	}
	public void setLista_zvanja(List<Zvanje> lista_zvanja) {
		this.lista_zvanja = lista_zvanja;
	}

	

}
