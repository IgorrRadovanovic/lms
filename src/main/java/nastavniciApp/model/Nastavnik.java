package nastavniciApp.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
//import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Nastavnik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	private String ime;
	@Column(nullable = false)
	private String biografija;
	@Column(nullable = false)
	private String jmbg;
	@OneToMany(mappedBy = "nosilac_zvanja")
	private List<Zvanje> zvanja = new ArrayList<Zvanje>();
	@OneToMany(mappedBy = "nastavnik")
	private List<NastavnikNaRealizaciji> realizacije = new ArrayList<NastavnikNaRealizaciji>();
//	@ManyToOne
//	private Adresa adresa;
	@Column
	private Integer adresaId;
	
//	@OneToMany(mappedBy = "rektor")
//	private List<Univerzitet> univerziteti = new ArrayList<>();;
//	@OneToMany(mappedBy = "dekan")
//	private List<Fakultet> fakulteti = new ArrayList<>();;
//	@OneToMany(mappedBy = "rukovodilac")
//	private List<StudijskiProgram> studijskiProgrami = new ArrayList<>();;

	public Nastavnik(Integer id, String ime, String biografija, String jmbg, List<Zvanje> zvanja,
			List<NastavnikNaRealizaciji> realizacije,
			/*Adresa*/Integer adresaId//, /* List<Univerzitet> univerziteti, */
			/* List<Fakultet> fakulteti, */ /*List<StudijskiProgram> studijskiProgrami*/) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
		this.zvanja = zvanja;
		this.realizacije = realizacije;
		this.adresaId = adresaId;
//		this.univerziteti = univerziteti;
//		this.fakulteti = fakulteti;
//		this.studijskiProgrami = studijskiProgrami;
	}



	public Nastavnik() {
		super();
	}

	

	public Nastavnik(Integer id, String ime, String biografija, String jmbg) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getBiografija() {
		return biografija;
	}

	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public List<Zvanje> getZvanja() {
		return zvanja;
	}

	public void setZvanja(List<Zvanje> zvanja) {
		this.zvanja = zvanja;
	}

	public List<NastavnikNaRealizaciji> getRealizacije() {
		return realizacije;
	}

	public void setRealizacije(List<NastavnikNaRealizaciji> realizacije) {
		this.realizacije = realizacije;
	}

	public Integer getId() {
		return id;
	}



//	public Adresa getAdresa() {
//		return adresa;
//	}
//
//
//
//	public void setAdresa(Adresa adresa) {
//		this.adresa = adresa;
//	}



//	public List<Univerzitet> getUniverziteti() {
//		return univerziteti;
//	}
//
//
//
//	public void setUniverziteti(List<Univerzitet> univerziteti) {
//		this.univerziteti = univerziteti;
//	}



//	public List<Fakultet> getFakulteti() {
//		return fakulteti;
//	}
//
//
//
//	public void setFakulteti(List<Fakultet> fakulteti) {
//		this.fakulteti = fakulteti;
//	}



//	public List<StudijskiProgram> getStudijskiProgrami() {
//		return studijskiProgrami;
//	}
//
//
//
//	public void setStudijskiProgrami(List<StudijskiProgram> studijskiProgrami) {
//		this.studijskiProgrami = studijskiProgrami;
//	}



	public void setId(Integer id) {
		this.id = id;
	}



	public Integer getAdresaId() {
		return adresaId;
	}



	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

}
