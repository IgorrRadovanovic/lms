package nastavniciApp.model.dto;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;


public class NastavnikDTO {
	

	private Integer id;

	private String ime;

	private String biografija;

	private String jmbg;

	private List<ZvanjeDTO> zvanja = new ArrayList<ZvanjeDTO>();

	private List<NastavnikNaRealizacijiDTO> realizacije = new ArrayList<NastavnikNaRealizacijiDTO>();
//	
//	private AdresaDTO adresa;
	@Column
	private Integer adresaId;
	

//	private List<UniverzitetDTO> univerziteti = new ArrayList<>();;

//	private List<FakultetDTO> fakulteti = new ArrayList<>();;

//	private List<StudijskiProgramDTO> studijskiProgrami = new ArrayList<>();;

	public NastavnikDTO(Integer id, String ime, String biografija, String jmbg, List<ZvanjeDTO> zvanja,
			List<NastavnikNaRealizacijiDTO> realizacije,
			/*AdresaDTO*/Integer adresaId//, /* List<UniverzitetDTO> univerziteti, */
			/* List<FakultetDTO> fakulteti, */ /*List<StudijskiProgramDTO> studijskiProgrami*/) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
		this.zvanja = zvanja;
		this.realizacije = realizacije;
		this.adresaId = adresaId;
//		this.univerziteti = univerziteti;
//		this.fakulteti = fakulteti;
//		this.studijskiProgrami = studijskiProgrami;
	}



	public NastavnikDTO() {
		super();
	}

	

	public NastavnikDTO(Integer id, String ime, String biografija, String jmbg) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getBiografija() {
		return biografija;
	}

	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public List<ZvanjeDTO> getZvanja() {
		return zvanja;
	}

	public void setZvanja(List<ZvanjeDTO> zvanja) {
		this.zvanja = zvanja;
	}

	public List<NastavnikNaRealizacijiDTO> getRealizacije() {
		return realizacije;
	}

	public void setRealizacije(List<NastavnikNaRealizacijiDTO> realizacije) {
		this.realizacije = realizacije;
	}

	public Integer getId() {
		return id;
	}



//	public AdresaDTO getAdresa() {
//		return adresa;
//	}
//
//
//
//	public void setAdresa(AdresaDTO adresa) {
//		this.adresa = adresa;
//	}



//	public List<UniverzitetDTO> getUniverziteti() {
//		return univerziteti;
//	}
//
//
//
//	public void setUniverziteti(List<UniverzitetDTO> univerziteti) {
//		this.univerziteti = univerziteti;
//	}



//	public List<FakultetDTO> getFakulteti() {
//		return fakulteti;
//	}
//
//
//
//	public void setFakulteti(List<FakultetDTO> fakulteti) {
//		this.fakulteti = fakulteti;
//	}



//	public List<StudijskiProgramDTO> getStudijskiProgrami() {
//		return studijskiProgrami;
//	}
//
//
//
//	public void setStudijskiProgrami(List<StudijskiProgramDTO> studijskiProgrami) {
//		this.studijskiProgrami = studijskiProgrami;
//	}



	public void setId(Integer id) {
		this.id = id;
	}



	public Integer getAdresaId() {
		return adresaId;
	}



	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}

}
