package nastavniciApp.model.dto;


import java.util.ArrayList;
import java.util.List;


public class NaucnaOblastDTO {
	

	private Integer id;

	private String naziv;

	private List<ZvanjeDTO> lista_zvanja=new ArrayList<ZvanjeDTO>();
	public NaucnaOblastDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NaucnaOblastDTO(Integer id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public List<ZvanjeDTO> getLista_zvanja() {
		return lista_zvanja;
	}
	public void setLista_zvanja(List<ZvanjeDTO> lista_zvanja) {
		this.lista_zvanja = lista_zvanja;
	}

	

}
