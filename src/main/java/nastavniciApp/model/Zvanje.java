package nastavniciApp.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Zvanje {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	private NaucnaOblast oblast;
	@ManyToOne
	private TipZvanja tip;
	@Column(nullable=false)
	private String datum_izbora;
	@Column(nullable=true)
	private String datum_isteka;
	@ManyToOne
	private Nastavnik nosilac_zvanja;
	public Zvanje() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Zvanje(Integer id,NaucnaOblast oblast, TipZvanja tip, String datum_izbora, String datum_isteka) {
		super();
		this.id = id;
		this.oblast = oblast;
		this.tip = tip;
		this.datum_izbora = datum_izbora;
		this.datum_isteka = datum_isteka;
	}
	public NaucnaOblast getOblast() {
		return oblast;
	}
	public void setOblast(NaucnaOblast oblast) {
		this.oblast = oblast;
	}
	public TipZvanja getTip() {
		return tip;
	}
	public void setTip(TipZvanja tip) {
		this.tip = tip;
	}
	public String getDatum_izbora() {
		return datum_izbora;
	}
	public void setDatum_izbora(String datum_izbora) {
		this.datum_izbora = datum_izbora;
	}
	public String getDatum_isteka() {
		return datum_isteka;
	}
	public void setDatum_isteka(String datum_isteka) {
		this.datum_isteka = datum_isteka;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Nastavnik getNosilac_zvanja() {
		return nosilac_zvanja;
	}
	public void setNosilac_zvanja(Nastavnik nosilac_zvanja) {
		this.nosilac_zvanja = nosilac_zvanja;
	}

}
