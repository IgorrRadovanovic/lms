package nastavniciApp.model;



import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class TipNastave {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String naziv;
	@OneToMany(mappedBy="tipNastave")
	private List<NastavnikNaRealizaciji> realizacije = new ArrayList<NastavnikNaRealizaciji>();

	public TipNastave() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipNastave(Integer id,String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<NastavnikNaRealizaciji> getRealizacije() {
		return realizacije;
	}

	public void setRealizacije(List<NastavnikNaRealizaciji> realizacije) {
		this.realizacije = realizacije;
	}
	
}
