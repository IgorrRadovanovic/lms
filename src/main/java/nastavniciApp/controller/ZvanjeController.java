package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import nastavniciApp.model.Zvanje;
import nastavniciApp.model.dto.ZvanjeDTO;
import nastavniciApp.service.GenericService;
@Controller 
@RequestMapping("/api/zvanja")
public class ZvanjeController extends GenericController<Zvanje,ZvanjeDTO,GenericService<Zvanje, Integer>>{
public ZvanjeController(GenericService<Zvanje, Integer> service){
 super(service);
}}