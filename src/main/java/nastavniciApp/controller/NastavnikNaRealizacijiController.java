package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import nastavniciApp.model.NastavnikNaRealizaciji;
import nastavniciApp.model.dto.NastavnikNaRealizacijiDTO;
import nastavniciApp.service.GenericService;
@Controller 
@RequestMapping("/api/nastavnicinarealizaciji")
public class NastavnikNaRealizacijiController extends GenericController<NastavnikNaRealizaciji,NastavnikNaRealizacijiDTO,GenericService<NastavnikNaRealizaciji, Integer>>{
public NastavnikNaRealizacijiController(GenericService<NastavnikNaRealizaciji, Integer> service){
 super(service);
}}