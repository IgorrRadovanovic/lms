package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import nastavniciApp.model.NaucnaOblast;
import nastavniciApp.model.dto.NaucnaOblastDTO;
import nastavniciApp.service.GenericService;
@Controller 
@RequestMapping("/api/naucneoblasti")
public class NaucnaOblastController extends GenericController<NaucnaOblast,NaucnaOblastDTO,GenericService<NaucnaOblast, Integer>>{
public NaucnaOblastController(GenericService<NaucnaOblast, Integer> service){
 super(service);
}}