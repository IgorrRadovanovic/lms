package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import nastavniciApp.model.TipZvanja;
import nastavniciApp.model.dto.TipZvanjaDTO;
import nastavniciApp.service.GenericService;
@Controller 
@RequestMapping("/api/tipovizvanja")
public class TipZvanjaController extends GenericController<TipZvanja,TipZvanjaDTO,GenericService<TipZvanja, Integer>>{
public TipZvanjaController(GenericService<TipZvanja, Integer> service){
 super(service);
}}