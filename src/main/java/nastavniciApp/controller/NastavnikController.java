package nastavniciApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import nastavniciApp.model.Nastavnik;
import nastavniciApp.model.dto.NastavnikDTO;
import nastavniciApp.service.GenericService;
@Controller 
@RequestMapping("/api/nastavnici")
public class NastavnikController extends GenericController<Nastavnik,NastavnikDTO,GenericService<Nastavnik, Integer>>{
public NastavnikController(GenericService<Nastavnik, Integer> service){
 super(service);
}}