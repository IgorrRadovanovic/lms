package nastavniciApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import nastavniciApp.model.NastavnikNaRealizaciji; 
 public interface NastavnikNaRealizacijiRepository extends CrudRepository<NastavnikNaRealizaciji, Integer> {

}