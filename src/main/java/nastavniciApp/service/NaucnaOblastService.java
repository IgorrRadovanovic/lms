package nastavniciApp.service; 
import org.springframework.stereotype.Service;

import nastavniciApp.model.NaucnaOblast;
import nastavniciApp.repository.NaucnaOblastRepository;  
@Service
public class NaucnaOblastService extends GenericService<NaucnaOblast, Integer> {
public NaucnaOblastService(NaucnaOblastRepository repository){super(repository);
}
}