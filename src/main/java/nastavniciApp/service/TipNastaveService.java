package nastavniciApp.service; 
import org.springframework.stereotype.Service;

import nastavniciApp.model.TipNastave;
import nastavniciApp.repository.TipNastaveRepository;  
@Service
public class TipNastaveService extends GenericService<TipNastave, Integer> {
public TipNastaveService(TipNastaveRepository repository){super(repository);
}
}