package nastavniciApp.service; 
import org.springframework.stereotype.Service;

import nastavniciApp.model.Nastavnik;
import nastavniciApp.repository.NastavnikRepository;  
@Service
public class NastavnikService extends GenericService<Nastavnik, Integer> {
public NastavnikService(NastavnikRepository repository){super(repository);
}
}