package nastavniciApp.service; 
import org.springframework.stereotype.Service;

import nastavniciApp.model.NastavnikNaRealizaciji;
import nastavniciApp.repository.NastavnikNaRealizacijiRepository;  
@Service
public class NastavnikNaRealizacijiService extends GenericService<NastavnikNaRealizaciji, Integer> {
public NastavnikNaRealizacijiService(NastavnikNaRealizacijiRepository repository){super(repository);
}
}