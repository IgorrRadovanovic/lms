package nastavniciApp.service; 
import org.springframework.stereotype.Service;

import nastavniciApp.model.TipZvanja;
import nastavniciApp.repository.TipZvanjaRepository;  
@Service
public class TipZvanjaService extends GenericService<TipZvanja, Integer> {
public TipZvanjaService(TipZvanjaRepository repository){super(repository);
}
}