package nastavniciApp.service; 
import org.springframework.stereotype.Service;

import nastavniciApp.model.Zvanje;
import nastavniciApp.repository.ZvanjeRepository;  
@Service
public class ZvanjeService extends GenericService<Zvanje, Integer> {
public ZvanjeService(ZvanjeRepository repository){super(repository);
}
}