package studentiApp.model;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Polaganje {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	@Column(nullable=false)
	private int bodovi;
	@Column(nullable=false)
	private String napomena;
	@ManyToOne
	private StudentNaGodini studentNaGodini;
	@ManyToOne
	private EvaluacijaZnanja evaluacijaZnanja;
	
	
	public Polaganje() {
		super();
    }
	
	public Polaganje(Integer id, int bodovi, String napomena,StudentNaGodini studentNaGodini, EvaluacijaZnanja evaluacijaZnanja) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.napomena = napomena;
		this.studentNaGodini=studentNaGodini;
		this.evaluacijaZnanja=evaluacijaZnanja;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getBodovi() {
		return bodovi;
	}
	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}
	public String getNapomena() {
		return napomena;
	}
	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}
	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}
	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}
	
	
	
	
}
