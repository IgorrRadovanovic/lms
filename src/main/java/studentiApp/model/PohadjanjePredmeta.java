package studentiApp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class PohadjanjePredmeta {

	public PohadjanjePredmeta() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private int konacnaOcena;
	
	
	@ManyToOne 
    @JoinColumn(name = "studentNaGodini_id", nullable = false)
    private StudentNaGodini studentNaGodini;

//    @ManyToOne 
//    @JoinColumn(name = "realizacijaPredmeta_id", nullable = false)
	@Column()
    private /*RealizacijaPredmeta*/Integer realizacijaPredmetaId;
	
	public PohadjanjePredmeta(Integer id,int konacnaOcena,StudentNaGodini studentNaGodini, /*RealizacijaPredmeta*/Integer realizacijaPredmeta) {
		super();
		this.id = id;
		this.konacnaOcena=konacnaOcena;
		this.studentNaGodini = studentNaGodini;
		this.realizacijaPredmetaId=realizacijaPredmeta;
	}

	public int getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(int konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public /*RealizacijaPredmeta*/Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(/*RealizacijaPredmeta*/Integer realizacijaPredmeta) {
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentNaGodini getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodini studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}
	
	
	
}
