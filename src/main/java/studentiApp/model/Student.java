package studentiApp.model;



import java.util.ArrayList;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
//import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
@Entity
public class Student {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String jmbg;
	@Column(nullable=false)
	private String ime;
//	@ManyToOne
//	private Adresa adresa;
	@Column
	private Integer adresaId;
	@OneToMany(mappedBy = "student")
	private ArrayList<StudentNaGodini> studentNaGodinama = new ArrayList<>();
	
	public Student(Integer id,String jmbg, String ime, /*Adresa*/Integer adresaId,  ArrayList<StudentNaGodini> studentNaGodini) {
		this.id = id;
		this.jmbg=jmbg;
		this.ime=ime;
		this.adresaId=adresaId;
		this.studentNaGodinama=studentNaGodini;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

//	public Adresa getAdresa() {
//		return adresa;
//	}
//
//	public void setAdresa(Adresa adresa) {
//		this.adresa = adresa;
//	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArrayList<StudentNaGodini> getStudentNaGodinama() {
		return studentNaGodinama;
	}

	public void setStudentNaGodinama(ArrayList<StudentNaGodini> studentNaGodinama) {
		this.studentNaGodinama = studentNaGodinama;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}
	
	
	
	
	
	

}
