package studentiApp.model.dto;



import java.util.ArrayList;

import jakarta.persistence.Column;

public class StudentDTO {
	

	private Integer id;

	private String jmbg;

	private String ime;
//	
//	private AdresaDTO adresa;
	@Column
	private Integer adresaId;

	private ArrayList<StudentNaGodiniDTO> studentNaGodinama = new ArrayList<>();
	
	public StudentDTO(Integer id,String jmbg, String ime, /*AdresaDTO*/Integer adresaId,  ArrayList<StudentNaGodiniDTO> studentNaGodini) {
		this.id = id;
		this.jmbg=jmbg;
		this.ime=ime;
		this.adresaId=adresaId;
		this.studentNaGodinama=studentNaGodini;
	}

	public StudentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

//	public AdresaDTO getAdresa() {
//		return adresa;
//	}
//
//	public void setAdresa(AdresaDTO adresa) {
//		this.adresa = adresa;
//	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ArrayList<StudentNaGodiniDTO> getStudentNaGodinama() {
		return studentNaGodinama;
	}

	public void setStudentNaGodinama(ArrayList<StudentNaGodiniDTO> studentNaGodinama) {
		this.studentNaGodinama = studentNaGodinama;
	}

	public Integer getAdresaId() {
		return adresaId;
	}

	public void setAdresaId(Integer adresaId) {
		this.adresaId = adresaId;
	}
	
	
	
	
	
	

}
