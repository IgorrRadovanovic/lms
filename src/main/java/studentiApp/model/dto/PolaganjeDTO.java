package studentiApp.model.dto;

public class PolaganjeDTO {
	

    private Integer id;

	private int bodovi;

	private String napomena;
	
	private StudentNaGodiniDTO studentNaGodini;
	
	private EvaluacijaZnanjaDTO evaluacijaZnanja;
	
	
	public PolaganjeDTO() {
		super();
    }
	
	public PolaganjeDTO(Integer id, int bodovi, String napomena,StudentNaGodiniDTO studentNaGodini, EvaluacijaZnanjaDTO evaluacijaZnanja) {
		super();
		this.id = id;
		this.bodovi = bodovi;
		this.napomena = napomena;
		this.studentNaGodini=studentNaGodini;
		this.setEvaluacijaZnanja(evaluacijaZnanja);
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getBodovi() {
		return bodovi;
	}
	public void setBodovi(int bodovi) {
		this.bodovi = bodovi;
	}
	public String getNapomena() {
		return napomena;
	}
	public void setNapomena(String napomena) {
		this.napomena = napomena;
	}
	public StudentNaGodiniDTO getStudentNaGodini() {
		return studentNaGodini;
	}
	public void setStudentNaGodini(StudentNaGodiniDTO studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}

	public EvaluacijaZnanjaDTO getEvaluacijaZnanja() {
		return evaluacijaZnanja;
	}

	public void setEvaluacijaZnanja(EvaluacijaZnanjaDTO evaluacijaZnanja) {
		this.evaluacijaZnanja = evaluacijaZnanja;
	}
	
	
	
	
}
