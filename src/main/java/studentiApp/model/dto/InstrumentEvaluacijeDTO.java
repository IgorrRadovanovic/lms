package studentiApp.model.dto;

import java.util.ArrayList;

import jakarta.persistence.Column;


public class InstrumentEvaluacijeDTO {
	

	private Integer id;
	
	@Column
	private String naziv;
	

	private ArrayList<EvaluacijaZnanjaDTO> evaluacije = new ArrayList<>();

	public InstrumentEvaluacijeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InstrumentEvaluacijeDTO(Integer id, String naziv, ArrayList<EvaluacijaZnanjaDTO> evaluacije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.evaluacije = evaluacije;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<EvaluacijaZnanjaDTO> getEvaluacije() {
		return evaluacije;
	}

	public void setEvaluacije(ArrayList<EvaluacijaZnanjaDTO> evaluacije) {
		this.evaluacije = evaluacije;
	}
	
	
}