package studentiApp.model.dto;

public class PohadjanjePredmetaDTO {

	public PohadjanjePredmetaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	private Integer id;

	private int konacnaOcena;
	
	
	 

    private StudentNaGodiniDTO studentNaGodini;

//     


    private /*RealizacijaPredmetaDTO*/Integer realizacijaPredmetaId;
	
	public PohadjanjePredmetaDTO(Integer id,int konacnaOcena,StudentNaGodiniDTO studentNaGodini, /*RealizacijaPredmetaDTO*/Integer realizacijaPredmeta) {
		super();
		this.id = id;
		this.konacnaOcena=konacnaOcena;
		this.studentNaGodini = studentNaGodini;
		this.realizacijaPredmetaId=realizacijaPredmeta;
	}

	public int getKonacnaOcena() {
		return konacnaOcena;
	}

	public void setKonacnaOcena(int konacnaOcena) {
		this.konacnaOcena = konacnaOcena;
	}

	public /*RealizacijaPredmetaDTO*/Integer getRealizacijaPredmetaId() {
		return realizacijaPredmetaId;
	}

	public void setRealizacijaPredmetaId(/*RealizacijaPredmetaDTO*/Integer realizacijaPredmeta) {
		this.realizacijaPredmetaId = realizacijaPredmeta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentNaGodiniDTO getStudentNaGodini() {
		return studentNaGodini;
	}

	public void setStudentNaGodini(StudentNaGodiniDTO studentNaGodini) {
		this.studentNaGodini = studentNaGodini;
	}
	
	
	
}
