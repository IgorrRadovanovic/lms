package studentiApp.model.dto;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;

public class StudentNaGodiniDTO {
	public StudentNaGodiniDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	private Integer id;

	private LocalDateTime datumUpisa;

	private String brojIndeksa;

	private List<PohadjanjePredmetaDTO> pohadjanjePredmeta=new ArrayList<PohadjanjePredmetaDTO>();//ok

//	private List<GodinaStudijaDTO> godineStudija=new ArrayList<GodinaStudijaDTO>();
	@Column
	private Integer godinaStudijaId;
	@OneToMany
	private List<PolaganjeDTO> polaganja=new ArrayList<PolaganjeDTO>();//ok
	
	private StudentDTO student;
	
	public StudentNaGodiniDTO(LocalDateTime datumUpisa, String brojIndeksa, ArrayList<PohadjanjePredmetaDTO> pohadjanjePredmeta,/*ArrayList<GodinaStudijaDTO>*/Integer godinaStudijaId, ArrayList<PolaganjeDTO> polaganja) {
		this.datumUpisa=datumUpisa;
		this.brojIndeksa=brojIndeksa;
		this.godinaStudijaId=godinaStudijaId;
		this.pohadjanjePredmeta=pohadjanjePredmeta;
		this.polaganja=polaganja;
	}

	public LocalDateTime getDatumUpisa() {
		return datumUpisa;
	}

	public void setDatumUpisa(LocalDateTime datumUpisa) {
		this.datumUpisa = datumUpisa;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	

	public List<PohadjanjePredmetaDTO> getPohadjanjePredmeta() {
		return pohadjanjePredmeta;
	}

	public void setPohadjanjePredmeta(List<PohadjanjePredmetaDTO> pohadjanjePredmeta) {
		this.pohadjanjePredmeta = pohadjanjePredmeta;
	}

//	public List<GodinaStudijaDTO> getGodineStudija() {
//		return godineStudija;
//	}
//
//	public void setGodineStudija(ArrayList<GodinaStudijaDTO> godineStudija) {
//		this.godineStudija = godineStudija;
//	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<PolaganjeDTO> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(List<PolaganjeDTO> polaganja) {
		this.polaganja = polaganja;
	}

	public Integer getGodinaStudijaId() {
		return godinaStudijaId;
	}

	public void setGodinaStudijaId(Integer godinaStudijaId) {
		this.godinaStudijaId = godinaStudijaId;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

//	public void setGodineStudija(List<GodinaStudijaDTO> godineStudija) {
//		this.godineStudija = godineStudija;
//	}

	
	

	
}
