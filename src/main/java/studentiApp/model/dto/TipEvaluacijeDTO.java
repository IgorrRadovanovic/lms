package studentiApp.model.dto;

import java.util.ArrayList;
import java.util.List;


public class TipEvaluacijeDTO {
	

	private Integer id;
	

	private String naziv;
	

	private List<EvaluacijaZnanjaDTO> evaluacije = new ArrayList<>();

	public TipEvaluacijeDTO(Integer id,String tipEvaluacije) {
		super();
		this.id = id;
		this.naziv = tipEvaluacije;
	}

	public TipEvaluacijeDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<EvaluacijaZnanjaDTO> getEvaluacije() {
		return evaluacije;
	}

	public void setEvaluacije(List<EvaluacijaZnanjaDTO> evaluacije) {
		this.evaluacije = evaluacije;
	}

}