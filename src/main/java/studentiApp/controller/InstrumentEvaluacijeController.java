package studentiApp.controller;

import studentiApp.model.InstrumentEvaluacije;
import org.springframework.web.bind.annotation.RequestMapping;
import studentiApp.model.dto.InstrumentEvaluacijeDTO;
import org.springframework.stereotype.Controller;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/instrumentievaluacije")
public class InstrumentEvaluacijeController extends
		GenericController<InstrumentEvaluacije, InstrumentEvaluacijeDTO, GenericService<InstrumentEvaluacije, Integer>> {
	public InstrumentEvaluacijeController(GenericService<InstrumentEvaluacije, Integer> service) {
		super(service);
	}
}