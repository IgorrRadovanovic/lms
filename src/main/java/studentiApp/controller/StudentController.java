package studentiApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.Student;
import studentiApp.model.dto.StudentDTO;
import studentiApp.service.GenericService;
@Controller 
@RequestMapping("/api/studenti")
public class StudentController extends GenericController<Student,StudentDTO,GenericService<Student, Integer>>{
public StudentController(GenericService<Student, Integer> service){
 super(service);
}}