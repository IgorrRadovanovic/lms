package studentiApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.TipEvaluacije;
import studentiApp.model.dto.TipEvaluacijeDTO;
import studentiApp.service.GenericService;
@Controller 
@RequestMapping("/api/tipovievaluacije")
public class TipEvaluacijeController extends GenericController<TipEvaluacije,TipEvaluacijeDTO,GenericService<TipEvaluacije, Integer>>{
public TipEvaluacijeController(GenericService<TipEvaluacije, Integer> service){
 super(service);
}}