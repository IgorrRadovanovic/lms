package studentiApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.EvaluacijaZnanja;
import studentiApp.model.dto.EvaluacijaZnanjaDTO;
import studentiApp.service.GenericService;

@Controller
@RequestMapping("/api/evaluacijeznanja")
public class EvaluacijaZnanjaController
		extends GenericController<EvaluacijaZnanja, EvaluacijaZnanjaDTO, GenericService<EvaluacijaZnanja, Integer>> {
	public EvaluacijaZnanjaController(GenericService<EvaluacijaZnanja, Integer> service) {
		super(service);
	}
}