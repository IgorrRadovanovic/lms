package studentiApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.Polaganje;
import studentiApp.model.dto.PolaganjeDTO;
import studentiApp.service.GenericService;
@Controller 
@RequestMapping("/api/polaganja")
public class PolaganjeController extends GenericController<Polaganje,PolaganjeDTO,GenericService<Polaganje, Integer>>{
public PolaganjeController(GenericService<Polaganje, Integer> service){
 super(service);
}}