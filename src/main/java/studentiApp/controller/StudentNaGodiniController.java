package studentiApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.StudentNaGodini;
import studentiApp.model.dto.StudentNaGodiniDTO;
import studentiApp.service.GenericService;
@Controller 
@RequestMapping("/api/studentinagodini")
public class StudentNaGodiniController extends GenericController<StudentNaGodini,StudentNaGodiniDTO,GenericService<StudentNaGodini, Integer>>{
public StudentNaGodiniController(GenericService<StudentNaGodini, Integer> service){
 super(service);
}}