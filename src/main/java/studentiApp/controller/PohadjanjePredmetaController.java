package studentiApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import studentiApp.model.PohadjanjePredmeta;
import studentiApp.model.dto.PohadjanjePredmetaDTO;
import studentiApp.service.GenericService;
@Controller 
@RequestMapping("/api/pohadjanjapredmeta")
public class PohadjanjePredmetaController extends GenericController<PohadjanjePredmeta,PohadjanjePredmetaDTO,GenericService<PohadjanjePredmeta, Integer>>{
public PohadjanjePredmetaController(GenericService<PohadjanjePredmeta, Integer> service){
 super(service);
}}