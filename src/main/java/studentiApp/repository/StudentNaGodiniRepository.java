package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.StudentNaGodini; 
 public interface StudentNaGodiniRepository extends CrudRepository<StudentNaGodini, Integer> {

}