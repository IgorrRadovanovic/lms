package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.Polaganje; 
 public interface PolaganjeRepository extends CrudRepository<Polaganje, Integer> {

}