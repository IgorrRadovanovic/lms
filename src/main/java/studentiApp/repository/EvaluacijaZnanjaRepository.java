package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.EvaluacijaZnanja; 
 public interface EvaluacijaZnanjaRepository extends CrudRepository<EvaluacijaZnanja, Integer> {

}