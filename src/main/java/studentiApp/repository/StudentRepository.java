package studentiApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import studentiApp.model.Student; 
 public interface StudentRepository extends CrudRepository<Student, Integer> {

}