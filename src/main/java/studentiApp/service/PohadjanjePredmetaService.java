package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.PohadjanjePredmeta;
import studentiApp.repository.PohadjanjePredmetaRepository;  
@Service
public class PohadjanjePredmetaService extends GenericService<PohadjanjePredmeta, Integer> {
public PohadjanjePredmetaService(PohadjanjePredmetaRepository repository){super(repository);
}
}