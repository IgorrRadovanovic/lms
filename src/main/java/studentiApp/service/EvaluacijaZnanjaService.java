package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.EvaluacijaZnanja;
import studentiApp.repository.EvaluacijaZnanjaRepository;  
@Service
public class EvaluacijaZnanjaService extends GenericService<EvaluacijaZnanja, Integer> {
public EvaluacijaZnanjaService(EvaluacijaZnanjaRepository repository){super(repository);
}
}