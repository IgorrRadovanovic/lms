package studentiApp.service; 
import org.springframework.stereotype.Service;

import studentiApp.model.Polaganje;
import studentiApp.repository.PolaganjeRepository;  
@Service
public class PolaganjeService extends GenericService<Polaganje, Integer> {
public PolaganjeService(PolaganjeRepository repository){super(repository);
}
}