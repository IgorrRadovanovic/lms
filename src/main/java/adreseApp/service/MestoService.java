package adreseApp.service; 
import org.springframework.stereotype.Service;

import adreseApp.model.Mesto;
import adreseApp.repository.MestoRepository;  
@Service
public class MestoService extends GenericService<Mesto, Integer> {
public MestoService(MestoRepository repository){super(repository);
}
}