package adreseApp.model.dto;

import java.util.ArrayList;
import java.util.List;


public class DrzavaDTO implements DTOObject{
	public DrzavaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	private Integer id;


	private String naziv;
	

	private List<MestoDTO> mesta = new ArrayList<>();
			
	public DrzavaDTO(Integer id, String naziv, List<MestoDTO> mesta) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.mesta = mesta;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<MestoDTO> getMesta() {
		return mesta;
	}

	public void setMesta(List<MestoDTO> mesta) {
		this.mesta = mesta;
	}

}
