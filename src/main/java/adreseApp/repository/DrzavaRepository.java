package adreseApp.repository; 
 import org.springframework.data.repository.CrudRepository;

import adreseApp.model.Drzava; 
 public interface DrzavaRepository extends CrudRepository<Drzava, Integer> {

}