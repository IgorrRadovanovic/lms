package adreseApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import adreseApp.model.Adresa;
import adreseApp.model.dto.AdresaDTO;
import adreseApp.service.GenericService;
@Controller 
@RequestMapping("/api/adrese")
public class AdresaController extends GenericController<Adresa,AdresaDTO,GenericService<Adresa, Integer>>{
public AdresaController(GenericService<Adresa, Integer> service){
 super(service);
}}