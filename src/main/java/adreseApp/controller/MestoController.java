package adreseApp.controller; 
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import adreseApp.model.Mesto;
import adreseApp.model.dto.MestoDTO;
import adreseApp.service.GenericService;
@Controller 
@RequestMapping("/api/mesta")
public class MestoController extends GenericController<Mesto,MestoDTO,GenericService<Mesto, Integer>>{
public MestoController(GenericService<Mesto, Integer> service){
 super(service);
}}