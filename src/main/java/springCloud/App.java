package springCloud;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer //neophodno za funkcionisanje 
public class App {
	public static void main(String[] args) {
            //ovo je discovery servis/centralni repozitorijum mikro servisa
            //obavezne zavisnosti: (raditi preko initializr
            // org.springframework.cloud - spring-cloud-starter
            //                           - spring-cloud-starter-netflix-eureka-server
            //                           - spring-cloud-dependencies
            //config - samo prekopirati za sada
            // server.port=8761 - default
            // eureka.instance.hostname=localhost - za rezoluciju adresa
            // eureka.client.register-with-eureka=false - mi smo discovery server
            // eureka.client.fetch-registry=false - mi smo registry
		SpringApplication.run(App.class, args);
	}
}