-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: localhost    Database: lmsdb
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adresa`
--

DROP TABLE IF EXISTS `adresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `adresa` (
  `id` int NOT NULL AUTO_INCREMENT,
  `broj` varchar(255) DEFAULT NULL,
  `ulica` varchar(255) DEFAULT NULL,
  `mesto_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKs3cl12kvk7ugqu3wi2u9y7vd0` (`mesto_id`),
  CONSTRAINT `FKs3cl12kvk7ugqu3wi2u9y7vd0` FOREIGN KEY (`mesto_id`) REFERENCES `mesto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adresa`
--

LOCK TABLES `adresa` WRITE;
/*!40000 ALTER TABLE `adresa` DISABLE KEYS */;
INSERT INTO `adresa` VALUES (1,'2','Prvomajska',1),(2,'22','Bulevar Oslobodjenja',2),(3,'25','Cara Dusana',1),(4,'3','Kralja Petra',1),(5,'5','Bulevar Oslobodjenja',1),(6,'6','Nikole Tesle',1),(7,'77','Cara Dusana',1),(8,'85','Vojvode Misica',1),(9,'123','Kneza Milosa',2),(10,'4','Maksima Gorkog',2),(11,'55','Jovana Cvijica',2),(12,'6','Narodnog Fronta',2),(13,'77','Milo┼ía Obili─ça',2),(14,'37','Nikole Pa┼íi─ça',1),(15,'53','Drinske Divizije',1),(16,'9','Save Kova─ìevica',1),(17,'87','Gospodar Jovanova',1),(18,'3','Stevana Sremca',1),(19,'2','Prvomajska2',1),(20,'23453','Prvomajska232',1),(21,'233','Prvomajska',1),(22,'22','Bulevar Oslobodjenja',1),(23,'7','Kralja Petra',1),(26,'124','Kneza Milosa',1),(37,'2','Prvomajska',2),(38,'221323','asdasd',2);
/*!40000 ALTER TABLE `adresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drzava`
--

DROP TABLE IF EXISTS `drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `drzava` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava`
--

LOCK TABLES `drzava` WRITE;
/*!40000 ALTER TABLE `drzava` DISABLE KEYS */;
INSERT INTO `drzava` VALUES (1,'Srbija');
/*!40000 ALTER TABLE `drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drzava_mesta`
--

DROP TABLE IF EXISTS `drzava_mesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `drzava_mesta` (
  `drzava_id` int NOT NULL,
  `mesta_id` int NOT NULL,
  UNIQUE KEY `UK_h03x13w24nmt28w9ah1jc40yw` (`mesta_id`),
  KEY `FKgfvnn84rsu1aafc0fd80aonaj` (`drzava_id`),
  CONSTRAINT `FK38k059v5dmjt82bw4x1t5xe8b` FOREIGN KEY (`mesta_id`) REFERENCES `mesto` (`id`),
  CONSTRAINT `FKgfvnn84rsu1aafc0fd80aonaj` FOREIGN KEY (`drzava_id`) REFERENCES `drzava` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drzava_mesta`
--

LOCK TABLES `drzava_mesta` WRITE;
/*!40000 ALTER TABLE `drzava_mesta` DISABLE KEYS */;
INSERT INTO `drzava_mesta` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `drzava_mesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluacija_znanja`
--

DROP TABLE IF EXISTS `evaluacija_znanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evaluacija_znanja` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ishod_id` int NOT NULL,
  `realizacija_predmeta_id` int DEFAULT NULL,
  `vreme_pocetka` datetime(6) DEFAULT NULL,
  `vreme_zavrsetka` datetime(6) DEFAULT NULL,
  `instrument_id` int DEFAULT NULL,
  `tip_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `realizacija_predmeta_id_UNIQUE` (`realizacija_predmeta_id`),
  KEY `FKgxjh275mfesjg68gogp7q7cmx` (`instrument_id`),
  KEY `FKeovnhph09ul4oh3f5ohpiqgse` (`tip_id`),
  CONSTRAINT `FKeovnhph09ul4oh3f5ohpiqgse` FOREIGN KEY (`tip_id`) REFERENCES `tip_evaluacije` (`id`),
  CONSTRAINT `FKgxjh275mfesjg68gogp7q7cmx` FOREIGN KEY (`instrument_id`) REFERENCES `instrument_evaluacije` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluacija_znanja`
--

LOCK TABLES `evaluacija_znanja` WRITE;
/*!40000 ALTER TABLE `evaluacija_znanja` DISABLE KEYS */;
INSERT INTO `evaluacija_znanja` VALUES (1,1,1,'2024-09-12 20:14:52.000000','2024-09-12 21:14:52.000000',1,1),(3,3,3,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',3,3),(4,4,4,'2024-10-09 10:00:00.000000','2024-01-09 12:00:00.000000',4,3),(5,5,5,'2024-10-09 10:00:00.000000','2024-01-09 12:00:00.000000',5,3),(7,7,7,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',1,3),(8,8,37,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',2,3),(9,9,9,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',3,3),(10,10,10,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',4,3),(11,11,11,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',5,3),(12,12,12,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',6,3),(13,6,6,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',1,3),(14,14,14,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',2,3),(15,15,15,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',3,3),(16,16,16,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',4,3),(17,17,17,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',5,3),(18,18,18,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',6,3),(19,19,19,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',1,3),(20,20,20,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',2,3),(21,21,21,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',3,3),(22,22,22,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',4,3),(23,23,23,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',5,3),(24,24,24,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',6,3),(25,25,25,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',1,3),(26,26,26,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',2,3),(27,27,27,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',3,3),(28,28,28,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',4,3),(29,29,29,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',5,3),(30,30,30,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',6,3),(31,31,31,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',1,3),(32,32,32,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',2,3),(33,33,33,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',3,3),(34,34,34,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',4,3),(35,35,35,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',5,3),(36,36,36,'2024-01-09 10:00:00.000000','2024-01-09 12:00:00.000000',6,3),(74,1,2,'2024-10-09 08:00:00.000000','2024-10-09 10:00:00.000000',4,4);
/*!40000 ALTER TABLE `evaluacija_znanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fajl`
--

DROP TABLE IF EXISTS `fajl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fajl` (
  `id` int NOT NULL AUTO_INCREMENT,
  `opis` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `obavestenje_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrxpr8birexi66xifbx80reiwy` (`obavestenje_id`),
  CONSTRAINT `FKrxpr8birexi66xifbx80reiwy` FOREIGN KEY (`obavestenje_id`) REFERENCES `obavestenje` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fajl`
--

LOCK TABLES `fajl` WRITE;
/*!40000 ALTER TABLE `fajl` DISABLE KEYS */;
/*!40000 ALTER TABLE `fajl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fakultet`
--

DROP TABLE IF EXISTS `fakultet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fakultet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `adresa_id` int DEFAULT NULL,
  `dekan_id` int DEFAULT NULL,
  `naziv` varchar(255) DEFAULT NULL,
  `univerzitet_id` int NOT NULL,
  `broj_telefona` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKc9lc5sekwpb19qpobc7eegjpn` (`univerzitet_id`),
  CONSTRAINT `FKc9lc5sekwpb19qpobc7eegjpn` FOREIGN KEY (`univerzitet_id`) REFERENCES `univerzitet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fakultet`
--

LOCK TABLES `fakultet` WRITE;
/*!40000 ALTER TABLE `fakultet` DISABLE KEYS */;
INSERT INTO `fakultet` VALUES (1,1,1,'Faculty of Engineering',1,'011748373','foe@aegis.ac.rs'),(2,1,2,'Faculty of Arts',1,'011474738','foa@aegis.ac.rs'),(3,1,3,'Faculty of Science',1,'0116263637','fos@aegis.ac.rs'),(4,2,4,'Faculty of Medicine',2,'021737377','fom@astrum.ac.rs'),(5,2,4,'Faculty of Law',2,'021873425','fol@astrum.ac.rs'),(6,2,3,'Faculty of Bussiness',2,'021936271','fob@astrum.ac.rs'),(7,3,2,'Faculty of Education',3,'011526262','foe@stella.ac.rs'),(8,3,2,'Faculty of Environmental Studies',3,'011928363','foen@stella.ac.rs'),(9,3,1,'Faculty of Social Sciences',3,'011377283','foss@stella.ac.rs');
/*!40000 ALTER TABLE `fakultet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `godina_studija`
--

DROP TABLE IF EXISTS `godina_studija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `godina_studija` (
  `id` int NOT NULL AUTO_INCREMENT,
  `godina` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `godina_studija`
--

LOCK TABLES `godina_studija` WRITE;
/*!40000 ALTER TABLE `godina_studija` DISABLE KEYS */;
INSERT INTO `godina_studija` VALUES (1,2024),(2,2023);
/*!40000 ALTER TABLE `godina_studija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrument_evaluacije`
--

DROP TABLE IF EXISTS `instrument_evaluacije`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `instrument_evaluacije` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrument_evaluacije`
--

LOCK TABLES `instrument_evaluacije` WRITE;
/*!40000 ALTER TABLE `instrument_evaluacije` DISABLE KEYS */;
INSERT INTO `instrument_evaluacije` VALUES (1,'instrument1'),(2,'instrument2'),(3,'instrument3'),(4,'instrument3'),(5,'instrument5'),(6,'instrument6');
/*!40000 ALTER TABLE `instrument_evaluacije` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ishod`
--

DROP TABLE IF EXISTS `ishod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ishod` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  `obrazovni_cilj_id` int DEFAULT NULL,
  `silabus_id` int DEFAULT NULL,
  `termin_nastave_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbbs7ut8a93q1ipta559tx2tk9` (`obrazovni_cilj_id`),
  KEY `FK1dqh3np2whrlx5nofy11x8mxr` (`silabus_id`),
  KEY `FK7dluv7bntchu3ggx1q8ooyd6t` (`termin_nastave_id`),
  CONSTRAINT `FK1dqh3np2whrlx5nofy11x8mxr` FOREIGN KEY (`silabus_id`) REFERENCES `silabus` (`id`),
  CONSTRAINT `FK7dluv7bntchu3ggx1q8ooyd6t` FOREIGN KEY (`termin_nastave_id`) REFERENCES `termin_nastave` (`id`),
  CONSTRAINT `FKbbs7ut8a93q1ipta559tx2tk9` FOREIGN KEY (`obrazovni_cilj_id`) REFERENCES `obrazovni_cilj` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ishod`
--

LOCK TABLES `ishod` WRITE;
/*!40000 ALTER TABLE `ishod` DISABLE KEYS */;
INSERT INTO `ishod` VALUES (1,'topic1',1,1,1),(2,'topic2',2,2,1),(3,'topic3',3,3,2),(4,'topic4',4,4,2),(5,'topic5',5,5,3),(6,'topic6',1,1,3),(7,'topic7',2,2,4),(8,'topic8',3,3,4),(9,'topic9',4,4,5),(10,'topic10',5,5,5),(11,'topic11',1,1,1),(12,'topic12',2,2,1),(13,'topic113',3,3,2),(14,'t14',4,4,2),(15,'topic1',5,5,3),(16,'topic1',1,1,3),(17,'topic1',2,2,4),(18,'topic1',3,3,4),(19,'topic1',4,5,5),(20,'topic1',5,1,5),(21,'topic1',1,2,1),(22,'topic1',2,3,1),(23,'topic1',3,4,2),(24,'topic1',4,5,2),(25,'topic1',5,1,3),(26,'topic1',1,2,3),(27,'topic1',2,3,4),(28,'topic1',3,4,4),(29,'topic1',4,5,5),(30,'topic1',5,1,5),(31,'topic1',1,2,6),(32,'topic1',2,3,6),(33,'topic1',3,4,2),(34,'topic1',4,5,1),(35,'topic1',5,1,1),(36,'topic1',1,2,1);
/*!40000 ALTER TABLE `ishod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mesto`
--

DROP TABLE IF EXISTS `mesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mesto` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  `drzava_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKs8rcbu6hgv8df9mee76jih079` (`drzava_id`),
  CONSTRAINT `FKs8rcbu6hgv8df9mee76jih079` FOREIGN KEY (`drzava_id`) REFERENCES `drzava` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mesto`
--

LOCK TABLES `mesto` WRITE;
/*!40000 ALTER TABLE `mesto` DISABLE KEYS */;
INSERT INTO `mesto` VALUES (1,'Beograd',1),(2,'Novi Sad',1);
/*!40000 ALTER TABLE `mesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavnik`
--

DROP TABLE IF EXISTS `nastavnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavnik` (
  `id` int NOT NULL AUTO_INCREMENT,
  `adresa_id` int DEFAULT NULL,
  `biografija` text NOT NULL,
  `ime` varchar(255) NOT NULL,
  `jmbg` varchar(255) NOT NULL,
  `broj_telefona` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavnik`
--

LOCK TABLES `nastavnik` WRITE;
/*!40000 ALTER TABLE `nastavnik` DISABLE KEYS */;
INSERT INTO `nastavnik` VALUES (1,1,'Tatjana Ivanic has been a dedicated history teacher for over 12 years. She holds a MasterΓÇÖs degree in European History from the University of Belgrade. Tatjana is known for her captivating storytelling and her ability to bring historical events to life in the classroom. She has a particular interest in medieval history and has led several student trips to historical sites across Europe.','Tatjana Ivani─ç','1507983741237','011555336','tatjana@aegis.ac.rs'),(2,1,'Marko Markovic has been teaching physics at the high school level for 18 years. He earned his degree in Physics from the University of Novi Sad. Marko is celebrated for his hands-on experiments and practical demonstrations that make physics both fun and understandable for his students. He is also involved in various science fairs and competitions, mentoring students to achieve their best.','Marko Markovi─ç','2203992814565','011843646','marko@aegis.ac.rs'),(3,2,'Milana Milanovic has been an inspiring literature teacher for over a decade. She graduated with a degree in Comparative Literature from the University of Belgrade. Milana is passionate about fostering a love for reading and critical thinking in her students. She has organized numerous literary clubs and events, encouraging students to explore diverse literary genres and authors.','Milana Milanovi─ç','1004985762343','021773635','milana@aegis.ac.rs'),(4,2,'Andrej Savovic has been a mathematics teacher for 15 years. He holds a degree in Mathematics from the University of Novi Sad. Andrej is known for his patient and methodical teaching approach, helping students to build a strong foundation in mathematics. He has developed several innovative teaching materials and is actively involved in curriculum development to enhance math education.','Andrej Savovi─ç','3009990827899','021774836','andrej@aegis.ac.rs');
/*!40000 ALTER TABLE `nastavnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nastavnik_na_realizaciji`
--

DROP TABLE IF EXISTS `nastavnik_na_realizaciji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nastavnik_na_realizaciji` (
  `id` int NOT NULL AUTO_INCREMENT,
  `broj_casova` int NOT NULL,
  `realizacija_predmeta_id` int DEFAULT NULL,
  `nastavnik_id` int DEFAULT NULL,
  `tip_nastave_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnevmktxs4agqf99pqqqlaeuhg` (`nastavnik_id`),
  KEY `FKo7x2h84alaxk70bvj5s7to3iu` (`tip_nastave_id`),
  CONSTRAINT `FKnevmktxs4agqf99pqqqlaeuhg` FOREIGN KEY (`nastavnik_id`) REFERENCES `nastavnik` (`id`),
  CONSTRAINT `FKo7x2h84alaxk70bvj5s7to3iu` FOREIGN KEY (`tip_nastave_id`) REFERENCES `tip_nastave` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nastavnik_na_realizaciji`
--

LOCK TABLES `nastavnik_na_realizaciji` WRITE;
/*!40000 ALTER TABLE `nastavnik_na_realizaciji` DISABLE KEYS */;
INSERT INTO `nastavnik_na_realizaciji` VALUES (1,20,1,1,1),(2,20,2,2,2),(3,20,3,3,1),(4,20,4,4,2),(5,20,5,1,1),(6,20,6,2,2),(7,20,7,3,1),(8,20,8,4,2),(9,20,9,1,1),(10,20,10,2,2),(11,20,11,3,1),(12,20,12,4,2),(13,20,13,1,1),(14,20,14,2,2),(15,20,15,3,1),(16,20,16,4,2),(17,20,17,1,1),(18,20,18,2,2),(19,20,19,3,1),(20,20,20,4,2),(21,20,21,1,1),(22,20,22,2,2),(23,20,23,3,1),(24,20,24,4,2),(25,20,25,1,1),(26,20,26,2,2),(27,20,27,3,1),(28,20,28,4,2),(29,20,29,1,1),(30,20,30,2,2),(31,20,31,3,1),(32,20,32,4,2),(33,20,33,1,1),(34,20,34,2,2),(35,20,35,3,1),(36,20,36,4,2);
/*!40000 ALTER TABLE `nastavnik_na_realizaciji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `naucna_oblast`
--

DROP TABLE IF EXISTS `naucna_oblast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `naucna_oblast` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `naucna_oblast`
--

LOCK TABLES `naucna_oblast` WRITE;
/*!40000 ALTER TABLE `naucna_oblast` DISABLE KEYS */;
INSERT INTO `naucna_oblast` VALUES (1,'Biology'),(2,'Mathematics'),(3,'Computer Science\n'),(4,'Engineering'),(5,'Geology'),(6,'Medicine'),(7,'Sociology'),(8,'Psychology'),(9,'Neuroscience'),(10,'Visual Arts\n'),(11,'Graphic Design\n'),(12,'Digital Art\n');
/*!40000 ALTER TABLE `naucna_oblast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obavestenje`
--

DROP TABLE IF EXISTS `obavestenje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obavestenje` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naslov` varchar(255) NOT NULL,
  `sadrzaj` varchar(255) NOT NULL,
  `vreme_postavljanja` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obavestenje`
--

LOCK TABLES `obavestenje` WRITE;
/*!40000 ALTER TABLE `obavestenje` DISABLE KEYS */;
INSERT INTO `obavestenje` VALUES (1,'Obavestenje1','Sadrzaj1','2024-05-02 00:00:00.000000'),(2,'Obavestenje2','Sadrzaj2','2024-05-02 00:00:00.000000'),(3,'Obavestenje3','Sadrzaj3','2024-05-02 00:00:00.000000');
/*!40000 ALTER TABLE `obavestenje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obrazovni_cilj`
--

DROP TABLE IF EXISTS `obrazovni_cilj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `obrazovni_cilj` (
  `id` int NOT NULL AUTO_INCREMENT,
  `opis` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obrazovni_cilj`
--

LOCK TABLES `obrazovni_cilj` WRITE;
/*!40000 ALTER TABLE `obrazovni_cilj` DISABLE KEYS */;
INSERT INTO `obrazovni_cilj` VALUES (1,'ObrazovniCilj1'),(2,'ObrazovniCilj2'),(3,'ObrazovniCilj3'),(4,'ObrazovniCilj4'),(5,'ObrazovniCilj5'),(6,'ObrazovniCilj6'),(7,'ObrazovniCilj9'),(8,'ObrazovniCilj7'),(9,'ObrazovniCilj8'),(10,'ObrazovniCilj10'),(11,'ObrazovniCilj11'),(12,'ObrazovniCilj12'),(13,'ObrazovniCilj13'),(14,'ObrazovniCilj14'),(15,'opis'),(16,'opis'),(17,'opis'),(18,'opis'),(19,'opis'),(20,'opis'),(21,'opis'),(22,'opis'),(23,'opis'),(24,'opis'),(25,'opis'),(26,'opis'),(27,'opis'),(28,'opis'),(29,'opis'),(30,'opis'),(31,'ObrazovniCilj1'),(32,'ObrazovniCilj1'),(33,'ObrazovniCilj1'),(34,'ObrazovniCilj1'),(35,'ObrazovniCilj1'),(36,'ObrazovniCilj36'),(37,'');
/*!40000 ALTER TABLE `obrazovni_cilj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pohadjanje_predmeta`
--

DROP TABLE IF EXISTS `pohadjanje_predmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pohadjanje_predmeta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `konacna_ocena` int DEFAULT NULL,
  `realizacija_predmeta_id` int DEFAULT NULL,
  `student_na_godini_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdbolvvj1fuj0powmeqr3v5xyu` (`student_na_godini_id`),
  CONSTRAINT `FKdbolvvj1fuj0powmeqr3v5xyu` FOREIGN KEY (`student_na_godini_id`) REFERENCES `student_na_godini` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pohadjanje_predmeta`
--

LOCK TABLES `pohadjanje_predmeta` WRITE;
/*!40000 ALTER TABLE `pohadjanje_predmeta` DISABLE KEYS */;
INSERT INTO `pohadjanje_predmeta` VALUES (38,NULL,2,1),(39,8,3,1),(40,NULL,4,2),(41,NULL,5,3),(42,7,6,1),(43,6,7,4),(44,6,8,4),(45,7,9,5),(46,7,10,5),(47,8,11,6),(48,8,12,6),(49,10,13,7),(50,10,14,7),(51,10,15,8),(52,9,16,8),(53,8,17,9),(54,8,18,9),(55,7,19,10),(56,76,20,10),(57,6,21,11),(58,9,22,11),(59,9,23,12),(60,6,24,12),(61,7,25,13),(62,8,26,13),(63,9,27,14),(64,10,28,14),(65,10,29,15),(66,10,30,15),(67,7,31,16),(68,8,32,16),(69,9,33,17),(70,8,34,17),(71,9,35,18),(72,10,36,18);
/*!40000 ALTER TABLE `pohadjanje_predmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polaganje`
--

DROP TABLE IF EXISTS `polaganje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polaganje` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bodovi` int NOT NULL,
  `napomena` varchar(255) NOT NULL,
  `evaluacija_znanja_id` int DEFAULT NULL,
  `student_na_godini_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKovo8yfdimt6l74kvcaih5l9q5` (`evaluacija_znanja_id`),
  KEY `FKk3is5klk83tliwxm5yy2yvqgg` (`student_na_godini_id`),
  CONSTRAINT `FKk3is5klk83tliwxm5yy2yvqgg` FOREIGN KEY (`student_na_godini_id`) REFERENCES `student_na_godini` (`id`),
  CONSTRAINT `FKovo8yfdimt6l74kvcaih5l9q5` FOREIGN KEY (`evaluacija_znanja_id`) REFERENCES `evaluacija_znanja` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polaganje`
--

LOCK TABLES `polaganje` WRITE;
/*!40000 ALTER TABLE `polaganje` DISABLE KEYS */;
INSERT INTO `polaganje` VALUES (1,50,'Nema napomene',1,1),(3,30,'Nema napomene',3,1),(7,67,'Nema napomene',7,4),(8,56,'Nema napomene',8,1),(9,55,'Nema napomene',9,5),(10,89,'Nema napomene',10,5),(11,99,'Nema napomene',11,6),(12,87,'Nema napomene',12,6),(13,72,'Nema napomene',13,7),(14,66,'Nema napomene',14,7),(15,78,'Nema napomene',15,8),(16,95,'Nema napomene',16,8),(17,56,'Nema napomene',17,9),(18,55,'Nema napomene',18,9),(19,57,'Nema napomene',19,10),(20,58,'Nema napomene',20,10),(21,59,'Nema napomene',21,11),(22,76,'Nema napomene',22,11),(23,61,'Nema napomene',23,12),(24,62,'Nema napomene',24,12),(25,72,'Nema napomene',25,13),(26,76,'Nema napomene',26,13),(27,77,'Nema napomene',27,14),(28,71,'Nema napomene',28,14),(29,71,'Nema napomene',29,15),(30,71,'Nema napomene',30,15),(31,71,'Nema napomene',31,16),(32,71,'Nema napomene',32,16),(33,73,'Nema napomene',33,17),(34,73,'Nema napomene',34,17),(35,74,'Nema napomene',35,18),(36,84,'Nema napomene',36,18);
/*!40000 ALTER TABLE `polaganje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `predmet`
--

DROP TABLE IF EXISTS `predmet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `predmet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `broj_predavanja` int NOT NULL,
  `broj_vezbi` int NOT NULL,
  `drugi_oblici_nastave` int NOT NULL,
  `espb` int NOT NULL,
  `istrazivacki_rad` int NOT NULL,
  `naziv` varchar(255) NOT NULL,
  `obavezan` bit(1) NOT NULL,
  `ostali_casovi` int NOT NULL,
  `godina_studija_id` int DEFAULT NULL,
  `preduslov_id` int DEFAULT NULL,
  `silabus_id` int DEFAULT NULL,
  `studijski_program_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK37thl4qshgxy3wornrfek2ji3` (`godina_studija_id`),
  KEY `FKimfvf1oxnywiehjr9gvnmaskc` (`preduslov_id`),
  KEY `FKk1okndwr49jpih2ban1qqhspc` (`silabus_id`),
  KEY `FKj6qcp2uo1cvr5xi31g0aihxot` (`studijski_program_id`),
  CONSTRAINT `FK37thl4qshgxy3wornrfek2ji3` FOREIGN KEY (`godina_studija_id`) REFERENCES `godina_studija` (`id`),
  CONSTRAINT `FKimfvf1oxnywiehjr9gvnmaskc` FOREIGN KEY (`preduslov_id`) REFERENCES `predmet` (`id`),
  CONSTRAINT `FKj6qcp2uo1cvr5xi31g0aihxot` FOREIGN KEY (`studijski_program_id`) REFERENCES `studijski_program` (`id`),
  CONSTRAINT `FKk1okndwr49jpih2ban1qqhspc` FOREIGN KEY (`silabus_id`) REFERENCES `silabus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `predmet`
--

LOCK TABLES `predmet` WRITE;
/*!40000 ALTER TABLE `predmet` DISABLE KEYS */;
INSERT INTO `predmet` VALUES (3,12,12,1,6,1,'Finite Element Analysis',_binary '',1,1,3,1,1),(4,12,12,1,6,1,'Robotics',_binary '',1,1,4,2,1),(5,12,12,1,6,1,'Control Systems',_binary '',1,2,3,3,2),(6,12,12,1,6,1,'Mechanics of Materials',_binary '',1,1,6,4,2),(7,12,12,1,6,1,'World Literature',_binary '',1,1,5,5,3),(8,12,12,1,6,1,'Literary Theory',_binary '',1,2,8,6,3),(9,12,12,1,6,1,'Modern European History',_binary '',1,1,7,7,4),(10,12,12,1,6,1,'Ancient Civilizations',_binary '',1,1,10,8,4),(11,12,12,1,6,1,'Genetics',_binary '',1,1,9,9,5),(12,12,12,1,6,1,'Cell Biology',_binary '',1,1,12,10,5),(13,12,12,1,6,1,'Quantum Mechanics',_binary '',1,1,11,11,6),(14,12,12,1,6,1,'Classical Mechanics',_binary '',1,1,14,12,6),(15,12,12,1,6,1,'Oral Anatomy',_binary '',1,1,13,13,7),(16,12,12,1,6,1,'Dental Materials',_binary '',1,1,16,14,7),(17,12,12,1,6,1,'Human Anatomy',_binary '',1,1,15,15,8),(18,12,12,1,6,1,'Pharmacology',_binary '',1,1,18,16,8),(19,12,12,1,6,1,'Constitutional Law',_binary '',1,1,17,17,9),(20,12,12,1,6,1,'Criminal Law',_binary '',1,1,20,18,9),(21,12,12,1,6,1,'Civil Procedure',_binary '',1,1,19,19,10),(22,12,12,1,6,1,'Contract Law',_binary '',1,1,19,20,10),(23,12,12,1,6,1,'Financial Accounting',_binary '',1,1,3,21,11),(24,12,12,1,6,1,'Marketing Management',_binary '',1,1,3,22,11),(25,12,12,1,6,1,'Strategic Management',_binary '',1,1,3,23,12),(26,12,12,1,6,1,'Corporate Finance',_binary '',1,1,3,24,12),(27,12,12,1,6,1,'Child Development',_binary '',1,1,3,25,13),(28,12,12,1,6,1,'Early Childhood Curriculum',_binary '',1,1,3,26,13),(29,12,12,1,6,1,'Instructional Design',_binary '',1,1,3,27,14),(30,12,12,1,6,1,'Educational Assessment',_binary '',1,1,3,28,14),(31,12,12,1,6,1,'Environmental Chemistry',_binary '',1,1,3,29,15),(32,12,12,1,6,1,'Ecology',_binary '',1,1,3,30,15),(33,12,12,1,6,1,'Environmental Policy',_binary '',1,1,3,31,16),(34,12,12,1,6,1,'Sustainable Development',_binary '',1,1,3,32,16),(35,12,12,1,6,1,'Social Theory',_binary '',1,1,3,33,17),(36,12,12,1,6,1,'Research Methods in Sociology',_binary '',1,1,3,34,17),(37,12,12,1,6,1,'International Political Economy',_binary '',1,1,3,35,18),(38,12,12,1,6,1,'Global Security Studies',_binary '',1,1,3,36,18);
/*!40000 ALTER TABLE `predmet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realizacija_predmeta`
--

DROP TABLE IF EXISTS `realizacija_predmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `realizacija_predmeta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `predmet_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrw6tx0pbaevbvs89psuqfqijx` (`predmet_id`),
  CONSTRAINT `FKrw6tx0pbaevbvs89psuqfqijx` FOREIGN KEY (`predmet_id`) REFERENCES `predmet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realizacija_predmeta`
--

LOCK TABLES `realizacija_predmeta` WRITE;
/*!40000 ALTER TABLE `realizacija_predmeta` DISABLE KEYS */;
INSERT INTO `realizacija_predmeta` VALUES (1,3),(2,4),(11,4),(14,4),(3,5),(37,5),(4,6),(12,6),(15,6),(5,7),(10,7),(13,7),(6,8),(7,9),(8,10),(9,11),(16,18),(17,19),(18,20),(19,21),(20,22),(21,23),(22,24),(23,25),(24,26),(25,27),(26,28),(27,29),(28,30),(29,31),(30,32),(31,33),(32,34),(33,35),(34,36),(35,37),(36,38);
/*!40000 ALTER TABLE `realizacija_predmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `silabus`
--

DROP TABLE IF EXISTS `silabus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `silabus` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `silabus`
--

LOCK TABLES `silabus` WRITE;
/*!40000 ALTER TABLE `silabus` DISABLE KEYS */;
INSERT INTO `silabus` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36);
/*!40000 ALTER TABLE `silabus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `adresa_id` int DEFAULT NULL,
  `ime` varchar(255) NOT NULL,
  `jmbg` varchar(255) NOT NULL,
  `broj_telefona` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,38,'Milica2','0203981170028','064353535','milica@aegis.student.ac.rs'),(3,3,'Stefan','3105016185034','066343253','stefan@aegis.student.ac.rs'),(4,4,'Nina','1907980170031','062435252','nina@aegis.student.ac.rs'),(5,5,'Dusan','2512995170010\n','061425364','dusan@aegis.student.ac.rs'),(6,6,'Lana','1204998170032\n','062987564','lana@astrum.student.ac.rs'),(7,7,'Emilija','0703009185005','064234167','emilija@astrum.student.ac.rs'),(8,8,'Nenad','2809984175041','065342421','nenad@astrum.student.ac.rs'),(9,9,'Mila','1507017185016\n','065982536','mila@astrum.student.ac.rs'),(10,10,'Martina','2904019170025','062341627','martina@astrum.student.ac.rs'),(11,11,'Milos','3004019170025','064535353','milos@stella.student.ac.rs'),(12,12,'Dusica','1904019170025','061434352','dusica@stella.student.ac.rs'),(13,13,'Nevena','2905019170025','062526373','nevena@stella.student.ac.rs'),(14,14,'Luka','2906019170025','067382515','luka@stella.student.ac.rs'),(15,15,'Lazar','1905019170025','063524142','lazar@stella.student.ac.rs'),(16,12,'Ema','1905019170026','063425472','ema@stella.student.ac.rs'),(17,11,'Anastasija','1905019170027','069999123','anastasija@astrum.student.ac.rs'),(18,9,'Milutin','1905029170025','065132927','milutin@aegis.student.ac.rs');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_na_godini`
--

DROP TABLE IF EXISTS `student_na_godini`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_na_godini` (
  `id` int NOT NULL AUTO_INCREMENT,
  `broj_indeksa` varchar(255) NOT NULL,
  `datum_upisa` date NOT NULL,
  `godina_studija_id` int DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `studijski_program_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKmu7rrfp6rrd3ds2i229h46d8l` (`student_id`),
  CONSTRAINT `FKmu7rrfp6rrd3ds2i229h46d8l` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_na_godini`
--

LOCK TABLES `student_na_godini` WRITE;
/*!40000 ALTER TABLE `student_na_godini` DISABLE KEYS */;
INSERT INTO `student_na_godini` VALUES (1,'123456','2024-09-26',1,1,1),(2,'123457','2024-09-26',1,1,2),(3,'123458','2022-09-26',2,1,3),(4,'123459','2024-09-26',1,4,4),(5,'123441','2024-09-26',1,5,5),(6,'321321','2024-09-26',1,6,6),(7,'456456','2024-09-26',1,7,7),(8,'765765','2024-09-26',1,8,8),(9,'789789','2024-09-26',1,9,9),(10,'987987','2024-09-26',1,10,10),(11,'135135','2024-09-26',1,11,11),(12,'579579','2024-09-26',1,12,12),(13,'975975','2024-09-26',1,13,13),(14,'531531','2024-09-26',1,14,14),(15,'712812','2024-09-26',1,15,15),(16,'463723','2024-09-26',1,16,16),(17,'340382','2024-09-26',1,17,17),(18,'263127','2024-09-26',1,18,18);
/*!40000 ALTER TABLE `student_na_godini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_na_godini_polaganja`
--

DROP TABLE IF EXISTS `student_na_godini_polaganja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_na_godini_polaganja` (
  `student_na_godini_id` int NOT NULL,
  `polaganja_id` int NOT NULL,
  UNIQUE KEY `UK_qob7tdt8oda0406qil3m4p5rs` (`polaganja_id`),
  KEY `FKp45hc2iq5apa0hk23b3chhvf0` (`student_na_godini_id`),
  CONSTRAINT `FKp45hc2iq5apa0hk23b3chhvf0` FOREIGN KEY (`student_na_godini_id`) REFERENCES `student_na_godini` (`id`),
  CONSTRAINT `FKppkaiwch7o4u2cww22bm3h06w` FOREIGN KEY (`polaganja_id`) REFERENCES `polaganje` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_na_godini_polaganja`
--

LOCK TABLES `student_na_godini_polaganja` WRITE;
/*!40000 ALTER TABLE `student_na_godini_polaganja` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_na_godini_polaganja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studijski_program`
--

DROP TABLE IF EXISTS `studijski_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studijski_program` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  `rukovodilac_id` int DEFAULT NULL,
  `godina_studija_id` int DEFAULT NULL,
  `fakultet_id` int DEFAULT NULL,
  `opis` text,
  PRIMARY KEY (`id`),
  KEY `FKc4cril27vvawgn5jixa08xs41` (`godina_studija_id`),
  KEY `FK303wuewako66hti92rcv4a9mh` (`fakultet_id`),
  CONSTRAINT `FK303wuewako66hti92rcv4a9mh` FOREIGN KEY (`fakultet_id`) REFERENCES `fakultet` (`id`),
  CONSTRAINT `FKc4cril27vvawgn5jixa08xs41` FOREIGN KEY (`godina_studija_id`) REFERENCES `godina_studija` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studijski_program`
--

LOCK TABLES `studijski_program` WRITE;
/*!40000 ALTER TABLE `studijski_program` DISABLE KEYS */;
INSERT INTO `studijski_program` VALUES (1,'Electrical Engineering',1,1,1,'Focuses on the design, development, and maintenance of electrical systems and components. Students learn about circuits, signals, power systems, and electronics.'),(2,'Mechanical Engineering',2,2,1,' Involves the design, analysis, and manufacturing of mechanical systems. Key areas include thermodynamics, fluid mechanics, and materials science.'),(3,'Comparative Literature',3,1,2,' An interdisciplinary program that examines literature across different cultures and languages, exploring themes, genres, and historical contexts.'),(4,'History',4,1,2,'Studies the development of human societies over time, covering political, social, economic, and cultural aspects. Students learn research methods and historiography.'),(5,'Biology',4,1,3,' Focuses on the study of living organisms, their functions, and interactions. Core subjects include genetics, ecology, and molecular biology.'),(6,'Physics',1,1,3,'Explores the fundamental principles governing the universe, including matter, energy, and their interactions. Topics include classical mechanics, quantum physics, and electromagnetism.'),(7,'Dentistry',2,1,4,'Prepares students for careers as dentists, covering oral health, anatomy, periodontics, and clinical practice. Includes extensive hands-on training with patients.'),(8,'Medicine',3,1,4,'A comprehensive program that trains students to become physicians. It includes basic sciences, clinical skills, and patient care, typically over four years.'),(9,'Bachelor of Laws (LL.B.)',4,1,5,'An undergraduate law degree providing a foundation in legal principles, research, and practice. It is often required for legal practice in many jurisdictions.'),(10,'Juris Doctor (J.D.)',1,1,5,'A professional graduate degree in law, typically required to practice law in the U.S. It covers core legal subjects and practical skills.'),(11,'Bachelor of Business Administration (BBA)',1,1,6,': Focuses on business management principles, including finance, marketing, operations, and strategic planning.'),(12,'Master of Business Administration (MBA)',2,1,6,'An advanced degree in business management, emphasizing leadership, strategic thinking, and specialized business functions.'),(13,'Bachelor of Education in Early Childhood Education\n',3,1,7,' Prepares students to teach young children, covering child development, curriculum design, and classroom management.'),(14,'Master of Education in Curriculum and Instruction\n',4,1,7,'Focuses on advanced educational strategies, curriculum development, and instructional methods for educators.'),(15,'Bachelor of Environmental Science\n',1,1,8,'Studies the interaction between humans and the environment, including ecology, conservation, and environmental policy.'),(16,'Master of Environmental Management\n',1,1,8,'An advanced degree focusing on sustainable practices, environmental policy, and resource management.'),(17,'Bachelor of Sociology\n',2,1,9,'Examines social behavior, institutions, and structures. Topics include social theory, research methods, and social issues.'),(18,'Master of International Relations\n\n\n\n\n\n',3,1,9,'Focuses on global politics, diplomacy, and international affairs. It covers theories of international relations, global security, and foreign policy.');
/*!40000 ALTER TABLE `studijski_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `termin_nastave`
--

DROP TABLE IF EXISTS `termin_nastave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `termin_nastave` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tip_nastave_id` int DEFAULT NULL,
  `vreme_pocetka` datetime(6) NOT NULL,
  `vreme_zavrsetka` datetime(6) NOT NULL,
  `realizacija_predmeta_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKasrq9uj8dmfa2dtyycxclwwwu` (`realizacija_predmeta_id`),
  CONSTRAINT `FKasrq9uj8dmfa2dtyycxclwwwu` FOREIGN KEY (`realizacija_predmeta_id`) REFERENCES `realizacija_predmeta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `termin_nastave`
--

LOCK TABLES `termin_nastave` WRITE;
/*!40000 ALTER TABLE `termin_nastave` DISABLE KEYS */;
INSERT INTO `termin_nastave` VALUES (1,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',1),(2,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',2),(3,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',3),(4,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',4),(5,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',5),(6,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',6),(7,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',7),(8,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',8),(9,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',9),(10,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',10),(11,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',11),(12,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',12),(13,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',13),(14,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',14),(15,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',15),(16,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',16),(17,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',17),(18,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',18),(19,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',19),(20,1,'2024-09-26 00:00:00.000000','2024-12-30 00:00:00.000000',20);
/*!40000 ALTER TABLE `termin_nastave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_evaluacije`
--

DROP TABLE IF EXISTS `tip_evaluacije`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tip_evaluacije` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_evaluacije`
--

LOCK TABLES `tip_evaluacije` WRITE;
/*!40000 ALTER TABLE `tip_evaluacije` DISABLE KEYS */;
INSERT INTO `tip_evaluacije` VALUES (1,'Kolokvijum'),(2,'Test'),(3,'Usmeni ispit'),(4,'Projekat');
/*!40000 ALTER TABLE `tip_evaluacije` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_nastave`
--

DROP TABLE IF EXISTS `tip_nastave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tip_nastave` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_nastave`
--

LOCK TABLES `tip_nastave` WRITE;
/*!40000 ALTER TABLE `tip_nastave` DISABLE KEYS */;
INSERT INTO `tip_nastave` VALUES (1,'Predavanja'),(2,'Vezbe'),(3,'Mentorska nastava');
/*!40000 ALTER TABLE `tip_nastave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_zvanja`
--

DROP TABLE IF EXISTS `tip_zvanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tip_zvanja` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_zvanja`
--

LOCK TABLES `tip_zvanja` WRITE;
/*!40000 ALTER TABLE `tip_zvanja` DISABLE KEYS */;
INSERT INTO `tip_zvanja` VALUES (1,'Doctorate '),(2,'Master\'s Degree'),(3,'Bachelor\'s Degree '),(4,'Professor'),(5,'Assistant Professor'),(6,'Postdoctoral Fellow');
/*!40000 ALTER TABLE `tip_zvanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `univerzitet`
--

DROP TABLE IF EXISTS `univerzitet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `univerzitet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `adresa_id` int DEFAULT NULL,
  `datum_osnivanja` datetime(6) DEFAULT NULL,
  `naziv` varchar(255) DEFAULT NULL,
  `rektor_id` int DEFAULT NULL,
  `broj_telefona` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `opis` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `univerzitet`
--

LOCK TABLES `univerzitet` WRITE;
/*!40000 ALTER TABLE `univerzitet` DISABLE KEYS */;
INSERT INTO `univerzitet` VALUES (1,1,'1999-02-02 00:00:00.000000','Aegis',1,'011543453','office@aegis.ac.rs',NULL),(2,2,'2002-03-25 00:00:00.000000','Astrum',2,'021345456','office@astrum.ac.rs',NULL),(3,3,'2012-05-25 00:00:00.000000','Stella',3,'011435263','office@stella.ac.rs',NULL);
/*!40000 ALTER TABLE `univerzitet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zvanje`
--

DROP TABLE IF EXISTS `zvanje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zvanje` (
  `id` int NOT NULL AUTO_INCREMENT,
  `datum_isteka` varchar(255) DEFAULT NULL,
  `datum_izbora` varchar(255) NOT NULL,
  `nosilac_zvanja_id` int DEFAULT NULL,
  `oblast_id` int DEFAULT NULL,
  `tip_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrivofqwliq572swd6e343h48r` (`nosilac_zvanja_id`),
  KEY `FK4arhwgno1epsc1c9rr4vt290t` (`oblast_id`),
  KEY `FK62p29irm7y8pr6hj66k06u4b` (`tip_id`),
  CONSTRAINT `FK4arhwgno1epsc1c9rr4vt290t` FOREIGN KEY (`oblast_id`) REFERENCES `naucna_oblast` (`id`),
  CONSTRAINT `FK62p29irm7y8pr6hj66k06u4b` FOREIGN KEY (`tip_id`) REFERENCES `tip_zvanja` (`id`),
  CONSTRAINT `FKrivofqwliq572swd6e343h48r` FOREIGN KEY (`nosilac_zvanja_id`) REFERENCES `nastavnik` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zvanje`
--

LOCK TABLES `zvanje` WRITE;
/*!40000 ALTER TABLE `zvanje` DISABLE KEYS */;
INSERT INTO `zvanje` VALUES (1,'2026.10.23.','2020.09.25.',1,1,4),(2,'2026.10.23.','2020.09.25.',2,2,4),(3,'2026.10.23.','2020.09.25.',3,3,4),(4,'2026.10.23.','2020.09.25.',4,4,4);
/*!40000 ALTER TABLE `zvanje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-09-17 19:56:11
